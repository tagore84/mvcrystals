package es.pozoesteban.alberto.mvcrystals.console;


import es.pozoesteban.alberto.mvcrystals.Configuration;
import es.pozoesteban.alberto.mvcrystals.controller.MVCrystalsGameController;
import es.pozoesteban.alberto.mvcrystals.model.player.Player;
import es.pozoesteban.alberto.mvcrystals.model.player.PlayerColor;
import es.pozoesteban.alberto.mvcrystals.model.player.PlayerConfig;

import java.io.FileWriter;
import java.io.IOException;

public class ConsoleLauncher {
	public static void main (String[] arg) {
		nGamesTest(100);
	}

	private static void testPy4J() {
		MVCrystalsGameController controller = new MVCrystalsGameController(false, true);
		controller.createGame("-seed 3 -player JUANITO_EL_ALEATORIO 0 Juanito0 -player JUANITO_EL_ALEATORIO 1 Juanito1");
	}

	private static void nGamesTest(int n) {
		for (int i = 0; i < n; i++) {
			System.out.println("START Game " + i);
			oneGameTest(i);
			System.out.println("END Game " + i);
		}
	}
	private static void oneGameTest(int seed) {
		MVCrystalsGameController controller = new MVCrystalsGameController(false, false);
		Configuration.SEED = seed;
		controller.startGame(new PlayerConfig[]{
				PlayerConfig.createMaximinlian(0,"_0", 2, 1, 0.0001f, 0.5f, PlayerColor.values()[0]),
				PlayerConfig.createJuanitoSemillasPlayerConfig(1,"_1", PlayerColor.values()[1]),
				PlayerConfig.createJuanitoSemillasPlayerConfig(2,"_2", PlayerColor.values()[2]),
				PlayerConfig.createJuanitoSemillasPlayerConfig(3,"_3", PlayerColor.values()[3])
		});
	}
	private static void someThing(int seed) {
		MVCrystalsGameController controller = new MVCrystalsGameController(false, false);

		try {
			FileWriter file0 = new FileWriter("../../maxi.csv", true);
			file0.write("Maxi 1s;Maxi 2s;Maxi 4s;Maxi 8s;");
			file0.write(System.getProperty("line.separator"));
			file0.close();
			while (true) {
				FileWriter file = new FileWriter("../../maxi.csv", true);
				controller.startGame(new PlayerConfig[]{
						//PlayerConfig.createMaximinlian(0, "Maxi Imbatible0", 1, 1, 0.00001f, 0.5f),
						//PlayerConfig.createLillo(0,"Lillo"),
						PlayerConfig.createJuanitoSemillasPlayerConfig(0,"_0", PlayerColor.values()[0]),
						PlayerConfig.createJuanitoSemillasPlayerConfig(1,"_1", PlayerColor.values()[1]),
						PlayerConfig.createJuanitoSemillasPlayerConfig(2,"_2", PlayerColor.values()[2]),
						PlayerConfig.createJuanitoSemillasPlayerConfig(3,"_3", PlayerColor.values()[3])
				});
				for (int i = 0; i < controller.getGameState().getPlayers().length; i++) {
					Player player = controller.getGameState().getPlayers()[i];
					if (player != null) {
						file.write(player.getPoints() + ";" + controller.playerControllers.get(player.getId()).getTotalSecondsThingking() + ";");
					}
				}
				file.write(System.getProperty("line.separator"));
				file.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
