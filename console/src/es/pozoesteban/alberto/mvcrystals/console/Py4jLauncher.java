package es.pozoesteban.alberto.mvcrystals.console;

import es.pozoesteban.alberto.mvcrystals.controller.MVCrystalsGameController;
import py4j.GatewayServer;


public class Py4jLauncher {
    public static void main (String[] args) {
        System.out.println("Wainting from python...");
        MVCrystalsGameController controller = new MVCrystalsGameController(false, true);
        GatewayServer server = new GatewayServer(controller);
        server.start();
    }

}