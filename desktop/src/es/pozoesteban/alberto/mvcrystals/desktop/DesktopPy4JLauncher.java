package es.pozoesteban.alberto.mvcrystals.desktop;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import es.pozoesteban.alberto.mvcrystals.controller.MVCrystalsGameController;
import py4j.GatewayServer;

public class DesktopPy4JLauncher {

    public static void main(String[] args) {
        System.out.println("Loading interface...");
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.title = "MVCrystals";
        config.height = 1012;
        config.width = 1440;
        config.resizable = false;
        config.forceExit = false;
        final MVCrystalsGameController controller = new MVCrystalsGameController(true, true);
        new LwjglApplication(controller, config);

        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                System.out.println("Wainting from python...");
                GatewayServer server = new GatewayServer(controller);
                server.start();
            }
        });
    }
}
