package es.pozoesteban.alberto.mvcrystals.desktop;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.OrthographicCamera;
import es.pozoesteban.alberto.mvcrystals.controller.MVCrystalsGameController;

import java.util.Date;

public class DesktopLauncher {
	public static void main(String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Cristales";
		//config.fullscreen = true;
		config.height = 1000;//Gdx.graphics.getDisplayMode().height;;
		config.width = 1600;//Gdx.graphics.getDisplayMode().height;;
		config.resizable = false;
		config.forceExit = true;
		new Date().getTime();
		new LwjglApplication(new MVCrystalsGameController(true, false), config);
	}
}
