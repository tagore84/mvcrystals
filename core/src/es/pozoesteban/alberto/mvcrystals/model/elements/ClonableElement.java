package es.pozoesteban.alberto.mvcrystals.model.elements;

public class ClonableElement {

    protected final boolean isClone;

    public ClonableElement(boolean isClone) {
        this.isClone = isClone;
    }
    public boolean isClone() {
        return isClone;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
