package es.pozoesteban.alberto.mvcrystals.model.elements;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import static es.pozoesteban.alberto.mvcrystals.Configuration.MODEL_LOGGER;
import static es.pozoesteban.alberto.mvcrystals.Configuration.SEED;

public class Sack extends ClonableElement {

    private final List<Crystal> crystals;

    public Sack(List<Crystal> crystals) {
        super(false);
        this.crystals = new ArrayList();
        this.crystals.addAll(crystals);
    }

    // Clone...
    public Sack(Sack sack, List<Crystal> crystals) {
        super(true);
        this.crystals = new ArrayList();
        for (Crystal crystalToClone : sack.crystals) {
            for (Crystal crystal : crystals) {
                if (crystalToClone.getId() == crystal.getId()) {
                    this.crystals.add(crystal);
                    break;
                }
            }
        }
    }

    public void shuffle() {
        Collections.shuffle(crystals, new Random(SEED));
    }

    public int size() {
        return crystals.size();
    }

    public void refill(List<Crystal> moreCrystals) {
        MODEL_LOGGER.fine("Refilling sack!");
        crystals.addAll(moreCrystals);
    }

    public Crystal takeRandomCrystal() {
        if (crystals.isEmpty()) return null;
        shuffle();
        Crystal crystal = crystals.get(0);
        crystals.remove(crystal);
        return crystal;
    }

    public List<Crystal> getAllCrystals() {
        List<Crystal> output = new ArrayList();
        output.addAll(crystals);
        return output;
    }

    public boolean hasCrystal(int id) {
        if (id == 0) return false;
        for (Crystal crystal : crystals) {
            if (crystal.getId() == id) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Sack: ");
        sb.append(Integer.toHexString(hashCode()));
        for (Crystal crystal : crystals) {
            sb.append(crystal.getId()).append(", ");
        }
        return sb.toString();
    }
}
