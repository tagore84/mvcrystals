package es.pozoesteban.alberto.mvcrystals.model.elements;

import es.pozoesteban.alberto.mvcrystals.model.token_position.TokenPosition;

public class Crystal extends Token {

    private final Color color;
    private boolean isSelected;

    public Crystal(int id, Color color, TokenPosition tokenPosition, boolean isClone, boolean isSelected) {
        super(id, isClone);
        if (id <= 0 || id > 100) {
            throw new IllegalArgumentException("Invalid crystal id " + id);
        }
        this.color = color;
        this.isSelected = isSelected;
        this.move(tokenPosition);
    }

    @Override
    public String toString() {
        return "(" + id + ") " + color.toString() + (isSelected ? " SELECTED!" : "");
    }

    @Override
    public boolean isCrystal() {
        return true;
    }

    @Override
    public String toOneCharString() {
        return getColor().toOneChartString();
    }

    public Color getColor() {
        return color;
    }

    public void isSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }
    public boolean isSelected() {
        return isSelected;
    }
}
