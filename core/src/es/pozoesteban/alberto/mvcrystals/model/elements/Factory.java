package es.pozoesteban.alberto.mvcrystals.model.elements;

import es.pozoesteban.alberto.mvcrystals.model.token_position.InFactory;

import java.util.ArrayList;
import java.util.List;

public class Factory extends TokenWarehouse {

    private final int id;

    public Factory(int id) {
        super(4, true, false);
        this.id = id;
    }

    // Clone...
    public Factory(Factory factoryToClone, List<Crystal> crystals) {
        super(4, true, true);
        this.id = factoryToClone.id;

        for (int i = 0; i < factoryToClone.slots.length; i++) {
            for (Crystal crystal : crystals) {
                if (!factoryToClone.slots[i].isEmpty() && factoryToClone.slots[i].getToken().getId() == crystal.getId()) {
                    this.slots[i].put(crystal);
                    crystal.move(new InFactory(factoryToClone.getId(), i));
                    break;
                }
            }
        }
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Factory ").append(id).append(" ");
        sb.append(Integer.toHexString(hashCode()));
        sb.append(" (");
        for (Token token : getTokens()) {
            sb.append(token.toString()).append(" ");
        }
        sb.append(")");
        return sb.toString();
    }

    public List<Crystal> getOthers(int crystalId) {
        Color color = null;
        List<Token> tokens = getTokens();
        for (Token token : tokens) {
            if (token.getId() == crystalId) color = ((Crystal)token).getColor();
        }
        if (color == null) throw new IllegalArgumentException("Crystal " + crystalId + " not found in center plate!");
        List<Crystal> others = new ArrayList();
        for (Token token : tokens) {
            if (token.isCrystal() && ((Crystal)token).getColor() != color) {
                others.add((Crystal) token);
            }
        }
        return others;
    }


}
