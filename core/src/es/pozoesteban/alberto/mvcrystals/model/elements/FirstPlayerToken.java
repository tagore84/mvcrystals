package es.pozoesteban.alberto.mvcrystals.model.elements;

public class FirstPlayerToken extends Token {

    private boolean isTaken;

    public FirstPlayerToken() {
        super(0, false);
        this.isTaken = false;
    }

    // Clone
    public FirstPlayerToken(FirstPlayerToken firstPlayerToken) {
        super(0, true);
        this.isTaken = firstPlayerToken.isTaken;
        this.move(firstPlayerToken.position.clone());
    }

    @Override
    public boolean isCrystal() {
        return false;
    }

    @Override
    public String toOneCharString() {
        return "1";
    }

    @Override
    public String toString() {
        return "First Token " + (isTaken ? " is Taken" : "is no taken");
    }

    public void isTaken(boolean b) {
        isTaken = b;
    }
    public boolean isTaken() {
        return isTaken;
    }


}
