package es.pozoesteban.alberto.mvcrystals.model.elements;

public class Slot extends ClonableElement {

    private Token token;
    private final boolean onlyCrystals;
    private final Color justThisColor;

    public Slot(boolean onlyCrystals, boolean isClone) {
        super(isClone);
        this.onlyCrystals = onlyCrystals;
        this.justThisColor = null;
    }
    public Slot(Color justThisColor, boolean isClone) {
        super(isClone);
        this.onlyCrystals = true;
        this.justThisColor = justThisColor;
    }

    public void put(Token token) {
        if (onlyCrystals && token.isFirstPlayerToken()){
            throw new IllegalArgumentException("FirstPlayerToken in only crystal slot!");
        }
        if (justThisColor != null && justThisColor != ((Crystal)token).getColor()) {
            throw new IllegalArgumentException("Only " + justThisColor + " acepted and " + ((Crystal)token).getColor() + " found!");
        }
        this.token = token;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Slot ");
        if (onlyCrystals) sb.append("[OC] ");
        if (justThisColor != null) sb.append("[").append(justThisColor.toOneChartString()).append("] ");
        if (token == null) sb.append("empty");
        else sb.append(token.toString());
        return sb.toString();
    }

    public Token getToken() {
        return token;
    }

    public boolean isEmpty() {
        return token == null;
    }

    public void reset() {
        this.token = null;
    }

    public String toOneCharString() {
        if (isEmpty()) return " ";
        else return getToken().toOneCharString();
    }
}
