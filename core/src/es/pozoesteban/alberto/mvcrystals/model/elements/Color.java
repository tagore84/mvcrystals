package es.pozoesteban.alberto.mvcrystals.model.elements;

public enum Color {

    BLUE(0), YELLOW(1), ORANGE(2), RED(3), WHITE(4);

    private static final long serialVersionUID = 1L;

    private final int value;

    Color(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public String toOneChartString() {
        switch (this) {
            case BLUE:
                return "B";
            case ORANGE:
                return "O";
            case RED:
                return "R";
            case YELLOW:
                return "Y";
            case WHITE:
                return "W";
        }
        throw new IllegalArgumentException("Invalid color value: " + value);
    }
}
