package es.pozoesteban.alberto.mvcrystals.model.elements;

import java.util.ArrayList;
import java.util.List;

import static es.pozoesteban.alberto.mvcrystals.Configuration.CENTER_PLATE_RANDOM_POSITIONS;
import static es.pozoesteban.alberto.mvcrystals.model.GameState.DICE;

public abstract class TokenWarehouse extends ClonableElement{

    protected final Slot[] slots;

    public TokenWarehouse(int slotsSize, boolean onlyCrystals, boolean isClone) {
        super(isClone);
        slots = new Slot[slotsSize];
        for (int i = 0; i < slots.length; i++) {
            slots[i] = new Slot(onlyCrystals, isClone);
        }
    }

    public List<Crystal> getCrystals(int crystalId) {
        Color color = null;
        List<Token> tokens = getTokens();
        for (Token token : tokens) {
            if (token.getId() == crystalId) color = ((Crystal)token).getColor();
        }
        if (color == null) throw new IllegalArgumentException("Crystal " + crystalId + " not found in token warehouse!");
        List<Crystal> crystals = new ArrayList();
        for (Token token : tokens) {
            if (token.isCrystal() && ((Crystal)token).getColor() == color) {
                crystals.add((Crystal) token);
            }
        }
        return crystals;
    }

    public int put(Token token) {
        List<Integer> emptySlotsPositions = new ArrayList();
        for (int i = 0; i < slots.length; i++) {
            if (slots[i].isEmpty()) emptySlotsPositions.add(i);
        }
        int randomPos = (CENTER_PLATE_RANDOM_POSITIONS || isClone) ?
                emptySlotsPositions.get(DICE.nextInt(emptySlotsPositions.size())) :
                calcPositionByColor(token, emptySlotsPositions);
        slots[randomPos].put(token);
        return randomPos;
    }

    private int calcPositionByColor(Token token, List<Integer> emptySlotsPositions) {
        if (token.isFirstPlayerToken()) {
            return nearestPositionTo(9, emptySlotsPositions);
        }
        else {
            boolean someCrystal = false;
            for (int i = 0; i < slots.length; i++) {
                Slot slot = slots[i];
                if (!slot.isEmpty() && slot.getToken().isCrystal()) {
                    someCrystal = true;
                    if (((Crystal) slot.getToken()).getColor() == ((Crystal) token).getColor()) {
                        return nearestPositionTo(i, emptySlotsPositions);
                    }
                }
            }
            return someCrystal ? farestPositionTo(emptySlotsPositions) : emptySlotsPositions.get(DICE.nextInt(emptySlotsPositions.size()));
        }
    }
    private int farestPositionTo(List<Integer> emptySlotsPositions) {
        int sideSize = (int) Math.ceil(Math.sqrt(slots.length)) + 1;
        float maxDistance = 0;
        int bestPos = -1;
        for (int pos : emptySlotsPositions) {
            float xPos = (pos % (sideSize - 1) + 0.5f);
            float yPos = (pos / (sideSize - 1) + 0.5f);
            float sumDistance = 0;
            for (int i = 0; i < slots.length; i++) {
                Slot slot = slots[i];
                if (!slot.isEmpty() && !slot.getToken().isFirstPlayerToken()) {
                    float xCrystal = (i % (sideSize - 1) + 0.5f);
                    float yCrystal = (i / (sideSize - 1) + 0.5f);
                    sumDistance += (xCrystal - xPos) * (xCrystal - xPos) + (yCrystal - yPos) * (yCrystal - yPos);
                }
            }
            if (sumDistance > maxDistance) {
                maxDistance = sumDistance;
                bestPos = pos;
            }
        }
        if (bestPos == -1) {
            farestPositionTo(emptySlotsPositions); // ToDo remove!
            throw new IllegalArgumentException("Invalid pos for farest position");
        }
        return bestPos;
    }
    private int nearestPositionTo(int position, List<Integer> emptySlotsPositions) {
        int xSide = slots.length == 28 ? 8 : 7;
        //int ySide = slots.length == 16 ? 4 : 5;
        float xBase = (position % (xSide-1) + 0.5f);
        float yBase = (position / (xSide-1) + 0.5f);


        float minDistance = Float.MAX_VALUE;
        int bestPos = -1;
        for (int i = 0; i < slots.length; i++) {
            if (emptySlotsPositions.contains(i)) {
                float xPos = (i % (xSide - 1) + 0.5f);
                float yPos = (i / (xSide - 1) + 0.5f);
                float distance = (xBase - xPos) * (xBase - xPos) + (yBase - yPos) * (yBase - yPos);
                if (distance < minDistance) {
                    minDistance = distance;
                    bestPos = i;
                }
            }
        }
        if (bestPos == -1) {
            throw new IllegalArgumentException("Invalid pos for position " + position);
        }
        return bestPos;
    }

    public List<Token> getTokens() {
        List<Token> tokens = new ArrayList();
        for (Slot slot : slots) {
            if (slot.getToken() != null) tokens.add(slot.getToken());
        }
        return tokens;
    }

    public Slot[] getSlots() {
        return slots;
    }

    public int numSlots(){
        return slots.length;
    }

    public Crystal getCrystal(int id) {
        for (Slot slot : slots) {
            Token token = slot.getToken();
            if (token != null && token.getId() == id) return (Crystal) token;
        }
        return null;
    }

    public List<Crystal> getAllCrystals() {
        List<Crystal> output = new ArrayList();
        for (Slot slot : slots) {
            if (!slot.isEmpty() && slot.getToken().isCrystal()) {
                output.add((Crystal) slot.getToken());
            }
        }
        return output;
    }

    public boolean isEmpty() {
        for (Slot slot : slots) {
            if (!slot.isEmpty() && slot.getToken().getId() != 0) return false;
        }
        return true;
    }

    public List<Crystal> getCrystals(Color color) {
        List<Crystal> outputList = new ArrayList();
        for (Crystal crystal : getAllCrystals()) {
            if (crystal.getColor() == color) outputList.add(crystal);
        }
        return outputList;
    }
}
