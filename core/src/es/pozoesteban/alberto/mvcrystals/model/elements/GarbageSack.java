package es.pozoesteban.alberto.mvcrystals.model.elements;

import java.util.ArrayList;
import java.util.List;

public class GarbageSack extends ClonableElement {

    private final List<Crystal> crystals;

    public GarbageSack() {
        super(false);
        crystals = new ArrayList<>();
    }

    // Clone...
    public GarbageSack(GarbageSack garbageSack, List<Crystal> crystals) {
        super(true);
        this.crystals = new ArrayList();
        for (Crystal crystalToClone : garbageSack.crystals) {
            for (Crystal crystal : crystals) {
                if (crystalToClone.getId() == crystal.getId()) {
                    this.crystals.add(crystal);
                    break;
                }
            }
        }
    }

    public List<Crystal> getCrystals() {
        return crystals;
    }

    public void clear() {
        crystals.clear();
    }

    public void put(Crystal crystal) {
        crystals.add(crystal);
    }

    public int size() {
        return crystals.size();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("GarbageSack ");
        sb.append(Integer.toHexString(hashCode()));
        if (isClone()) sb.append("clone ");
        sb.append("( ");
        for (Crystal crystal : getCrystals()) {
            sb.append(crystal.toString()).append(" ");
        }
        sb.append(")");
        return sb.toString();
    }
}
