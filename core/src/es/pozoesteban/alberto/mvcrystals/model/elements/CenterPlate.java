package es.pozoesteban.alberto.mvcrystals.model.elements;

import es.pozoesteban.alberto.mvcrystals.model.token_position.InCenterPlate;

import java.util.List;

public class CenterPlate extends TokenWarehouse {

    public CenterPlate(int numFactories, FirstPlayerToken firstPlayerToken) {
        super((numFactories * 3) + 1, false, false);
        int positionIndex = put(firstPlayerToken);
        firstPlayerToken.move(new InCenterPlate(positionIndex));
    }

    public boolean hasFirstPlayerToken() {
        for (Slot slot : slots) {
            if (!slot.isEmpty() && slot.getToken().isFirstPlayerToken()) return true;
        }
        return false;
    }
    // Clone...
    public CenterPlate(CenterPlate centerPlateToClone, FirstPlayerToken firstPlayerToken, List<Crystal> crystals) {
        super(centerPlateToClone.slots.length, false, true);
        for (int i = 0; i < centerPlateToClone.getSlots().length; i++) {
            for (Crystal crystal : crystals) {
                if (!centerPlateToClone.slots[i].isEmpty() && centerPlateToClone.slots[i].getToken().getId() == crystal.getId()) {
                    this.slots[i].put(crystal);
                    crystal.move(new InCenterPlate(i));
                    break;
                }
            }
        }
        int positionIndex = put(firstPlayerToken);
        firstPlayerToken.move(new InCenterPlate(positionIndex));
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Center Plate");
        sb.append(Integer.toHexString(hashCode()));
        for (Token token : getTokens()) {
            sb.append(" | ").append(token.toString());
        }
        return sb.toString();
    }
}
