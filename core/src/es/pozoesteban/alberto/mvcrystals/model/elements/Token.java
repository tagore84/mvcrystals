package es.pozoesteban.alberto.mvcrystals.model.elements;

import es.pozoesteban.alberto.mvcrystals.model.token_position.TokenPosition;

import java.util.ArrayList;
import java.util.List;

public abstract class Token extends ClonableElement {

    protected final int id;
    protected TokenPosition position;
    protected List<TokenPosition> debugPath;

    protected Token(int id, boolean isClone) {
        super(isClone);
        this.id = id;
        this.debugPath = new ArrayList();
    }

    public abstract boolean isCrystal();
    public boolean isFirstPlayerToken(){
        return !isCrystal();
    }

    public int getId() {
        return id;
    }

    public abstract String toOneCharString();

    public void move(TokenPosition newPosition) {
        position = newPosition;
        debugPath.add(newPosition);
    }

    public TokenPosition getPosition() {
        return position;
    }
}
