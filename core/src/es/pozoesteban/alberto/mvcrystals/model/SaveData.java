package es.pozoesteban.alberto.mvcrystals.model;

import es.pozoesteban.alberto.mvcrystals.model.player.ConcretePlayer;
import es.pozoesteban.alberto.mvcrystals.model.player.PlayerColor;
import es.pozoesteban.alberto.mvcrystals.model.player.PlayerType;

public class SaveData {

    private boolean firstTime;
    private ConcretePlayer[] lastPlayer;
    private PlayerColor[] lastPlayerColor;
    private String[] lastPlayerName;

    public SaveData() {
        this.firstTime = true;
        lastPlayer = new ConcretePlayer[]{ConcretePlayer.HUMAN, ConcretePlayer.NONE, ConcretePlayer.NONE, ConcretePlayer.NONE};
        lastPlayerColor = new PlayerColor[]{PlayerColor.BLUE, PlayerColor.GREEN, PlayerColor.YELLOW, PlayerColor.RED};
        lastPlayerName = new String[]{"Jugador1", "", "", ""};
    }

    public void isFirstTime(boolean firstTime) {
        this.firstTime = firstTime;
    }
    public boolean isFirstTime() {
        return firstTime;
    }

    public ConcretePlayer[] getLastPlayer() {
        return lastPlayer;
    }

    public void setLastPlayer(ConcretePlayer lastPlayer, int i) {
        this.lastPlayer[i] = lastPlayer;
    }

    public PlayerColor[] getLastPlayerColor() {
        return lastPlayerColor;
    }

    public void setLastPlayerColor(PlayerColor lastPlayerColor, int i) {
        this.lastPlayerColor[i] = lastPlayerColor;
    }

    public String[] getLastPlayerName() {
        return lastPlayerName;
    }

    public void setLastPlayerName(String lastPlayerName, int i) {
        this.lastPlayerName[i] = lastPlayerName;
    }
}
