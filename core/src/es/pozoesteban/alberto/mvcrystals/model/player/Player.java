package es.pozoesteban.alberto.mvcrystals.model.player;

import es.pozoesteban.alberto.mvcrystals.model.elements.*;

import java.util.List;

public class Player extends ClonableElement implements Comparable<Player> {

    private final int id;
    private final PlayerType type;
    private final String name;
    private final SlotsPanel slotsPanel;
    private final FinalPanel finalPanel;
    private int points;
    private int tieBreakPoints;
    private boolean isNextFirstPlayer;
    private final PlayerColor playerColor;

    public Player(int id, String name, PlayerType type, GarbageSack garbageSack, CenterPlate centerPlate, PlayerColor playerColor) {
        super(false);
        this.id = id;
        this.type = type;
        this.name = name;
        this.slotsPanel = new SlotsPanel(garbageSack, centerPlate);
        this.finalPanel = new FinalPanel();
        this.points = 0;
        this.tieBreakPoints = 0;
        this.isNextFirstPlayer = false;
        this.playerColor = playerColor;
    }

    // Clone...
    public Player(Player player, GarbageSack garbageSack, CenterPlate centerPlate, List<Crystal> crystals) {
        super(true);
        this.id = player.id;
        this.type = player.type;
        this.name = player.name;
        this.playerColor = player.getColor();
        this.slotsPanel = new SlotsPanel(player.slotsPanel, garbageSack, centerPlate, crystals, id);
        this.finalPanel = new FinalPanel(player.finalPanel, crystals, id);
        this.points = 0;
        this.tieBreakPoints = 0;
        this.isNextFirstPlayer = false;
    }

    public PlayerColor getColor() {
        return playerColor;
    }

    public String getName() {
        return name;
    }

    public SlotsPanel getSlotsPanel() {
        return slotsPanel;
    }

    public FinalPanel getFinalPanel() {
        return finalPanel;
    }

    public int getId() {
        return id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Player) {
            Player player = (Player) obj;
            return player.getId() == this.id;
        }
        return false;
    }

    @Override
    public String toString() {
        return Integer.toHexString(hashCode()) + "Player " + this.id + ": " + name + "[" + type + "]";
    }

    public boolean selectableRow(int row, Color color) {
        if (finalPanel.hasColorInRow(row, color)) return false;
        if (slotsPanel.getColor(row) != null && slotsPanel.getColor(row) != color) return false;
        return true;
    }

    /**
     * Pone el crystal en la fila,
     * en caso de no caber lo pone en el suelo,
     * en caso de no caber devuelve false para que sea
     * tirado a la basura.
     * @param crystal
     * @param row
     * @return Si se ha insertado en el panel el cristal o no.
     */
    public boolean putCrystal(Crystal crystal, int row) {
        if (row == 5) {
            return slotsPanel.putCrystalInFloor(crystal, id);
        }
        boolean full = slotsPanel.putCrystal(crystal, row, id);
        if (full) {
            return slotsPanel.putCrystalInFloor(crystal, id);
        } else {
            return true;
        }
    }

    public void finalPhasePoints() {
        for (int row = 0; row < 5; row++) {
            if (slotsPanel.isComplete(row)) {
                Crystal crystal = slotsPanel.getOneAndThrowOthers(row);
                finalPanel.put(crystal, row, id);
                points += finalPanel.finalPhasePoints(crystal, row);
            }
        }
        points += slotsPanel.discountFloorPoints();
        this.isNextFirstPlayer = slotsPanel.clearFloor();
    }

    public void finalGamePoints() {
        int[] finalPoints = finalPanel.finalGamePoints();
        points += finalPoints[0];
        tieBreakPoints = finalPoints[1];
    }

    public boolean hasFinished() {
        return finalPanel.hasFinished();
    }

    public boolean isNextFirstPlayer() {
        return isNextFirstPlayer;
    }

    public int getPoints() {
        return points;
    }

    public boolean isAI() {
        return type != PlayerType.HUMAN && type != PlayerType.NONE;
    }

    public boolean isHuman() {
        return type == PlayerType.HUMAN;
    }

    public boolean takeFirstPlayerToken(FirstPlayerToken token) {
        return slotsPanel.putCrystalInFloor(token, id);
    }

    public void isNextFirstPlayer(boolean isNextFirstPlayer) {
        this.isNextFirstPlayer = isNextFirstPlayer;
    }

    public int getTieBreakPoints() {
        return tieBreakPoints;
    }

    @Override
    public int compareTo(Player o) {
        if (o.getId() == id) return 0;
        if (o.getPoints() > points) return 1;
        if (o.getPoints() == points) {
            if (o.getTieBreakPoints() > tieBreakPoints) return 1;
            else if (o.getTieBreakPoints() < tieBreakPoints) return -1;
            else if (o.getId() > id) return -1;
        }
        return -1;
    }
}
