package es.pozoesteban.alberto.mvcrystals.model.player;

import es.pozoesteban.alberto.mvcrystals.model.elements.*;
import es.pozoesteban.alberto.mvcrystals.model.token_position.InBoard;
import es.pozoesteban.alberto.mvcrystals.model.token_position.InCenterPlate;

import java.util.ArrayList;
import java.util.List;

public class SlotsPanel extends ClonableElement {

    private final Slot[][] slots;
    private final Slot[] floor;
    private final GarbageSack garbageSack;
    private final CenterPlate centerPlate;

    private static int[] FLOOR_DISCOUNT_POINTS = new int[]{-1, -1, -2, -2, -2, -3, -3};

    public SlotsPanel(GarbageSack garbageSack, CenterPlate centerPlate) {
        super(false);
        this.garbageSack = garbageSack;
        this.centerPlate = centerPlate;
        slots = new Slot[5][];
        for (int i = 0; i < 5; i++) {
            slots[i] = new Slot[i + 1];
            for (int s = 0; s < slots[i].length; s++) {
                slots[i][s] = new Slot(true, isClone);
            }
        }
        floor = new Slot[7];
        for (int s = 0; s < floor.length; s++) {
            floor[s] = new Slot(false, isClone);
        }
    }
    public SlotsPanel(SlotsPanel toClone, GarbageSack garbageSack, CenterPlate centerPlate, List<Crystal> crystals, int playerIndex) {
        super(true);
        this.garbageSack = garbageSack;
        this.centerPlate = centerPlate;
        slots = new Slot[5][];
        for (int i = 0; i < 5; i++) {
            slots[i] = new Slot[i + 1];
            for (int s = 0; s < slots[i].length; s++) {
                slots[i][s] = new Slot(true, true);
                if (!toClone.slots[i][s].isEmpty()) {
                    for (Crystal crystal : crystals) {
                        if (toClone.slots[i][s].getToken().getId() == crystal.getId()) {
                            slots[i][s].put(crystal);
                            crystal.move(new InBoard(playerIndex, this.getClass().getSimpleName(), i, s));
                        }
                    }
                }
            }
        }
        floor = new Slot[7];
        for (int s = 0; s < floor.length; s++) {
            floor[s] = new Slot(false, true);
            if (!toClone.floor[s].isEmpty()) {
                for (Crystal crystal : crystals) {
                    if (toClone.floor[s].getToken().getId() == crystal.getId()) {
                        floor[s].put(crystal);
                        crystal.move(new InBoard(playerIndex, this.getClass().getSimpleName(), 5, s));
                    }
                }
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(System.getProperty("line.separator"));
        sb.append("SlotsPanel ");
        sb.append(System.getProperty("line.separator"));
        for (Slot[] row : slots) {
            for (Slot slot : row) {
                sb.append(slot.toString());
                sb.append("\t");
            }
            sb.append(System.getProperty("line.separator"));
        }
        sb.append("Floor: ");
        for (Slot slot : floor) {
            sb.append(slot).append("\t");
        }
        return sb.toString();
    }

    public List<Crystal> getAllCrystals() {
        List<Crystal> output = new ArrayList();
        for (Slot[] row : slots) {
            for (Slot slot : row) {
                if (!slot.isEmpty() && slot.getToken().isCrystal()) output.add((Crystal) slot.getToken());
            }
        }
        for (Slot slot : floor) {
            if (!slot.isEmpty() && slot.getToken().isCrystal()) output.add((Crystal) slot.getToken());
        }
        return output;
    }

    public Color getColor(int row) {
        for (Slot slot : slots[row]) {
            if (!slot.isEmpty()) return ((Crystal)slot.getToken()).getColor();
        }
        return null;
    }

    public boolean putCrystal(Crystal crystal, int row, int playerIndex) {
        Slot[] slots = this.slots[row];
        for (int i = slots.length-1; i >= 0; i--) {
            if (slots[i].isEmpty()) {
                slots[i].put(crystal);
                crystal.move(new InBoard(playerIndex, this.getClass().getSimpleName(), row, i));
                return false;
            }
        }
        return true;
    }

    public boolean putCrystalInFloor(Token token, int playerIndex) {
        for (int i = 0; i < floor.length; i++) {
            Slot slot = floor[i];
            if (slot.isEmpty()) {
                slot.put(token);
                token.move(new InBoard(playerIndex, getClass().getSimpleName(), 5, i));
                return true;
            }
        }
        return false;
    }

    public boolean isComplete(int row) {
        for (Slot slot : slots[row]) {
            if (slot.isEmpty()) return false;
        }
        return true;
    }

    public Crystal getOneAndThrowOthers(int row) {
        for (int i = 1; i < slots[row].length; i++) {
            garbageSack.put((Crystal) slots[row][i].getToken());
            slots[row][i].reset();
        }
        Crystal output = (Crystal) slots[row][0].getToken();
        slots[row][0].reset();
        return output;
    }

    public int discountFloorPoints() {
        int points = 0;
        for (int i = 0; i < floor.length; i++) {
            if (!floor[i].isEmpty()) {
                points += FLOOR_DISCOUNT_POINTS[i];
            } else {
                break;
            }
        }
        return points;
    }

    public boolean clearFloor() {
        boolean nextFirstPlayer = false;
        for(Slot slot : floor) {
            if (!slot.isEmpty()) {
                Token token = slot.getToken();
                if (token.isCrystal()) {
                    garbageSack.put((Crystal) token);
                }
                else {
                    int positionIndex = centerPlate.put(token);
                    token.move(new InCenterPlate(positionIndex));
                    nextFirstPlayer = true;
                }
                slot.reset();
            } else {
                break;
            }
        }
        return nextFirstPlayer;
    }

    public Slot[][] getSlots() {
        return slots;
    }

    public Slot[] getFloor() {
        return floor;
    }

    public String toLargeString() {
        StringBuilder sb = new StringBuilder(super.toString());
        sb.append(System.getProperty("line.separator"));
        sb.append("is clone: ").append(isClone()).append(System.getProperty("line.separator"));
        for(Slot[] row : slots) {
            for(Slot slot : row) {
                sb.append("[").append(slot.toOneCharString()).append("]").append(" ");
            }
            sb.append(System.getProperty("line.separator"));
        }
        return sb.toString();
    }

    public int numCrystalsInFloor() {
        int numCrystals = 0;
        for (Slot slot : floor) {
            if (!slot.isEmpty()) numCrystals++;
        }
        return numCrystals;
    }

    public int numCrystalsInRow(int row) {
        int numCrystals = 0;
        for (Slot slot : slots[row]) {
            if (!slot.isEmpty()) numCrystals++;
        }
        return numCrystals;
    }

    public int getColorAt(int i) {
        int row = 0;
        int column = 0;
        if (i > 0) {
            row = 1;
            column = i - 1;
        }
        if (i > 2) {
            row = 2;
            column = i - 3;
        }
        if (i > 5) {
            row = 3;
            column = i - 6;
        }
        if (i > 9) {
            row = 4;
            column = i - 10;
        }
        if (i > 14) {
            row = 5;
            column = i - 15;
        }
        if (row < 5) return slots[row][column].isEmpty() ? -1 : ((Crystal)slots[row][column].getToken()).getColor().getValue();
        else return floor[column].isEmpty() ? 0 : 1;
    }
}
