package es.pozoesteban.alberto.mvcrystals.model.player;

import java.util.Random;

import static es.pozoesteban.alberto.mvcrystals.Configuration.DICE;
import static es.pozoesteban.alberto.mvcrystals.Configuration.SEED;

public enum PlayerColor {

    BLUE(0), YELLOW(1), GREEN(2), RED(3), ORANGE(4), PURPLE(5), BROWN(6);

    private static final long serialVersionUID = 1L;

    private final int value;

    PlayerColor(int value) {
        this.value = value;
    }

    public static PlayerColor random() {
        return values()[DICE.nextInt(values().length)];
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        switch (value) {
            case 0:
                return "Azul";
            case 1:
                return "Amarillo";
            case 2:
                return "Verde";
            case 3:
                return "Rojo";
            case 4:
                return "Naranja";
            case 5:
                return "Morado";
            case 6:
                return "Marrón";
            default:
                return "Unknown";
        }
    }
}
