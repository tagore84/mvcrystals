package es.pozoesteban.alberto.mvcrystals.model.player;

import java.util.Date;

public class PlayerConfig {

    private final int id;
    private final PlayerType type;
    private final ConcretePlayer player;
    private final String name;
    private final PlayerColor color;

    public RandomAIPlayerConfig randomAIPlayerConfig;
    public DeepSpaceConfig deepSpaceConfig;
    public MaximinConfig maximinConfig;

    public static PlayerConfig createHumanPlayerConfig(int id, String name, PlayerColor color) {
        return new PlayerConfig(id, PlayerType.HUMAN, ConcretePlayer.HUMAN, name, color);
    }
    public static PlayerConfig createJuanitoSemillasPlayerConfig(int id, String name, PlayerColor color) {
        PlayerConfig playerConfig = new PlayerConfig(id, PlayerType.JUANITO_SEMILLAS, ConcretePlayer.JUANITO_SEMILLAS, name, color);
        playerConfig.randomAIPlayerConfig = new RandomAIPlayerConfig(name.hashCode());
        return playerConfig;
    }
    public static PlayerConfig createJuanitoElAleatorioPlayerConfig(int id, String name, PlayerColor color) {
        PlayerConfig playerConfig = new PlayerConfig(id, PlayerType.JUANITO_EL_ALEATORIO, ConcretePlayer.JUANITO_EL_ALEATORIO, name, color);
        playerConfig.randomAIPlayerConfig = new RandomAIPlayerConfig(new Date().getTime());
        return playerConfig;
    }
    public static PlayerConfig createMaximinlian(int id, String name, int maxSpace, float maxTimeInSeconds, float numCrystalsFactor, float ruinFactor, PlayerColor color) {
        PlayerConfig playerConfig = new PlayerConfig(id, PlayerType.MAXIMINLIAN, ConcretePlayer.MAXI, name, color);
        playerConfig.maximinConfig = new MaximinConfig(maxSpace, maxTimeInSeconds, numCrystalsFactor, ruinFactor);

        return playerConfig;
    }
    public static PlayerConfig createLillo(int id, String name, PlayerColor color) {
        PlayerConfig playerConfig = new PlayerConfig(id, PlayerType.LILLO_EL_EXPERTILLO, ConcretePlayer.LILLO, name, color);
        return playerConfig;
    }

    private PlayerConfig(int id, PlayerType type, ConcretePlayer player, String name, PlayerColor color) {
        this.id = id;
        this.type = type;
        this.player = player;
        this.name = name;
        this.color = color;
    }

    public PlayerType getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public PlayerColor getColor() {
        return color;
    }

    public ConcretePlayer getConcretePlayer() {
        return player;
    }

    public static class RandomAIPlayerConfig {
        public final long seed;

        protected RandomAIPlayerConfig(long seed) {
            this.seed = seed;
        }
    }

    public static class DeepSpaceConfig {
        public int maxDeep;
        public int maxSpace;
        public float numCrystalsFactor;

        private DeepSpaceConfig(int maxDeep, int maxSpace, float numCrystalsFactor) {
            this.maxDeep = maxDeep;
            this.maxSpace = maxSpace;
            this.numCrystalsFactor = numCrystalsFactor;
        }
    }
    public static class MaximinConfig {
        public int maxSpace;
        public float maxTimeInSeconds;
        public float numCrystalsFactor;
        public float ruinFactor;

        //maxSpace, int maxNumNodes, float numCrystalsFactor, float ruinFactor
        private MaximinConfig(int maxSpace, float maxTimeInSeconds, float numCrystalsFactor, float ruinFactor) {
            this.maxSpace = maxSpace;
            this.maxTimeInSeconds = maxTimeInSeconds;
            this.numCrystalsFactor = numCrystalsFactor;
            this.ruinFactor = ruinFactor;
        }
    }
}
