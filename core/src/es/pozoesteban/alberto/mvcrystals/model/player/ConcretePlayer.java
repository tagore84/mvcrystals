package es.pozoesteban.alberto.mvcrystals.model.player;

public enum ConcretePlayer {

    NONE(-1),
    HUMAN(0),
    JUANITO_EL_ALEATORIO(1), JUANITO_SEMILLAS(2),
    MAXI(3),
    EGO(4),
    ORTEGA(5),
    LILLO(6);

    private static final long serialVersionUID = 1L;

    private final int value;

    ConcretePlayer(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }


    @Override
    public String toString() {
        switch (value) {
            case -1:
                return "Vacío";
            case 0:
                return "Jugador";
            case 3:
                return "(IA) Difícil";
            case 4:
                return "(IA) Medio";
            case 5:
                return "(IA) Puñetera";
            case 6:
                return "(IA) Media";
            default:
                return "Unknown";
        }
    }
}
