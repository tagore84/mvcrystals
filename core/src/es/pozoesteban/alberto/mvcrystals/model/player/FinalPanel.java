package es.pozoesteban.alberto.mvcrystals.model.player;

import es.pozoesteban.alberto.mvcrystals.model.elements.ClonableElement;
import es.pozoesteban.alberto.mvcrystals.model.elements.Color;
import es.pozoesteban.alberto.mvcrystals.model.elements.Crystal;
import es.pozoesteban.alberto.mvcrystals.model.elements.Slot;
import es.pozoesteban.alberto.mvcrystals.model.token_position.InBoard;

import java.util.ArrayList;
import java.util.List;


public class FinalPanel extends ClonableElement {

    private final Slot[][] slots;

    public FinalPanel() {
        super(false);
        this.slots = new Slot[5][5];
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                slots[i][j] = new Slot(Color.values()[(5 + j - i) % 5], isClone);
            }
        }
    }
    public FinalPanel(FinalPanel toClone, List<Crystal> crystals, int playerIndex) {
        super(true);
        this.slots = new Slot[5][5];
        for (int i = 0; i < toClone.getSlots().length; i++) {
            for (int j = 0; j < toClone.getSlots()[i].length; j++) {
                this.slots[i][j] = new Slot(Color.values()[(5 + j - i) % 5], true);
                for (Crystal crystal : crystals) {
                    if (!toClone.slots[i][j].isEmpty() && toClone.slots[i][j].getToken().getId() == crystal.getId()) {
                        this.slots[i][j].put(crystal);
                        crystal.move(new InBoard(playerIndex, toClone.getClass().getSimpleName(), i, j));
                        break;
                    }
                }
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(Integer.toHexString(hashCode()));
        sb.append(" FinalPanel:");
        sb.append(System.getProperty("line.separator"));
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                sb.append(slots[i][j].toString());
                sb.append("\t");
            }
            sb.append(System.getProperty("line.separator"));
        }
        return sb.toString();
    }

    public Slot[][] getSlots() {
        return slots;
    }

    public List<Crystal> getAllCrystals() {
        List<Crystal> output = new ArrayList();
        for (Slot[] row : slots) {
            for (Slot slot : row) {
                if (!slot.isEmpty() && slot.getToken().isCrystal()) output.add((Crystal) slot.getToken());
            }
        }
        return output;
    }

    public boolean hasColorInRow(int row, Color color) {
        return !slots[row][(color.getValue() + row) % 5].isEmpty();
    }

    public void put(Crystal crystal, int row, int playerIndex) {
        int column = (crystal.getColor().getValue() + row) % 5;
        slots[row][column].put(crystal);
        crystal.move(new InBoard(playerIndex, this.getClass().getSimpleName(), row, column));
    }

    public int finalPhasePoints(Crystal crystal, int row) {
        int points = 1;
        boolean vertical = false;
        boolean horizontal = false;
        int column = (crystal.getColor().getValue() + row) % 5;
        // Left
        for (int i = column-1; i >= 0; i--) {
            if (!slots[row][i].isEmpty()) {
                points++;
                horizontal = true;
            } else {
                break;
            }
        }
        // Right
        for (int i = column+1; i < slots[row].length; i++) {
            if (!slots[row][i].isEmpty()) {
                points++;
                horizontal = true;
            } else {
                break;
            }
        }
        // Up
        for (int i = row-1; i >= 0; i--) {
            if (!slots[i][column].isEmpty()) {
                points++;
                vertical = true;
            } else {
                break;
            }
        }
        // Down
        for (int i = row+1; i < slots.length; i++) {
            if (!slots[i][column].isEmpty()) {
                points++;
                vertical = true;
            } else {
                break;
            }
        }
        return points + ((horizontal && vertical) ? 1 : 0);
    }
    public int[] finalGamePoints() {
        int points = 0;
        int tieBreakPoints = 0;
        // Complete rows...
        for (int i = 0; i < slots.length; i++) {
            boolean complete = true;
            for (int j = 0; j < slots[i].length; j++) {
                if (slots[i][j].isEmpty()) {
                    complete = false;
                    break;
                }
            }
            if (complete) {
                points += 5;
                tieBreakPoints += 1;
            }
        }

        // Complete columns...
        for (int j = 0; j < slots[0].length; j++) {
            boolean complete = true;
            for (int i = 0; i < slots.length; i++) {
                if (slots[i][j].isEmpty()) {
                    complete = false;
                    break;
                }
            }
            if (complete) points += 7;
        }

        // Color groups...
        for (Color color : Color.values()) {
            points += (numCrystals(color)/5) * 10;
        }
        return new int[]{points, tieBreakPoints};
    }

    private int numCrystals(Color color) {
        int total = 0;
        for (Crystal crystal : getAllCrystals()) {
            if (crystal.getColor() == color) total++;
        }
        return total;
    }

    public boolean hasFinished() {
        for (int i = 0; i < slots.length; i++) {
            boolean complete = true;
            for (int j = 0; j < slots[i].length; j++) {
                if (slots[i][j].isEmpty()) {
                    complete = false;
                    break;
                }
            }
            if (complete) return true;
        }
        return false;
    }

    public String toLargeString() {
        StringBuilder sb = new StringBuilder();
        for(Slot[] row : slots) {
            for(Slot slot : row) {
                sb.append("[").append(slot.toOneCharString()).append("]").append(" ");
            }
            sb.append(System.getProperty("line.separator"));
        }
        return sb.toString();
    }
}
