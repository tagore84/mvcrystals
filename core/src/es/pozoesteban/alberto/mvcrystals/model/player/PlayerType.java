package es.pozoesteban.alberto.mvcrystals.model.player;

public enum PlayerType {

    NONE(-1), HUMAN(0), JUANITO_EL_ALEATORIO(1), JUANITO_SEMILLAS(2), MAXIMINLIAN(3), LILLO_EL_EXPERTILLO(4);

    private static final long serialVersionUID = 1L;

    private final int value;

    PlayerType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
