package es.pozoesteban.alberto.mvcrystals.model;

import es.pozoesteban.alberto.mvcrystals.Configuration;
import es.pozoesteban.alberto.mvcrystals.model.elements.*;
import es.pozoesteban.alberto.mvcrystals.model.player.Player;
import es.pozoesteban.alberto.mvcrystals.model.player.PlayerConfig;
import es.pozoesteban.alberto.mvcrystals.model.token_position.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static es.pozoesteban.alberto.mvcrystals.Configuration.MODEL_LOGGER;

public class GameState extends ClonableElement {

    public final static int NUM_CRYSTALS_BY_COLOR = 20;

    public final static Random DICE = new Random(Configuration.SEED);

    private final Player[] players;
    private final Sack sack;
    private final GarbageSack garbageSack;
    private final Factory[] factories;
    private final CenterPlate centerPlate;
    private final int numPlayers;
    private final TokenWarehouse[] tokenWarehouses;
    private final FirstPlayerToken firstPlayerToken;
    private final List<Crystal> crystals;

    private int phase;
    private float turn;
    private Player currentPlayer;

    private List<Crystal> selectedCrystals;
    private List<Crystal> selectedCrystalsPartners;
    private boolean finish;
    private Player winner;

    public GameState (PlayerConfig[] playerConfigs) {
        super(false);
        this.finish = false;
        this.turn = 0;
        this.phase = 0;
        this.firstPlayerToken = new FirstPlayerToken();
        crystals = new ArrayList<>();
        int idCount = 1;
        for (Color color : Color.values()) {
            for (int i = 0; i < NUM_CRYSTALS_BY_COLOR; i++) {
                crystals.add(new Crystal(idCount++, color, new InSack(), isClone, false));
            }
        }
        sack = new Sack(crystals);

        selectedCrystals = new ArrayList();
        selectedCrystalsPartners = new ArrayList();
        players = new Player[playerConfigs.length];
        garbageSack = new GarbageSack();
        int numPlayers = 0;
        for (int i = 0; i < playerConfigs.length; i++) {
            if (playerConfigs[i] != null) {
                numPlayers++;
            }
        }
        this.numPlayers = numPlayers;
        int numFactories = 0;
        switch (this.numPlayers) {
            case 2:
                numFactories = 5;
                break;
            case 3:
                numFactories = 7;
                break;
            case 4:
                numFactories = 9;
                break;
        }
        centerPlate = new CenterPlate(numFactories, firstPlayerToken);
        for (int i = 0; i < playerConfigs.length; i++) {
            if (playerConfigs[i] != null) {
                players[i] = new Player(playerConfigs[i].getId(), playerConfigs[i].getName(),
                        playerConfigs[i].getType(), garbageSack, centerPlate, playerConfigs[i].getColor());
                numPlayers++;
            } else {
                players[i] = null;
            }
        }

        tokenWarehouses = new TokenWarehouse[numFactories + 1];
        factories = new Factory[numFactories];
        for (int factoryIndex = 0; factoryIndex < numFactories; factoryIndex++) {
            Factory factory = new Factory(factoryIndex);
            factories[factoryIndex] = factory;
            tokenWarehouses[factoryIndex] = factory;
        }
        tokenWarehouses[numFactories] = centerPlate;
        phase = 0;
        turn = 0f;

        while(currentPlayer == null) {
            currentPlayer = players[DICE.nextInt(players.length)];
        }
        MODEL_LOGGER.info(currentPlayer + " starts the game!");
        putCrystalsInFactories();
    }

    public void putCrystalsInFactories() {
        if (this.sack.size() < (factories.length * 4)) refillSack();
        for (Factory factory : factories) {
            for (int i = 0; i < factory.getSlots().length; i++) {
                Slot slot = factory.getSlots()[i];
                Crystal crystal = this.sack.takeRandomCrystal();
                if (crystal == null) break;
                slot.put(crystal);
                crystal.move(new InFactory(factory.getId(), i));
            }
        }
    }

    private void refillSack() {
        sack.refill(garbageSack.getCrystals());
        garbageSack.clear();
    }

    public int getNumPlayers() {
        return numPlayers;
    }

    public int getNumFactories() {
        return factories.length;
    }

    public boolean isCurrentPlayer(int playerIndex) {
        return players[playerIndex] != null && players[playerIndex] == currentPlayer;
    }

    public Player getPlayer(int id) {
        for (Player player : players) {
            if (player != null && player.getId() == id) return player;
        }
        return null;
    }

    public TokenPosition tokenPosition(int id) {
        return tokenPosition(id, null);
    }

    public TokenPosition tokenPosition(int id, TokenPosition cache) {
        TokenPosition position = null;

        if (cache != null) {
            if (cache instanceof InBoard) {
                position = findInBoard(((InBoard) cache).getPlayerIndex(), id);
            }
            else if (cache instanceof InSack) {
                position = findInSack(id);
            }
            else if (cache instanceof InFactory){
                position = findInFactory(((InFactory) cache).getFactoryIndex(), id);
            }
            else if (cache instanceof InGarbage) {
                position = findInGarbage(id);
            }
            else if (cache instanceof InCenterPlate) {
                position = findInCenterPlate(id);
            }
            if (position != null) {
                return position;
            }
        }

        for (int playerIndex = 0; playerIndex < players.length; playerIndex++) {
            position = findInBoard(playerIndex, id);
            if (position != null) return position;
        }

        position = findInCenterPlate(id);
        if (position != null) return position;

        position = findInSack(id);
        if (position != null) return position;

        position = findInGarbage(id);
        if (position != null) return position;

        for (int factoryIndex = 0; factoryIndex < factories.length; factoryIndex++) {
            position = findInFactory(factoryIndex, id);
            if (position != null) return position;
        }
        throw new IllegalArgumentException("Token not found id: " + id);
    }

    private TokenPosition findInCenterPlate(int id) {
        for (int slotIndex = 0; slotIndex < centerPlate.numSlots(); slotIndex++) {
            Slot slot = centerPlate.getSlots()[slotIndex];
            if (slot.getToken() != null && slot.getToken().getId() == id) {
                return new InCenterPlate(slotIndex);
            }
        }
        return null;
    }

    private TokenPosition findInSack(int id) {
        if (sack.hasCrystal(id)) return new InSack();
        return null;
    }

    private TokenPosition findInGarbage(int id) {
        for (Token token : garbageSack.getCrystals()) {
            if (token.getId() == id) return new InGarbage();
        }
        return null;
    }
    private TokenPosition findInFactory(int factoryIndex, int id) {
        for (int slotIndex = 0; slotIndex < factories[factoryIndex].numSlots(); slotIndex++) {
            Slot slot = factories[factoryIndex].getSlots()[slotIndex];
            if (slot.getToken() != null && slot.getToken().getId() == id) {
                return new InFactory(factoryIndex, slotIndex);
            }
        }
        return null;
    }

    private TokenPosition findInBoard(int playerIndex, int id) {
        Player player = players[playerIndex];
        if (player != null) {
            for (int row = 0; row < player.getFinalPanel().getSlots().length; row++) {
                for (int column = 0; column <  player.getFinalPanel().getSlots()[row].length; column++) {
                    Slot slot = player.getFinalPanel().getSlots()[row][column];
                    if (slot.getToken() != null && slot.getToken().getId() == id) {
                        return new InBoard(playerIndex, player.getFinalPanel().getClass().getSimpleName(), row, column);
                    }
                }
            }
            for (int row = 0; row < player.getSlotsPanel().getSlots().length; row++) {
                for (int column = 0; column <  player.getSlotsPanel().getSlots()[row].length; column++) {
                    Slot slot = player.getSlotsPanel().getSlots()[row][column];
                    if (slot.getToken() != null && slot.getToken().getId() == id) {
                        return new InBoard(playerIndex, player.getSlotsPanel().getClass().getSimpleName(), row, column);
                    }
                }
            }
            for (int column = 0; column < player.getSlotsPanel().getFloor().length; column++) {
                Slot slot = player.getSlotsPanel().getFloor()[column];
                if (!slot.isEmpty()) {
                    if (slot.getToken().getId() == id) {
                        return new InBoard(playerIndex,
                                player.getSlotsPanel().getClass().getSimpleName(),
                                5,
                                column);
                    }
                }
            }
        }
        return null;
    }

    // Getters

    public Player[] getPlayers() {
        return players;
    }

    public Sack getSack() {
        return sack;
    }

    public GarbageSack getGarbageSack() {
        return garbageSack;
    }

    public Factory[] getFactories() {
        return factories;
    }

    public CenterPlate getCenterPlate() {
        return centerPlate;
    }

    public int getPhase() {
        return phase;
    }

    public int getTurn() {
        return (int)turn;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public Factory getFactory(int factoryIndex) {
        return factories[factoryIndex];
    }

    public boolean crystalIsSelected(int id) {
        List<Crystal> copy = new ArrayList();
        copy.addAll(selectedCrystals);
        for (Crystal crystal : copy) {
            if (crystal != null && crystal.getId() == id) return true;
        }
        return false;
    }

    public List<Crystal> getCrystals() {
        return crystals;
    }

    public boolean isRowSelectable(int row) {
        if (!someCrystalSelected()) return false;
        try {
            Color color = getColorSelected();
            return isRowSelectable(row, color);
        } catch (IllegalArgumentException ex) {
            MODEL_LOGGER.warning("Exception in isRowSelectable with row: " + row + " -> " + ex.toString());
        }
        return false;
    }

    public boolean isRowSelectable(int row, Color color) {
        if (row == 5) return true;
        return currentPlayer.selectableRow(row, color);
    }

    private Color getColorSelected() {
        for (TokenWarehouse tokenWarehouse : tokenWarehouses) {
            for (Crystal crystal : tokenWarehouse.getAllCrystals()) {
                if (crystal.isSelected()) return crystal.getColor();
            }
        }
        throw new IllegalArgumentException("No crystal found!");
    }

    public void setSelectedCrystals(List<Crystal> selectedCrystals) {
        resetSelectedCrystals();
        for (Crystal crystal : selectedCrystals) {
            crystal.isSelected(true);
        }
        this.selectedCrystals.addAll(selectedCrystals);
    }
    public void setSelectedCrystalPartners(List<Crystal> partners) {
        resetSelectedCrystalPartners();
        this.selectedCrystalsPartners.addAll(partners);
    }
    public void resetSelectedCrystals() {
        for (Crystal crystal : selectedCrystals) {
            crystal.isSelected(false);
        }
        this.selectedCrystals.clear();
    }
    public void resetSelectedCrystalPartners() {
        this.selectedCrystalsPartners.clear();
    }
    public boolean someCrystalSelected() {
        return !selectedCrystals.isEmpty();
    }
    public int numCrystalSelected() {
        return selectedCrystals.size();
    }

    public List<Crystal> getSelectedCrystals() {
        return selectedCrystals;
    }

    public void putSelectedCrystalsPartnersIntoCenterPlate() {
        for (Crystal partner : selectedCrystalsPartners) {
            for (Factory factory : factories) {
                for (Slot slot : factory.getSlots()) {
                    if (!slot.isEmpty() && slot.getToken().equals(partner)) slot.reset();
                }
            }
            int positionIndex = centerPlate.put(partner);
            partner.move(new InCenterPlate(positionIndex));
        }
    }

    public void putSelectedCrystalsIntoPlayerBoard(int row) {
        boolean thisTokenWarehouse = false; // Una vez encontrado un cristal, todos los demás están en el mismo sitio :)
        for (TokenWarehouse tokenWarehouse : tokenWarehouses) {
            for (Crystal crystal : selectedCrystals) {
                for (Slot slot : tokenWarehouse.getSlots()) {
                    if (!slot.isEmpty() && slot.getToken().equals(crystal)) {
                        thisTokenWarehouse = true;
                        slot.reset();
                        boolean crystalPut = currentPlayer.putCrystal(crystal, row);
                        if (!crystalPut) {
                            garbageSack.put(crystal);
                        }
                    }
                }
            }
            if (thisTokenWarehouse) return;
        }
    }

    public boolean noMoreCrystalsInTokenWarehouse() {
        boolean noMore = true;
        for (TokenWarehouse tokenWarehouse : tokenWarehouses) {
            if (!tokenWarehouse.isEmpty()) return false;
        }
        return true;
    }
    public Player whoIsNextPlayer() {
        int index = currentPlayer.getId();
        Player nextPlayer = null;
        while (nextPlayer == null) {
            index = (index + 1) % players.length;
            nextPlayer = players[index];
        }
        return nextPlayer;
    }
    public void nextPlayer(boolean phaseFinished) {
        if (phaseFinished) {
            turn = 0;
            phase += 1;
            for (Player player : players) {
                if (player != null && player.isNextFirstPlayer()) {
                    currentPlayer = player;
                }
            }
        } else {
            turn += 1.0/(float)numPlayers;
            currentPlayer = whoIsNextPlayer();
        }
    }

    public TokenWarehouse[] getTokenWarehouses() {
        return tokenWarehouses;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(super.toString());
        if (isClone()) sb.append(" clone ");
        for (TokenWarehouse tokenWarehouse : tokenWarehouses) {
            sb.append(System.getProperty("line.separator")).append(tokenWarehouse.toString());
        }
        sb.append(System.getProperty("line.separator"));
        sb.append(sack.toString());
        sb.append(System.getProperty("line.separator"));
        sb.append(garbageSack.toString());
        for (Player player : players) {
            if (player != null) {
                sb.append(player.getName()).append(System.getProperty("line.separator"));
                sb.append(player.getSlotsPanel().toLargeString()).append(System.getProperty("line.separator"));
                sb.append(player.getFinalPanel().toLargeString()).append(System.getProperty("line.separator"));
            }
        }
        return sb.toString();
    }

    public boolean takingFromCenterPlate() {
        boolean takenFirstPlayerToke = !firstPlayerToken.isTaken() && findInCenterPlate(selectedCrystals.get(0).getId()) != null;
        if (takenFirstPlayerToke) firstPlayerToken.isTaken(true);
        return takenFirstPlayerToke;
    }

    public void takeFirstPlayerToken() {
        for (Player player : players) {
            if (currentPlayer == player) {
                player.isNextFirstPlayer(true);
                if (currentPlayer.takeFirstPlayerToken(firstPlayerToken)) {
                    for (Slot slot : centerPlate.getSlots()) {
                        if (!slot.isEmpty() && slot.getToken().equals(firstPlayerToken)) {
                            slot.reset();
                            break;
                        }
                    }
                }
            } else if (player != null) {
                player.isNextFirstPlayer(true);
            }
        }

    }


    public boolean thereAreSomeHuman() {
        for (Player player : players) {
            if (player != null && player.isHuman()) return true;
        }
        return false;
    }

    public FirstPlayerToken getFirstPlayerToken() {
        return firstPlayerToken;
    }


    public GameState(GameState toClone) {
        super(true);
        this.finish = toClone.finish;
        this.crystals = new ArrayList<>();
        for (Crystal crystal : toClone.getCrystals()) {
            this.crystals.add(new Crystal(crystal.getId(), crystal.getColor(), crystal.getPosition().clone(), true, crystal.isSelected()));
        }
        this.firstPlayerToken = new FirstPlayerToken(toClone.firstPlayerToken);
        this.garbageSack = new GarbageSack(toClone.garbageSack, crystals);
        this.centerPlate = new CenterPlate(toClone.centerPlate, firstPlayerToken, crystals);
        this.players = new Player[toClone.players.length];
        for (int i = 0; i < toClone.players.length; i++) {
            this.players[i] = toClone.players[i] == null ? null : new Player(toClone.players[i], this.garbageSack, this.centerPlate, crystals);
        }
        this.sack = new Sack(toClone.sack, crystals);
        this.factories = new Factory[toClone.factories.length];
        this.tokenWarehouses = new TokenWarehouse[this.factories.length + 1];
        for (int i = 0; i < toClone.factories.length; i++) {
            this.factories[i] = new Factory(toClone.factories[i], crystals);
            this.tokenWarehouses[i] = this.factories[i];
        }

        this.tokenWarehouses[this.factories.length] = this.centerPlate;
        this.numPlayers = toClone.numPlayers;
        this.phase = toClone.phase;
        this.turn = toClone.turn;

        for (int i = 0; i < toClone.players.length; i++) {
            if (toClone.players[i] == toClone.currentPlayer) {
                this.currentPlayer = players[i];
                break;
            }
        }
        this.selectedCrystals = new ArrayList();
        for (Crystal toCloneCrystal : toClone.selectedCrystals) {
            for (Crystal crystal : crystals) {
                if (toCloneCrystal.getId() == crystal.getId()) {
                    this.selectedCrystals.add(crystal);
                    break;
                }
            }
        }
        this.selectedCrystalsPartners = new ArrayList();
        for (Crystal toCloneCrystal : toClone.selectedCrystalsPartners) {
            for (Crystal crystal : crystals) {
                if (toCloneCrystal.getId() == crystal.getId()) {
                    this.selectedCrystalsPartners.add(crystal);
                    break;
                }
            }
        }
    }

    private Token getCrystal(int id) {
        for (Token crystal : getCrystals()) {
            if (crystal.getId() == id) return crystal;
        }
        throw new IllegalArgumentException("Crystal not found id: " + id);
    }

    public void setCurrentPlayer(Player player) {
        currentPlayer = player;
    }

    public boolean crystalInSlotPanel(int crystalId) {
        return getSlotPanelRow(crystalId) >= 0;
    }
    public int getSlotPanelRow(int crystalId) {
        for (int i = 0; i < currentPlayer.getSlotsPanel().getSlots().length; i++) {
            Slot[] slots = currentPlayer.getSlotsPanel().getSlots()[i];
            for (Slot slot : slots) {
                if (!slot.isEmpty() && slot.getToken().getId() == crystalId) {
                    return i;
                }
            }
        }
        return -1;
    }

    //<editor-fold desc="ReinformentLearningMethods Testing...">
    public int[][] toMultiInputVector() {
        int dimensionFactories = (getNumFactories() + 1) * 5;
        int dimensionSlotBoards = 15 + 7;
        int dimensionFinalBoard = 25;
        int[] factoriesOutput = new int[dimensionFactories];
        int[] ownBoardOutput = new int[(dimensionFinalBoard + dimensionSlotBoards)];
        int[] otherBoardOutput = new int[(dimensionFinalBoard + dimensionSlotBoards)];
        for (Factory factory : factories) {
            for (Color color : Color.values()) {
                factoriesOutput[factory.getId() * 5 + color.getValue()] = factory.getCrystals(color).size();
            }
        }
        for (Color color : Color.values()) {
            factoriesOutput[getNumFactories() * 5 + color.getValue()] = centerPlate.getCrystals(color).size();
        }
        for (int i = 0; i < dimensionSlotBoards; i++) {
            ownBoardOutput[i] = currentPlayer.getSlotsPanel().getColorAt(i-dimensionFactories);
        }
        for (int row = 0; row < 5; row++) {
            for (int column = 0; column < 5; column++) {
                ownBoardOutput[dimensionSlotBoards + (5*row) + column] = currentPlayer.getFinalPanel().getSlots()[row][column].isEmpty() ? 0 : 1;
            }
        }
        for (Player player : players) {
            if (player != null && !player.equals(currentPlayer)) {
                for (int i = 0; i < dimensionSlotBoards; i++) {
                    otherBoardOutput[i] = player.getSlotsPanel().getColorAt(i-dimensionFactories);
                }
                for (int row = 0; row < 5; row++) {
                    for (int column = 0; column < 5; column++) {
                        otherBoardOutput[dimensionSlotBoards + (5*row) + column] = player.getFinalPanel().getSlots()[row][column].isEmpty() ? 0 : 1;
                    }
                }
            }
        }
        return new int[][]{factoriesOutput, ownBoardOutput, otherBoardOutput};
    }
    public  int[] toVector() {
        int dimensionFactories = (getNumFactories() + 1) * 5;
        int dimensionSlotBoards = 15 + 7;
        int dimensionFinalBoard = 25;
        int[] output = new int[dimensionFactories + ((dimensionFinalBoard + dimensionSlotBoards) * numPlayers)];
        for (Factory factory : factories) {
            for (Color color : Color.values()) {
                output[factory.getId() * 5 + color.getValue()] = factory.getCrystals(color).size();
            }
        }
        for (Color color : Color.values()) {
            output[getNumFactories() * 5 + color.getValue()] = centerPlate.getCrystals(color).size();
        }
        for (int i = dimensionFactories; i < (dimensionFactories + dimensionSlotBoards); i++) {
            output[i] = currentPlayer.getSlotsPanel().getColorAt(i-dimensionFactories);
        }
        int index = dimensionFactories + dimensionSlotBoards;
        for (Player player : players) {
            if (player != null && !player.equals(currentPlayer)) {
                for (int i = 0; i < dimensionSlotBoards; i++) {
                    output[index + i] = player.getSlotsPanel().getColorAt(i);
                }
                index += dimensionSlotBoards;
            }
        }
        for (int row = 0; row < 5; row++) {
            for (int column = 0; column < 5; column++) {
                output[index + (5*row) + column] = currentPlayer.getFinalPanel().getSlots()[row][column].isEmpty() ? 0 : 1;
            }
        }
        index += dimensionFinalBoard;
        for (Player player : players) {
            if (player != null && !player.equals(currentPlayer)) {
                for (int row = 0; row < 5; row++) {
                    for (int column = 0; column < 5; column++) {
                        output[index + (5*row) + column] = player.getFinalPanel().getSlots()[row][column].isEmpty() ? 0 : 1;
                    }
                }
                index += dimensionFinalBoard;
            }
        }
        return output;
    }

    public boolean isFinish() {
        return finish;
    }

    public void isFinish(boolean b) {
        this.finish = b;
    }

    public void setWinner(Player winner) {
        this.winner = winner;
    }
    public Player getWinner() {
        return this.winner;
    }
    //</editor-fold>
}
