package es.pozoesteban.alberto.mvcrystals.model.token_position;

import java.util.Objects;

public class InBoard extends TokenPosition {

    private final int playerIndex;
    private final String boardPlace;
    private final int row;
    private final int column;


    public InBoard(int playerIndex, String boardPlace, int row, int column) {
        this.playerIndex = playerIndex;
        this.boardPlace = boardPlace;
        this.row = row;
        this.column = column;
    }

    public int getPlayerIndex() {
        return playerIndex;
    }

    public String getBoardPlace() {
        return boardPlace;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    @Override
    public TokenPosition clone() {
        return new InBoard(playerIndex, boardPlace, row, column);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof InBoard)) return false;
        InBoard inBoard = (InBoard) o;
        return playerIndex == inBoard.playerIndex &&
                row == inBoard.row &&
                column == inBoard.column &&
                boardPlace.equals(inBoard.boardPlace);
    }

    @Override
    public int hashCode() {
        return Objects.hash(playerIndex, boardPlace, row, column);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("In board's player ").append(playerIndex).append(" in ").append(boardPlace);
        sb.append(" row ").append(row).append( " column ").append(column);
        return sb.toString();
    }
}
