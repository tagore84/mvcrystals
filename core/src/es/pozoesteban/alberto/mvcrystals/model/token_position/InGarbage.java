package es.pozoesteban.alberto.mvcrystals.model.token_position;

public class InGarbage extends TokenPosition {

    @Override
    public TokenPosition clone() {
        return new InGarbage();
    }

    @Override
    public int hashCode() {
        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof InGarbage;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("In Garbage");
        return sb.toString();
    }
}
