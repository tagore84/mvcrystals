package es.pozoesteban.alberto.mvcrystals.model.token_position;

import java.util.Objects;

public class InCenterPlate extends TokenPosition {

    private final int positionIndex;

    public InCenterPlate(int positionIndex) {
        this.positionIndex = positionIndex;
    }

    public int getPositionIndex() {
        return positionIndex;
    }

    @Override
    public TokenPosition clone() {
        return new InCenterPlate(positionIndex);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof InCenterPlate)) return false;
        InCenterPlate that = (InCenterPlate) o;
        return positionIndex == that.positionIndex;
    }

    @Override
    public int hashCode() {
        return Objects.hash(positionIndex);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("In Center Plate");
        return sb.toString();
    }

}
