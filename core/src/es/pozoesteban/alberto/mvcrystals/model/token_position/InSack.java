package es.pozoesteban.alberto.mvcrystals.model.token_position;

public class InSack extends TokenPosition {

    @Override
    public TokenPosition clone() {
        return new InSack();
    }

    @Override
    public int hashCode() {
        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof InSack;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("In Sack");
        return sb.toString();
    }
}
