package es.pozoesteban.alberto.mvcrystals.model.token_position;

import java.util.Objects;

public class InFactory extends TokenPosition {

    private final int factoryIndex;
    private final int positionIndex;


    public InFactory(int factoryIndex, int positionIndex) {
        this.factoryIndex = factoryIndex;
        this.positionIndex = positionIndex;
    }

    public int getFactoryIndex() {
        return factoryIndex;
    }

    public int getPositionIndex() {
        return positionIndex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof InFactory)) return false;
        InFactory inFactory = (InFactory) o;
        return factoryIndex == inFactory.factoryIndex &&
                positionIndex == inFactory.positionIndex;
    }

    @Override
    public int hashCode() {
        return Objects.hash(factoryIndex, positionIndex);
    }

    @Override
    public TokenPosition clone() {
        return new InFactory(this.factoryIndex, this.positionIndex);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("In factory ").append(factoryIndex).append(" position ").append(positionIndex);
        return sb.toString();
    }
}
