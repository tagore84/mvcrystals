package es.pozoesteban.alberto.mvcrystals.model.token_position;

public abstract class TokenPosition {

    @Override
    public abstract String toString();

    public abstract TokenPosition clone();

    @Override
    public abstract int hashCode();

    @Override
    public abstract boolean equals(Object obj);
}
