package es.pozoesteban.alberto.mvcrystals.view.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import es.pozoesteban.alberto.mvcrystals.controller.MVCrystalsGameController;
import es.pozoesteban.alberto.mvcrystals.model.player.PlayerColor;
import es.pozoesteban.alberto.mvcrystals.model.player.PlayerConfig;
import es.pozoesteban.alberto.mvcrystals.view.LayoutUtils;
import es.pozoesteban.alberto.mvcrystals.view.actors.BackgroundImage;
import es.pozoesteban.alberto.mvcrystals.view.actors.DisposableImage;

import java.util.ArrayList;
import java.util.List;

import static es.pozoesteban.alberto.mvcrystals.Configuration.SCREEN_DEBUG;

public class WelcomeScreen extends BaseScreen {

    private final List<TextButton> buttons;
    private final List<Label> labels;
    protected final List<DisposableImage> allBackgroundActors;
    protected final List<DisposableImage> allForegroundActors;
    protected BackgroundImage backgroundImage;

    public WelcomeScreen(MVCrystalsGameController controller) {
        super(controller, null);
        buttons = new ArrayList();
        labels = new ArrayList();
        allBackgroundActors = new ArrayList();
        allForegroundActors = new ArrayList();
    }

    protected void createActors() {
        buttons.clear();
        labels.clear();
        allBackgroundActors.clear();
        allForegroundActors.clear();
        // Background
        backgroundImage = new BackgroundImage();
        allBackgroundActors.add(backgroundImage);

        // Tittle
        Label tittle = new Label("CRISTALES", SKIN, "tittle");
        LayoutUtils.setSizeByWidthPercentage(tittle, 0.95f, 8f);
        LayoutUtils.moveYDistributeNElements(tittle, 3, 5);
        LayoutUtils.moveHorizontalCenter(tittle);
        tittle.setAlignment(Align.center);
        labels.add(tittle);

        // Buttons
        TextButton onePlayer = LayoutUtils.getTextButton("un jugador", 0.22f, 2.8f);
        LayoutUtils.moveYDistributeNElements(onePlayer, 1, 5);
        LayoutUtils.moveXDistributeNElements(onePlayer, 0, 3);
        buttons.add(onePlayer);

        TextButton boardMultiplayer = LayoutUtils.getTextButton("multijugador\n(modo tablero)", 0.22f, 2.8f);
        LayoutUtils.moveYDistributeNElements(boardMultiplayer, 1, 5);
        LayoutUtils.moveXDistributeNElements(boardMultiplayer, 1, 3);
        buttons.add(boardMultiplayer);

        TextButton localNetworkMultiplayer = LayoutUtils.getTextButton("multijugador\n(en red local)\nproximamente", 0.22f, 2.8f);
        LayoutUtils.moveYDistributeNElements(localNetworkMultiplayer, 1, 5);
        LayoutUtils.moveXDistributeNElements(localNetworkMultiplayer, 2, 3);
        localNetworkMultiplayer.setDisabled(true);
        buttons.add(localNetworkMultiplayer);

        TextButton exit = LayoutUtils.getTextButton("salir", 0.1f, 2f);
        LayoutUtils.moveUp(exit);
        LayoutUtils.moveLeft(exit);
        buttons.add(exit);

        TextButton howToPlay = LayoutUtils.getTextButton("tutorial", 0.1f, 2f);
        LayoutUtils.moveUp(howToPlay);
        LayoutUtils.moveRight(howToPlay);
        buttons.add(howToPlay);

        onePlayer.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                controller.goToOnePlayerMenuScreen();
            }
        });
        boardMultiplayer.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                controller.goToBoardMultiplayerScreen();
            }
        });


        exit.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                dispose();
                SKIN.dispose();
                System.exit(0);
            }
        });
        howToPlay.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                controller.goToHowToPlayScreen(false);
            }
        });

        // ToDo for debug
        TextButton test = new TextButton("Test", SKIN);
        es.pozoesteban.alberto.mvcrystals.view.LayoutUtils.setSizeByWidthPercentage(test, 0.03f, 1);
        es.pozoesteban.alberto.mvcrystals.view.LayoutUtils.moveDown(test);
        LayoutUtils.moveHorizontalCenter(test);
        test.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                controller.startGame(new PlayerConfig[]{
                    PlayerConfig.createJuanitoSemillasPlayerConfig(0, "_0", PlayerColor.BLUE),
                    PlayerConfig.createJuanitoSemillasPlayerConfig(1, "_1", PlayerColor.RED)
                });
            }
        });
        //buttons.add(test);
    }
    protected void addActorsToStage() {
        Group background = new Group();
        Group foreground = new Group();
        stage.addActor(background);
        stage.addActor(foreground);
        for (Actor actor : allBackgroundActors) {
            background.addActor(actor);
        }
        for (Actor actor : allForegroundActors) {
            foreground.addActor(actor);
        }
        for (TextButton button : buttons) {
            foreground.addActor(button);
        }
        for (Label label : labels) {
            foreground.addActor(label);
        }
    }

    @Override
    public void hide() {
        dispose();
        super.hide();
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        stage.act();
        stage.draw();
    }

    @Override
    public void dispose() {
        disposeActors();
        stage.clear();
        stage.dispose();
    }

    protected void disposeActors() {
        if (allBackgroundActors != null) {
            for (DisposableImage actor : allBackgroundActors) {
                actor.dispose();
            }
        }
        if (allForegroundActors != null) {
            for (DisposableImage actor : allForegroundActors) {
                actor.dispose();
            }
        }
    }


    @Override
    public void show() {
        super.show();
        stage.setDebugAll(SCREEN_DEBUG);
        Gdx.gl.glClearColor(1f, 1f, 1f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        createActors();
        addActorsToStage();
    }

}
