package es.pozoesteban.alberto.mvcrystals.view.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import es.pozoesteban.alberto.mvcrystals.controller.MVCrystalsGameController;
import es.pozoesteban.alberto.mvcrystals.model.player.PlayerColor;
import es.pozoesteban.alberto.mvcrystals.model.player.PlayerConfig;
import es.pozoesteban.alberto.mvcrystals.view.LayoutUtils;
import es.pozoesteban.alberto.mvcrystals.view.actors.BackgroundImage;
import es.pozoesteban.alberto.mvcrystals.view.actors.DisposableImage;
import es.pozoesteban.alberto.mvcrystals.view.actors.HorizontalCarouselImage;

import java.util.ArrayList;
import java.util.List;

import static es.pozoesteban.alberto.mvcrystals.Configuration.SCREEN_DEBUG;

public class MultiPlayerBoardMenuScreen extends BaseScreen {

    private final List<Button> buttons;
    private final List<Label> labels;
    protected final List<DisposableImage> allBackgroundActors;
    protected final List<DisposableImage> allForegroundActors;
    protected final List<PlayerColor> aiColors;
    protected BackgroundImage backgroundImage;
    private HorizontalCarouselImage[] aiLevelCarousel;
    private HorizontalCarouselImage[] numHumanPlayersCarousel;
    private HorizontalCarouselImage[][] playerColorCarousels;
    protected int numHumanPlayers;
    protected int lastNumHumanPlayers;
    protected int aiLevel;
    protected PlayerColor[] playerColors;
    Button[] nextColor;
    Button[] previousColor;
    Label[] playerColorLabel;
    protected Label aiLevelLabel;

    public MultiPlayerBoardMenuScreen(MVCrystalsGameController controller) {
        super(controller, null);
        buttons = new ArrayList();
        labels = new ArrayList();
        allBackgroundActors = new ArrayList();
        allForegroundActors = new ArrayList();
        playerColors = new PlayerColor[4];
        aiColors = new ArrayList();
    }

    protected void createActors() {
        buttons.clear();
        labels.clear();
        aiColors.clear();
        allBackgroundActors.clear();
        allForegroundActors.clear();
        numHumanPlayers = 3;
        aiLevel = 2;
        lastNumHumanPlayers = -1;
        for (int i = 0; i < playerColors.length; i++) {
            playerColors[i] = PlayerColor.values()[i+2];
        }
        aiLevelCarousel = new HorizontalCarouselImage[4];
        numHumanPlayersCarousel = new HorizontalCarouselImage[3];
        playerColorCarousels = new HorizontalCarouselImage[4][PlayerColor.values().length];
        nextColor = new Button[4];
        previousColor = new Button[4];
        playerColorLabel = new Label[4];

        // Background
        backgroundImage = new BackgroundImage();
        allBackgroundActors.add(backgroundImage);

        // Tittle
        Label tittle = new Label("MULTIJUGADOR", SKIN, "tittle");
        tittle.setAlignment(Align.center);
        LayoutUtils.setSizeByHeightPercentage(tittle, 0.2f, 3f);
        LayoutUtils.moveYDistributeNElements(tittle, 2, 3);
        LayoutUtils.moveHorizontalCenter(tittle);
        labels.add(tittle);

        // Human num players:
        final Label numHumanPlayersLabel = new Label("numero de jugadores", SKIN, "label");
        //LayoutUtils.moveAlignCenterXToOther(numHumanPlayersLabel, numHumanPlayersCarousel[numHumanPlayers-2]);
        LayoutUtils.moveXDistributeNElements(numHumanPlayersLabel, 0.5f, 3);
        LayoutUtils.moveYDistributeNElements(numHumanPlayersLabel, 1.5f, 3);//LayoutUtils.moveAboveOther(numHumanPlayersLabel, numHumanPlayersCarousel[numHumanPlayers-2]);
        numHumanPlayersLabel.setFontScale(0.3f);
        numHumanPlayersLabel.setAlignment(Align.center);
        labels.add(numHumanPlayersLabel);

        float widthPercentage = 0.05f;
        float width = es.pozoesteban.alberto.mvcrystals.view.LayoutUtils.getWidthByPercentage(widthPercentage);
        float wHRatio = 1f;
        float height = width * wHRatio;
        float x = LayoutUtils.getXByPercentage(1.5f/5f) - (width/2f);
        float y = numHumanPlayersLabel.getY() - (height + LayoutUtils.Y_MARGIN);// LayoutUtils.getYByPercentage(2.5f/4f) - (height/2f);
        for (int i = 0; i < numHumanPlayersCarousel.length; i++) {
            numHumanPlayersCarousel[i] = new HorizontalCarouselImage("numbers/dark_" + (i+2) + ".png", i - numHumanPlayers + 2, 3, width, height, x, y);
            allForegroundActors.add(numHumanPlayersCarousel[i]);
        }

        final Button addHumanPlayer = new Button(SKIN, "add");
        LayoutUtils.setSizeByWidthPercentage(addHumanPlayer, widthPercentage, wHRatio);
        LayoutUtils.moveRightOther(addHumanPlayer, numHumanPlayersCarousel[numHumanPlayers-2]);
        LayoutUtils.moveYDistributeNElements(addHumanPlayer, 0, 1, y, y+height);
        buttons.add(addHumanPlayer);

        final Button removeHumanPlayer = new Button(SKIN, "remove");
        LayoutUtils.setSizeByWidthPercentage(removeHumanPlayer, widthPercentage, wHRatio);
        LayoutUtils.moveLeftOther(removeHumanPlayer, numHumanPlayersCarousel[numHumanPlayers-2]);
        LayoutUtils.moveYDistributeNElements(removeHumanPlayer, 0, 1, y, y+height);
        buttons.add(removeHumanPlayer);

        // AI level:
        aiLevelLabel = new Label(aiLevel == 1 ? "dificultad de la ia (facil)" : (aiLevel == 2 ? "dificultad de la ia (media)" : (aiLevel == 3 ? "dificultad de la ia (dificil)" : "sin ia)")), SKIN, "label");
        LayoutUtils.moveXDistributeNElements(aiLevelLabel, 1.5f, 3);
        LayoutUtils.moveYDistributeNElements(aiLevelLabel, 1.5f, 3);


        aiLevelLabel.setFontScale(0.3f);
        aiLevelLabel.setAlignment(Align.center);
        labels.add(aiLevelLabel);

        wHRatio = 1f;
        height = width / wHRatio;
        x = LayoutUtils.getXByPercentage(3.5f/5f) - (width/2f);
        y = aiLevelLabel.getY() - (height + LayoutUtils.Y_MARGIN);// y = LayoutUtils.getYByPercentage(2.5f/4f) - (height/2f);
        for (int i = 0; i < aiLevelCarousel.length; i++) {
            aiLevelCarousel[i] = new HorizontalCarouselImage("ai_levels/level_" + i + ".png", i - aiLevel, 4, width, height, x, y);
            allForegroundActors.add(aiLevelCarousel[i]);
        }
        wHRatio = 1f;
        height = width / wHRatio;

        final Button harderAI = new Button(SKIN, "add");
        LayoutUtils.setSizeByWidthPercentage(harderAI, widthPercentage, wHRatio);
        LayoutUtils.moveRightOther(harderAI, aiLevelCarousel[aiLevel]);
        LayoutUtils.moveYDistributeNElements(harderAI, 0, 1, y, y+height);
        buttons.add(harderAI);

        final Button easierAI = new Button(SKIN, "remove");
        LayoutUtils.setSizeByWidthPercentage(easierAI, widthPercentage, wHRatio);
        LayoutUtils.moveLeftOther(easierAI, aiLevelCarousel[aiLevel]);
        LayoutUtils.moveYDistributeNElements(easierAI, 0, 1, y, y+height);
        buttons.add(easierAI);

        // Players colors: ToDo remove all do it in render!
        for (int j = 0; j < playerColorCarousels.length; j++) {
            playerColorLabel[j] = new Label("jugador " + (j+1), SKIN, "label");
            LayoutUtils.moveHorizontalCenter(playerColorLabel[j]);
            LayoutUtils.moveYDistributeNElements(playerColorLabel[j], 0.5f, 3);//LayoutUtils.moveAboveOther(playerColorLabel[j], playerColorCarousels[j][playerColors[j].getValue()]);
            playerColorLabel[j].setFontScale(0.3f);
            playerColorLabel[j].setAlignment(Align.center);
            labels.add(playerColorLabel[j]);

            playerColorCarousels[j] = new HorizontalCarouselImage[PlayerColor.values().length];
            x = LayoutUtils.getXByPercentage((1f+j) / (float)numHumanPlayers) - (width/2f);
            y = playerColorLabel[j].getY() - (height + LayoutUtils.Y_MARGIN);// y = LayoutUtils.getYByPercentage(1.5f/4f) - (height/2f);
            for (int i = 0; i < playerColorCarousels[j].length; i++) {
                playerColorCarousels[j][i] = new HorizontalCarouselImage("player_colors/" + PlayerColor.values()[i].name().toLowerCase() + ".png", i - playerColors[j].getValue(), PlayerColor.values().length, width, height, x, y);
                allForegroundActors.add(playerColorCarousels[j][i]);
            }
            nextColor[j] = new Button(SKIN, "right");
            LayoutUtils.setSizeByWidthPercentage(nextColor[j], widthPercentage, wHRatio);
            LayoutUtils.moveRightOther(nextColor[j], playerColorCarousels[j][playerColors[j].getValue()]);
            //LayoutUtils.moveYDistributeNElements(nextColor[j], 0, 1, y, y+height);
            buttons.add(nextColor[j]);

            previousColor[j] = new Button(SKIN, "left");
            LayoutUtils.setSizeByWidthPercentage(previousColor[j], widthPercentage, wHRatio);
            LayoutUtils.moveLeftOther(previousColor[j], playerColorCarousels[j][playerColors[j].getValue()]);
            //LayoutUtils.moveYDistributeNElements(previousColor[j], 0, 1, y, y+height);
            buttons.add(previousColor[j]);
        }

        // Back...
        final TextButton back = LayoutUtils.getTextButton("volver", 0.1f, 2f);
        LayoutUtils.moveDown(back);
        LayoutUtils.moveLeft(back);
        buttons.add(back);

        // Play...
        final TextButton play = LayoutUtils.getTextButton("jugar", 0.1f, 2f);
        LayoutUtils.moveDown(play);
        LayoutUtils.moveRight(play);
        buttons.add(play);

        // Listeners
        play.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                PlayerConfig[] playerConfigs = new PlayerConfig[aiLevel == 0 ? numHumanPlayers : 4];
                for (int i = 0; i < numHumanPlayers; i++) {
                    playerConfigs[i] = PlayerConfig.createHumanPlayerConfig(i, playerColors[i].toString(), playerColors[i]);
                }
                if (aiLevel > 0) {
                    int numAiPlayers = 4 - numHumanPlayers;
                    PlayerColor[] aiColors = new PlayerColor[numAiPlayers];
                    for (int i = 0; i < aiColors.length; i++) {
                        aiColors[i] = randomAIColor();
                    }
                    switch (numAiPlayers) {
                        case 1:
                            switch (aiLevel) {
                                case 1:
                                    playerConfigs[numHumanPlayers] = PlayerConfig.createJuanitoElAleatorioPlayerConfig(numHumanPlayers, aiColors[0].toString() + " (IA Facil)", aiColors[0]);
                                    break;
                                case 2:
                                    playerConfigs[numHumanPlayers] = PlayerConfig.createLillo(numHumanPlayers, aiColors[0].toString() + " (IA Medio)", aiColors[0]);
                                    break;
                                case 3:
                                    playerConfigs[numHumanPlayers] = PlayerConfig.createMaximinlian(numHumanPlayers, aiColors[0].toString() + " (IA Dificil)", 2, 5, 0.0001f, 0.5f, aiColors[0]);
                                    break;
                            }
                            break;
                        case 2:
                            switch (aiLevel) {
                                case 1:
                                    playerConfigs[numHumanPlayers] = PlayerConfig.createJuanitoElAleatorioPlayerConfig(numHumanPlayers, aiColors[0].toString() + " (IA Facil)", aiColors[0]);
                                    playerConfigs[numHumanPlayers+1] = PlayerConfig.createJuanitoElAleatorioPlayerConfig(numHumanPlayers+1, aiColors[1].toString() + " (IA Facil)", aiColors[1]);
                                    break;
                                case 2:
                                    playerConfigs[numHumanPlayers] = PlayerConfig.createLillo(numHumanPlayers, aiColors[0].toString() + " (IA Medio)", aiColors[0]);
                                    playerConfigs[numHumanPlayers+1] = PlayerConfig.createMaximinlian(numHumanPlayers+1, aiColors[1].toString() + " (IA Dificil)", 2, 4, 0.0001f, 0.5f, aiColors[1]);
                                    break;
                                case 3:
                                    playerConfigs[numHumanPlayers] = PlayerConfig.createMaximinlian(numHumanPlayers, aiColors[0].toString() + " (IA Difícil)", 2, 5, 0.0001f, 0.4f, aiColors[0]);
                                    playerConfigs[numHumanPlayers+1] = PlayerConfig.createMaximinlian(numHumanPlayers+1, aiColors[1].toString() + " (IA Difícil)", 2, 5, 0.0001f, 0.8f, aiColors[1]);
                                    break;
                            }
                            break;
                    }
                }
                controller.startGame(playerConfigs);
            }
        });

        addHumanPlayer.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return !addHumanPlayer.isDisabled();
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                nextNumHumanPlayers();
                removeHumanPlayer.setDisabled(false);
                if (numHumanPlayers == 4) {
                    addHumanPlayer.setDisabled(true);
                    int aiFixed = aiLevel;
                    for (int i = 0; i < aiFixed; i++) {
                        previousAILevel();
                    }
                    harderAI.setDisabled(true);
                    easierAI.setDisabled(true);
                }

            }
        });
        removeHumanPlayer.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return !removeHumanPlayer.isDisabled();
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if (numHumanPlayers > 2) {
                    previousNumHumanPlayers();
                    removeHumanPlayer.setDisabled(numHumanPlayers == 2);
                    addHumanPlayer.setDisabled(false);
                    harderAI.setDisabled(aiLevel == 3);
                    easierAI.setDisabled(aiLevel == 0);
                }

            }
        });
        harderAI.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return !harderAI.isDisabled();
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                nextAILevel();
                harderAI.setDisabled(aiLevel == 3);
                easierAI.setDisabled(false);

            }
        });

        easierAI.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return !easierAI.isDisabled();
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                previousAILevel();
                easierAI.setDisabled(aiLevel == 0);
                harderAI.setDisabled(false);

            }
        });

        back.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return !back.isDisabled();
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                controller.goToWelcomeScreen();
            }
        });

        for (int j = 0; j < previousColor.length; j++) {
            final int finalJ = j;
            nextColor[j].addListener(new ClickListener() {
                @Override
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    return true;
                }

                @Override
                public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    while(!nextColor(finalJ));
                }
            });
            final int finalJ1 = j;
            previousColor[j].addListener(new ClickListener() {
                @Override
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    return true;
                }

                @Override
                public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    while (!previousColor(finalJ));
                }
            });
        }
    }

    protected void addActorsToStage() {
        Group background = new Group();
        Group foreground = new Group();
        stage.addActor(background);
        stage.addActor(foreground);
        for (Actor actor : allBackgroundActors) {
            background.addActor(actor);
        }
        for (Actor actor : allForegroundActors) {
            foreground.addActor(actor);
        }
        for (Button button : buttons) {
            foreground.addActor(button);
        }
        for (Label label : labels) {
            foreground.addActor(label);
        }
    }


    @Override
    public void hide() {
        dispose();
        super.hide();
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        if (lastNumHumanPlayers != numHumanPlayers) {
            for (int j = 0; j < playerColorCarousels.length; j++) {
                if(j < numHumanPlayers) {
                    float widthPercentage = 0.05f;
                    float width = es.pozoesteban.alberto.mvcrystals.view.LayoutUtils.getWidthByPercentage(widthPercentage);
                    float wHRatio = 1f;
                    float height = width * wHRatio;
                    float x = LayoutUtils.getXByPercentage((0.5f+j) / (numHumanPlayers)) - (width / 2f);
                    float y = playerColorLabel[j].getY() - (height + LayoutUtils.Y_MARGIN);//LayoutUtils.getYByPercentage(1.5f / 4f) - (height / 2f);
                    for (HorizontalCarouselImage actor : playerColorCarousels[j]) {
                        actor.setValues(x, y, width, height);
                        actor.setVisible(true);
                    }
                    HorizontalCarouselImage otherPos0 = playerColorCarousels[j][playerColors[j].getValue()];
                    LayoutUtils.moveRightOther(nextColor[j], otherPos0);
                    LayoutUtils.moveYDistributeNElements(nextColor[j], 0, 1, y, y+height);
                    LayoutUtils.moveLeftOther(previousColor[j], otherPos0);
                    LayoutUtils.moveYDistributeNElements(previousColor[j], 0, 1, y, y+height);
                    LayoutUtils.moveAlignCenterXToOther(playerColorLabel[j], otherPos0);
                    //LayoutUtils.moveAboveOther(playerColorLabel[j], playerColorCarousels[j][playerColors[j].getValue()]);
                    playerColorLabel[j].setVisible(true);
                    nextColor[j].setVisible(true);
                    previousColor[j].setVisible(true);
                } else {
                    for (HorizontalCarouselImage actor : playerColorCarousels[j]) {
                        actor.setVisible(false);
                    }
                    playerColorLabel[j].setVisible(false);
                    nextColor[j].setVisible(false);
                    previousColor[j].setVisible(false);
                }
            }
            lastNumHumanPlayers = numHumanPlayers;
        }
        stage.act();
        stage.draw();
    }

    @Override
    public void dispose() {
        disposeActors();
        stage.dispose();
    }

    protected void disposeActors() {
        if (allBackgroundActors != null) {
            for (DisposableImage actor : allBackgroundActors) {
                actor.dispose();
            }
        }
        if (allForegroundActors != null) {
            for (DisposableImage actor : allForegroundActors) {
                actor.dispose();
            }
        }
    }
    public boolean nextColor(int j) {
        int value = playerColors[j].getValue();
        value++;
        if (value >= PlayerColor.values().length) value = 0;
        playerColors[j] = PlayerColor.values()[value];
        for (HorizontalCarouselImage actor : playerColorCarousels[j]) {
            actor.next();
        }
        return checkSimilarColors();
    }
    private boolean checkSimilarColors() {
        for (int i = 0; i < numHumanPlayers; i++) {
            for (int j = i+1; j < numHumanPlayers; j++) {
                if (playerColors[i].equals(playerColors[j])) return false;
            }
        }
        return true;
    }
    public boolean previousColor(int j) {
        int value = playerColors[j].getValue();
        value--;
        if (value < 0) value = PlayerColor.values().length-1;
        playerColors[j] = PlayerColor.values()[value];
        for (HorizontalCarouselImage actor : playerColorCarousels[j]) {
            actor.previous();
        }
        return checkSimilarColors();
    }
    private PlayerColor randomAIColor() {
        PlayerColor color = PlayerColor.random();

        while(color == playerColors[0] || color == playerColors[1] || color == playerColors[2] || aiColors.contains(color)) {
            color = PlayerColor.random();
        }
        return color;
    }

    public void previousNumHumanPlayers() {
        numHumanPlayers--;
        if (numHumanPlayers < 2) numHumanPlayers = 4;
        for (HorizontalCarouselImage actor : numHumanPlayersCarousel) {
            actor.previous();
        }
    }
    public void nextNumHumanPlayers() {
        numHumanPlayers++;
        if (numHumanPlayers > 4) numHumanPlayers = 2;
        for (HorizontalCarouselImage actor : numHumanPlayersCarousel) {
            actor.next();
        }
    }
    public void previousAILevel() {
        aiLevel--;
        if (aiLevel < 0) aiLevel = aiLevelCarousel.length-1;
        for (HorizontalCarouselImage actor : aiLevelCarousel) {
            actor.previous();
        }
        aiLevelLabel.setText(aiLevel == 1 ? "dificultad de la ia (facil)" : (aiLevel == 2 ? "dificultad de la ia (media)" : (aiLevel == 3 ? "dificultad de la ia (dificil)" : "sin ia)")));
    }
    public void nextAILevel() {
        aiLevel++;
        if (aiLevel > aiLevelCarousel.length) aiLevel = 0;
        for (HorizontalCarouselImage actor : aiLevelCarousel) {
            actor.next();
        }
        aiLevelLabel.setText(aiLevel == 1 ? "dificultad de la ia (facil)" : (aiLevel == 2 ? "dificultad de la ia (media)" : (aiLevel == 3 ? "dificultad de la ia (dificil)" : "sin ia)")));
    }

    @Override
    public void show() {
        super.show();
        stage.setDebugAll(SCREEN_DEBUG);
        Gdx.gl.glClearColor(1f, 1f, 1f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        createActors();
        addActorsToStage();
    }
}
