package es.pozoesteban.alberto.mvcrystals.view.screens;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Timer;
import es.pozoesteban.alberto.mvcrystals.controller.MVCrystalsGameController;
import es.pozoesteban.alberto.mvcrystals.view.LayoutUtils;
import es.pozoesteban.alberto.mvcrystals.view.actors.BackgroundImage;
import es.pozoesteban.alberto.mvcrystals.view.actors.SlideImage;

import java.util.ArrayList;
import java.util.List;

public class HowToPlayScreen extends BaseScreen {

    private static final int NUM_SLIDES = 9;

    private TextButton exitButton;
    private TextButton reloadButton;
    private List<SlideImage> slides;
    private int tipIndex;
    private int lastTipIndex;
    private final Timer timer;
    private final boolean firstTime;
    private BackgroundImage backgroundImage;

    public HowToPlayScreen(MVCrystalsGameController controller, boolean firstTime) {
        super(controller, null);
        this.timer = Timer.instance();
        this.lastTipIndex = -1;
        this.slides = new ArrayList();
        this.firstTime = firstTime;
    }

    public void createActors() {
        slides.clear();

        // Background
        backgroundImage = new BackgroundImage();

        exitButton = LayoutUtils.getTextButton(firstTime ? "saltar" : "salir", 0.1f, 2.5f);
        es.pozoesteban.alberto.mvcrystals.view.LayoutUtils.moveLeft(exitButton);
        es.pozoesteban.alberto.mvcrystals.view.LayoutUtils.moveDown(exitButton);
        exitButton.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                controller.goToWelcomeScreen();
            }
        });

        reloadButton = LayoutUtils.getTextButton("volver a ver", 0.1f, 2.5f);
        es.pozoesteban.alberto.mvcrystals.view.LayoutUtils.moveRightOther(reloadButton, exitButton);
        LayoutUtils.moveDown(reloadButton);
        reloadButton.setDisabled(tipIndex == 0);

        reloadButton.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return tipIndex > 0;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                loadPresentation();
            }
        });

        loadPresentation();
    }


    @Override
    public void show() {
        super.show();

        createActors();

        stage.addActor(backgroundImage);
        for (int i = slides.size()-1; i >= 0; i--) {
            stage.addActor(slides.get(i));
        }
        stage.addActor(exitButton);
        stage.addActor(reloadButton);

    }

    @Override
    public void hide() {
        super.hide();
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        if (tipIndex != lastTipIndex) {
            lastTipIndex = tipIndex;
            for (SlideImage slide : slides) {
                slide.setCurrentSlide(tipIndex);
            }
        }
        stage.act();
        stage.draw();
    }
    @Override
    public void dispose() {
        for (SlideImage slide : slides) {
            slide.dispose();
        }
        backgroundImage.dispose();
        stage.dispose();
    }

    private void loadPresentation() {
        if (slides.isEmpty()) {
            for (int index = 0; index < NUM_SLIDES; index++) {
                slides.add(new SlideImage(index, this));
            }
        }
        tipIndex = 0;
        reloadButton.setDisabled(tipIndex == 0);
    }

    public void next() {
        if (tipIndex < slides.size()-1) tipIndex++;
        else controller.goToWelcomeScreen();
        reloadButton.setDisabled(tipIndex == 0);
    }
    public void previous() {
        if (tipIndex > 0) tipIndex--;
        reloadButton.setDisabled(tipIndex == 0);
    }

}
