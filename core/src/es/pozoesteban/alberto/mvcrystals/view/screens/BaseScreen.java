package es.pozoesteban.alberto.mvcrystals.view.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import es.pozoesteban.alberto.mvcrystals.Configuration;
import es.pozoesteban.alberto.mvcrystals.controller.MVCrystalsGameController;
import es.pozoesteban.alberto.mvcrystals.model.GameState;


public class BaseScreen implements Screen {

    public final static float MOVE_DISTANCE = Gdx.graphics.getWidth() * 0.03f;


    protected final MVCrystalsGameController controller;
    protected final GameState gameState;
    protected Stage stage;

    public static final Skin SKIN = new Skin(Gdx.files.internal(Configuration.SKIN_NAME));

    public BaseScreen(MVCrystalsGameController controller, GameState gameState){
        this.controller = controller;
        this.gameState = gameState;
    }

    @Override
    public void show() {
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1f, 1f, 1f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
    }

    @Override
    public void dispose() {

    }

}
