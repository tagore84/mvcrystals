package es.pozoesteban.alberto.mvcrystals.view.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import es.pozoesteban.alberto.mvcrystals.controller.MVCrystalsGameController;
import es.pozoesteban.alberto.mvcrystals.model.GameState;
import es.pozoesteban.alberto.mvcrystals.model.elements.Crystal;
import es.pozoesteban.alberto.mvcrystals.view.LayoutUtils;
import es.pozoesteban.alberto.mvcrystals.view.ModalDialog;
import es.pozoesteban.alberto.mvcrystals.view.actors.*;

import java.util.ArrayList;
import java.util.List;

import static es.pozoesteban.alberto.mvcrystals.Configuration.SCREEN_DEBUG;

public class PlayingScreen extends BaseScreen {

    protected BoardImage[] boardActors;
    protected FactoryImage[] factoryActors;
    protected CrystalImage[] crystalActors;
    protected CellSelectorImage[][] cellSelectorActors;
    protected FirstPlayerTokenImage firstPlayerTokenActor;
    protected CenterPlateImage centerPlateActor;
    protected Button pointsPopUpButton;
    protected Button exitButton;
    protected BackgroundImage backgroundImage;

    protected List<DisposableImage> allBackgroundActors;
    protected List<DisposableImage> allForegroundActors;

    private final boolean oneHumanPlayerMode;

    public PlayingScreen(MVCrystalsGameController controller, GameState gameState, boolean oneHumanPlayerMode) {
        super(controller, gameState);
        this.oneHumanPlayerMode = oneHumanPlayerMode;
    }

    public void init() {
        disposeActors();
        boardActors = new BoardImage[4];
        factoryActors = new FactoryImage[gameState.getNumFactories()];
        crystalActors = new CrystalImage[100];
        cellSelectorActors = new CellSelectorImage[4][1+2+3+4+5+7];
        allBackgroundActors = new ArrayList();
        allForegroundActors = new ArrayList();
    }

    @Override
    public void show() {
        super.show();
        init();
        stage.setDebugAll(SCREEN_DEBUG);
        Gdx.gl.glClearColor(1f, 1f, 1f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        createActors();
        addActorsToStage();
    }

    protected void createActors() {

        // Background
        backgroundImage = new BackgroundImage();
        allBackgroundActors.add(backgroundImage);

        // Exit button
        exitButton = LayoutUtils.getTextButton("salir", 0.1f, 2.5f);
        es.pozoesteban.alberto.mvcrystals.view.LayoutUtils.moveLeft(exitButton);
        es.pozoesteban.alberto.mvcrystals.view.LayoutUtils.moveDown(exitButton);
        exitButton.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                showExit();
            }
        });

        // Points button
        pointsPopUpButton = LayoutUtils.getTextButton("puntos", 0.1f, 2.5f);
        es.pozoesteban.alberto.mvcrystals.view.LayoutUtils.setSizeByWidthPercentage(pointsPopUpButton, 0.1f, 2.5f);
        if (oneHumanPlayerMode) {
            es.pozoesteban.alberto.mvcrystals.view.LayoutUtils.moveLeft(pointsPopUpButton);
            es.pozoesteban.alberto.mvcrystals.view.LayoutUtils.moveAboveOther(pointsPopUpButton, exitButton);
        } else {
            es.pozoesteban.alberto.mvcrystals.view.LayoutUtils.moveRight(pointsPopUpButton);
            LayoutUtils.moveDown(pointsPopUpButton);
        }
        pointsPopUpButton.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                showPoints();
            }
        });

        // Boards
        for (int i = 0; i < gameState.getPlayers().length; i++) {
            if (gameState.getPlayer(i) != null) {
                boardActors[i] = new BoardImage(gameState, i, oneHumanPlayerMode, gameState.getPlayer(i).getColor());
                allBackgroundActors.add(boardActors[i]);

                int index = 0;
                for (int row = 0; row < 5; row++) {
                    for (int column = 0; column < row+1; column++) {
                        cellSelectorActors[i][index] = new CellSelectorImage(gameState, controller, i, row, column, boardActors, oneHumanPlayerMode);
                        allForegroundActors.add(cellSelectorActors[i][index]);
                        index++;
                    }
                }
                for (int column = 0; column < 7; column++) {
                    cellSelectorActors[i][index] = new CellSelectorImage(gameState, controller, i, 5, column, boardActors, oneHumanPlayerMode);
                    allForegroundActors.add(cellSelectorActors[i][index]);
                    index++;
                }
            } else {
                boardActors[i] = null;
            }
        }

        // Factories
        for (int i = 0; i < gameState.getNumFactories(); i++) {
            factoryActors[i] = new FactoryImage(i, oneHumanPlayerMode, boardActors, gameState.getNumFactories(), pointsPopUpButton);
            allBackgroundActors.add(factoryActors[i]);
        }

        // Center Plater
        centerPlateActor = new CenterPlateImage(gameState.getCenterPlate().numSlots(), oneHumanPlayerMode, exitButton, boardActors, factoryActors);
        allBackgroundActors.add(centerPlateActor);

        // Tokens
        firstPlayerTokenActor = new FirstPlayerTokenImage(gameState, boardActors, factoryActors, centerPlateActor, oneHumanPlayerMode);
        allForegroundActors.add(firstPlayerTokenActor);

        int crystalIndex = 0;
        for (Crystal crystal : gameState.getCrystals()) {
            crystalActors[crystalIndex] = new CrystalImage(gameState, controller, crystal.getId(), crystal.getColor(),
                    boardActors, factoryActors, centerPlateActor, oneHumanPlayerMode);
            allForegroundActors.add(crystalActors[crystalIndex]);
            crystalIndex++;
        }
    }

    public void showPoints() {
        //Dialog dialog = new Dialog("Ranking", SKIN, "dialog") {};
        ModalDialog dialog = new ModalDialog("ronda " + (gameState.getPhase() + 1) + " turno " + (gameState.getTurn() + 1)){
            public void result(Object obj) {
                if (gameState.isFinish()) {
                    controller.goToWelcomeScreen();
                }
            }
        };
        dialog.text(controller.getPointsText());
        dialog.button(gameState.isFinish() ? "salir" : "ok", true); //sends "true" as the result
        dialog.key(Input.Keys.ENTER, true); //sends "true" when the ENTER key is pressed
        dialog.show(stage);
    }
    public void showExit() {
        //Dialog dialog = new Dialog("Ranking", SKIN, "dialog") {};
        ModalDialog dialog = new ModalDialog("salir"){
            public void result(Object obj) {
                if ((Boolean)obj) controller.goToWelcomeScreen();
            }
        };
        dialog.text("¿seguro que quieres salir?");
        dialog.button(LayoutUtils.getTextButton("salir", 0.1f, 1.5f), true);
        //dialog.button("salir", true); //sends "true" as the result
        dialog.button(LayoutUtils.getTextButton("volver", 0.1f, 1f), false);
        //dialog.button("volver al juego", false); //sends "true" as the result
        dialog.key(Input.Keys.ENTER, true); //sends "true" when the ENTER key is pressed
        dialog.show(stage);
    }

    protected void addActorsToStage() {
        Group background = new Group();
        Group foreground = new Group();
        stage.addActor(background);
        stage.addActor(foreground);
        for (Actor actor : allBackgroundActors) {
            background.addActor(actor);
        }
        for (Actor actor : allForegroundActors) {
            foreground.addActor(actor);
        }
        foreground.addActor(pointsPopUpButton);
        foreground.addActor(exitButton);
        //for (PlayerLabel playerLabel : playerLabels) {
        //    foreground.addActor(playerLabel);
        //}
    }

    @Override
    public void hide() {
        dispose();
        stage.dispose();
        super.hide();
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        stage.act();
        stage.draw();
    }

    @Override
    public void dispose() {
        disposeActors();
    }

    protected void disposeActors() {
        if (allBackgroundActors != null) {
            for (DisposableImage actor : allBackgroundActors) {
                actor.dispose();
            }
        }
        if (allForegroundActors != null) {
            for (DisposableImage actor : allForegroundActors) {
                actor.dispose();
            }
        }
    }
}
