package es.pozoesteban.alberto.mvcrystals.view.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import es.pozoesteban.alberto.mvcrystals.controller.MVCrystalsGameController;
import es.pozoesteban.alberto.mvcrystals.model.player.PlayerColor;
import es.pozoesteban.alberto.mvcrystals.model.player.PlayerConfig;
import es.pozoesteban.alberto.mvcrystals.view.LayoutUtils;
import es.pozoesteban.alberto.mvcrystals.view.actors.BackgroundImage;
import es.pozoesteban.alberto.mvcrystals.view.actors.DisposableImage;
import es.pozoesteban.alberto.mvcrystals.view.actors.HorizontalCarouselImage;

import java.util.ArrayList;
import java.util.List;

import static es.pozoesteban.alberto.mvcrystals.Configuration.*;

public class OnePlayerMenuScreen extends BaseScreen {

    private final List<Button> buttons;
    private final List<Label> labels;
    protected final List<DisposableImage> allBackgroundActors;
    protected final List<DisposableImage> allForegroundActors;
    protected final List<PlayerColor> aiColors;
    private HorizontalCarouselImage[] aiNumPlayerCarousel;
    private HorizontalCarouselImage[] aiLevelCarousel;
    private HorizontalCarouselImage[] playerColorCarousel;
    protected BackgroundImage backgroundImage;
    protected int numAiPlayers;
    protected int aiLevel;
    protected PlayerColor playerColor;
    protected Label aiLevelLabel;


    public OnePlayerMenuScreen(MVCrystalsGameController controller) {
        super(controller, null);
        buttons = new ArrayList();
        labels = new ArrayList();
        allBackgroundActors = new ArrayList();
        allForegroundActors = new ArrayList();
        aiColors = new ArrayList();
    }

    protected void createActors() {
        buttons.clear();
        labels.clear();
        aiColors.clear();
        allBackgroundActors.clear();
        allForegroundActors.clear();
        numAiPlayers = 2;
        aiLevel = 2;
        playerColor = PlayerColor.values()[PlayerColor.values().length/2];
        aiNumPlayerCarousel = new HorizontalCarouselImage[3];
        aiLevelCarousel = new HorizontalCarouselImage[3];
        playerColorCarousel = new HorizontalCarouselImage[PlayerColor.values().length];

        // Background
        backgroundImage = new BackgroundImage();
        allBackgroundActors.add(backgroundImage);

        // Tittle
        Label tittle = new Label("UN JUGADOR", SKIN, "tittle");
        tittle.setAlignment(Align.center);
        LayoutUtils.setSizeByHeightPercentage(tittle, 0.2f, 3f);
        LayoutUtils.moveYDistributeNElements(tittle, 3, 4);
        LayoutUtils.moveHorizontalCenter(tittle);
        labels.add(tittle);

        //AI num players:
        Label aiNumPlayersLabel = new Label("numero de jugadores de la maquina", SKIN, "label");
        LayoutUtils.moveHorizontalCenter(aiNumPlayersLabel);
        LayoutUtils.moveYDistributeNElements(aiNumPlayersLabel, 2.5f, 4);
        aiNumPlayersLabel.setFontScale(0.5f);
        aiNumPlayersLabel.setAlignment(Align.center);
        labels.add(aiNumPlayersLabel);

        float widthPercentage = 0.05f;
        float width = es.pozoesteban.alberto.mvcrystals.view.LayoutUtils.getWidthByPercentage(widthPercentage);
        float wHRatio = 1f;
        float height = width * wHRatio;
        float x = LayoutUtils.getXCenter() - (width/2f);
        float y = aiNumPlayersLabel.getY() - (height + LayoutUtils.Y_MARGIN);//tittle.getY() - (height + LayoutUtils.Y_MARGIN) ;
        for (int i = 0; i < aiNumPlayerCarousel.length; i++) {
            aiNumPlayerCarousel[i] = new HorizontalCarouselImage("numbers/dark_" + (i+1) + ".png", numAiPlayers - (3-i), 3, width, height, x, y);
            allForegroundActors.add(aiNumPlayerCarousel[i]);
        }

        final Button addAIPlayer = new Button(SKIN, "add");
        LayoutUtils.setSizeByWidthPercentage(addAIPlayer, widthPercentage, wHRatio);
        LayoutUtils.moveRightOther(addAIPlayer, aiNumPlayerCarousel[numAiPlayers-1]);
        LayoutUtils.moveYDistributeNElements(addAIPlayer, 0, 1, y, y+height);
        buttons.add(addAIPlayer);

        final Button removeAIPlayer = new Button(SKIN, "remove");
        es.pozoesteban.alberto.mvcrystals.view.LayoutUtils.setSizeByWidthPercentage(removeAIPlayer, widthPercentage, wHRatio);
        es.pozoesteban.alberto.mvcrystals.view.LayoutUtils.moveLeftOther(removeAIPlayer, aiNumPlayerCarousel[numAiPlayers-1]);
        es.pozoesteban.alberto.mvcrystals.view.LayoutUtils.moveYDistributeNElements(removeAIPlayer, 0, 1, y, y+height);
        buttons.add(removeAIPlayer);

        //AI level:

        aiLevelLabel = new Label("dificultad de la ia (" + (aiLevel == 1 ? "facil)" : (aiLevel == 2 ? "media)" : "dificil)")), SKIN, "label");
        LayoutUtils.moveHorizontalCenter(aiLevelLabel);
        //LayoutUtils.moveAboveOther(aiLevelLabel, aiLevelCarousel[aiLevel-1]);
        LayoutUtils.moveYDistributeNElements(aiLevelLabel, 1.5f, 4);
        aiLevelLabel.setFontScale(0.5f);
        aiLevelLabel.setAlignment(Align.center);
        labels.add(aiLevelLabel);

        wHRatio = 1f;
        height = width / wHRatio;
        x = LayoutUtils.getXCenter() - (width/2f);
        y = aiLevelLabel.getY() - (height + LayoutUtils.Y_MARGIN); //LayoutUtils.getYByPercentage(2f/4f) - (height/2f);
        for (int i = 0; i < aiLevelCarousel.length; i++) {
            aiLevelCarousel[i] = new HorizontalCarouselImage("ai_levels/level_" + (i+1) + ".png", aiLevel - (3-i), 3, width, height, x, y);
            allForegroundActors.add(aiLevelCarousel[i]);
        }
        wHRatio = 1f;
        height = width / wHRatio;

        final Button harderAI = new Button(SKIN, "add");
        LayoutUtils.setSizeByWidthPercentage(harderAI, widthPercentage, wHRatio);
        LayoutUtils.moveRightOther(harderAI, aiLevelCarousel[aiLevel-1]);
        LayoutUtils.moveYDistributeNElements(harderAI, 0, 1, y, y+height);
        buttons.add(harderAI);

        final Button easierAI = new Button(SKIN, "remove");
        es.pozoesteban.alberto.mvcrystals.view.LayoutUtils.setSizeByWidthPercentage(easierAI, widthPercentage, wHRatio);
        es.pozoesteban.alberto.mvcrystals.view.LayoutUtils.moveLeftOther(easierAI, aiLevelCarousel[aiLevel-1]);
        es.pozoesteban.alberto.mvcrystals.view.LayoutUtils.moveYDistributeNElements(easierAI, 0, 1, y, y+height);
        buttons.add(easierAI);

        // Player Color:

        Label playerColorLabel = new Label("color de tu tablero", SKIN, "label");
        LayoutUtils.moveHorizontalCenter(playerColorLabel);
        LayoutUtils.moveYDistributeNElements(playerColorLabel, 0.5f, 4);
        playerColorLabel.setFontScale(0.5f);
        playerColorLabel.setAlignment(Align.center);
        labels.add(playerColorLabel);

        x = LayoutUtils.getXCenter() - (width/2f);
        y = playerColorLabel.getY() - (height + LayoutUtils.Y_MARGIN); //LayoutUtils.getYByPercentage(1f/4f) - (height/2f);
        for (int i = 0; i < playerColorCarousel.length; i++) {
            playerColorCarousel[i] = new HorizontalCarouselImage("player_colors/" + PlayerColor.values()[i].name().toLowerCase() + ".png", i - playerColor.getValue(), PlayerColor.values().length, width, height, x, y);
            allForegroundActors.add(playerColorCarousel[i]);
        }

        final Button nextColor = new Button(SKIN, "right");
        LayoutUtils.setSizeByWidthPercentage(nextColor, widthPercentage, wHRatio);
        LayoutUtils.moveRightOther(nextColor, playerColorCarousel[playerColor.getValue()]);
        LayoutUtils.moveYDistributeNElements(nextColor, 0, 1, y, y+height);
        buttons.add(nextColor);

        final Button previousColor = new Button(SKIN, "left");
        LayoutUtils.setSizeByWidthPercentage(previousColor, widthPercentage, wHRatio);
        LayoutUtils.moveLeftOther(previousColor, playerColorCarousel[playerColor.getValue()]);
        LayoutUtils.moveYDistributeNElements(previousColor, 0, 1, y, y+height);
        buttons.add(previousColor);

        // Back...
        final TextButton back = LayoutUtils.getTextButton("volver", 0.1f, 2f);
        LayoutUtils.moveDown(back);
        LayoutUtils.moveLeft(back);
        buttons.add(back);

        // Play...
        final TextButton play = LayoutUtils.getTextButton("jugar", 0.1f, 2f);
        LayoutUtils.moveDown(play);
        LayoutUtils.moveRight(play);
        buttons.add(play);

        // Listeners
        play.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return !play.isDisabled();
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                PlayerConfig[] playerConfigs = new PlayerConfig[numAiPlayers+1];
                playerConfigs[0] = PlayerConfig.createHumanPlayerConfig(0, playerColor.toString(), playerColor);
                PlayerColor[] aiColors = new PlayerColor[numAiPlayers];
                for (int i = 0; i < aiColors.length; i++) {
                    aiColors[i] = randomAIColor();
                }
                switch (numAiPlayers) {
                    case 1:
                        switch (aiLevel) {
                            case 1:
                                playerConfigs[1] = PlayerConfig.createJuanitoElAleatorioPlayerConfig(1, aiColors[0].toString() + " (IA Facil)", aiColors[0]);
                                break;
                            case 2:
                                playerConfigs[1] = PlayerConfig.createLillo(1, aiColors[0].toString() + " (IA Medio)", aiColors[0]);
                                break;
                            case 3:
                                playerConfigs[1] = PlayerConfig.createMaximinlian(1, aiColors[0].toString() + " (IA Difícil)", 2, 5, 0.0001f, 0.5f, aiColors[0]);
                                break;
                        }
                        break;
                    case 2:
                        switch (aiLevel) {
                            case 1:
                                playerConfigs[1] = PlayerConfig.createJuanitoElAleatorioPlayerConfig(1, aiColors[0].toString() + " (IA Facil)", aiColors[0]);
                                playerConfigs[2] = PlayerConfig.createJuanitoElAleatorioPlayerConfig(2, aiColors[1].toString() + " (IA Facil)", aiColors[1]);
                                break;
                            case 2:
                                playerConfigs[1] = PlayerConfig.createLillo(1, aiColors[0].toString() + " (IA Medio)", aiColors[0]);
                                playerConfigs[2] = PlayerConfig.createMaximinlian(2, aiColors[1].toString() + " (IA Dificil)", 2, 4, 0.0001f, 0.5f, aiColors[1]);
                                break;
                            case 3:
                                playerConfigs[1] = PlayerConfig.createMaximinlian(1, aiColors[0].toString() + " (IA Dificil)", 2, 5, 0.0001f, 0.4f, aiColors[0]);
                                playerConfigs[2] = PlayerConfig.createMaximinlian(2, aiColors[1].toString() + " (IA Dificil)", 2, 5, 0.0001f, 0.8f, aiColors[1]);
                                break;
                        }
                        break;
                    case 3:
                        switch (aiLevel) {
                            case 1:
                                playerConfigs[1] = PlayerConfig.createJuanitoElAleatorioPlayerConfig(1, aiColors[0].toString() + " (IA Facil)", aiColors[0]);
                                playerConfigs[2] = PlayerConfig.createJuanitoElAleatorioPlayerConfig(2, aiColors[1].toString() + " (IA Facil)", aiColors[1]);
                                playerConfigs[3] = PlayerConfig.createJuanitoElAleatorioPlayerConfig(3, aiColors[2].toString() + " (IA Facil)", aiColors[2]);
                                break;
                            case 2:
                                playerConfigs[1] = PlayerConfig.createLillo(1, aiColors[0].toString() + " (IA Medio)", aiColors[0]);
                                playerConfigs[2] = PlayerConfig.createLillo(2, aiColors[1].toString() + " (IA Medio)", aiColors[1]);
                                playerConfigs[3] = PlayerConfig.createMaximinlian(3, aiColors[2].toString() + " (IA Dificil)", 2, 4, 0.0001f, 0.5f, aiColors[2]);
                                break;
                            case 3:
                                playerConfigs[1] = PlayerConfig.createMaximinlian(1, aiColors[0].toString() + " (IA Dificil)", 2, 6, 0.0001f, 0.3f, aiColors[0]);
                                playerConfigs[2] = PlayerConfig.createMaximinlian(2, aiColors[1].toString() + " (IA Dificil)", 2, 6, 0.0001f, 0.5f, aiColors[1]);
                                playerConfigs[3] = PlayerConfig.createMaximinlian(3, aiColors[2].toString() + " (IA Dificil)", 2, 6, 0.0001f, 0.8f, aiColors[2]);
                                break;
                        }
                        break;
                }
                controller.startGame(playerConfigs);
            }
        });

        addAIPlayer.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                VIEW_LOGGER.info("addAIPlayer clicked and is disable: " + addAIPlayer.isDisabled());
                return !addAIPlayer.isDisabled();
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                nextNumAIPlayers();
                addAIPlayer.setDisabled(numAiPlayers == 3);
                removeAIPlayer.setDisabled(false);

            }
        });
        removeAIPlayer.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                VIEW_LOGGER.info("removeAIPlayer clicked and is disable: " + removeAIPlayer.isDisabled());
                return !removeAIPlayer.isDisabled();
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if (numAiPlayers > 1) {
                    previousNumAIPlayers();
                    removeAIPlayer.setDisabled(numAiPlayers == 1);
                    addAIPlayer.setDisabled(false);
                }

            }
        });
        harderAI.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                VIEW_LOGGER.info("harderAI clicked and is disable: " + harderAI.isDisabled());
                return !harderAI.isDisabled();
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                nextAILevel();
                harderAI.setDisabled(aiLevel == 3);
                easierAI.setDisabled(false);
            }
        });
        easierAI.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                VIEW_LOGGER.info("easierAI clicked and is disable: " + easierAI.isDisabled());
                return !easierAI.isDisabled();
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                previousAILevel();
                easierAI.setDisabled(aiLevel == 1);
                harderAI.setDisabled(false);
            }
        });
        nextColor.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                VIEW_LOGGER.info("nextColor clicked and is disable: " + nextColor.isDisabled());
                return !nextColor.isDisabled();
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                nextColor();
            }
        });
        previousColor.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                VIEW_LOGGER.info("previousColor clicked and is disable: " + previousColor.isDisabled());
                return !previousColor.isDisabled();
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                previousColor();
            }
        });
        back.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return !back.isDisabled();
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                controller.goToWelcomeScreen();
            }
        });
    }

    private PlayerColor randomAIColor() {
        PlayerColor color = PlayerColor.random();
        while(color == playerColor || aiColors.contains(color)) {
            color = PlayerColor.random();
        }
        return color;
    }

    protected void addActorsToStage() {
        Group background = new Group();
        Group foreground = new Group();
        stage.addActor(background);
        stage.addActor(foreground);
        for (Actor actor : allBackgroundActors) {
            background.addActor(actor);
        }
        for (Actor actor : allForegroundActors) {
            foreground.addActor(actor);
        }
        for (Button button : buttons) {
            foreground.addActor(button);
        }
        for (Label label : labels) {
            foreground.addActor(label);
        }
    }


    public void previousNumAIPlayers() {
        numAiPlayers--;
        if (numAiPlayers < 1) numAiPlayers = aiNumPlayerCarousel.length;
        for (HorizontalCarouselImage actor : aiNumPlayerCarousel) {
            actor.previous();
        }
    }
    public void nextNumAIPlayers() {
        numAiPlayers++;
        if (numAiPlayers > aiNumPlayerCarousel.length) numAiPlayers = 1;
        for (HorizontalCarouselImage actor : aiNumPlayerCarousel) {
            actor.next();
        }
    }
    public void previousColor() {
        int value = playerColor.getValue();
        value--;
        if (value < 0) value = PlayerColor.values().length-1;
        playerColor = PlayerColor.values()[value];
        for (HorizontalCarouselImage actor : playerColorCarousel) {
            actor.previous();
        }
    }
    public void nextColor() {
        int value = playerColor.getValue();
        value++;
        if (value >= PlayerColor.values().length) value = 0;
        playerColor = PlayerColor.values()[value];
        for (HorizontalCarouselImage actor : playerColorCarousel) {
            actor.next();
        }
    }
    public void previousAILevel() {
        aiLevel--;
        if (aiLevel <= 0) aiLevel = aiLevelCarousel.length;
        for (HorizontalCarouselImage actor : aiLevelCarousel) {
            actor.previous();
        }
        aiLevelLabel.setText("dificultad de la ia (" + (aiLevel == 1 ? "facil)" : (aiLevel == 2 ? "media)" : "dificil)")));
    }
    public void nextAILevel() {
        aiLevel++;
        if (aiLevel > aiLevelCarousel.length) aiLevel = 1;
        for (HorizontalCarouselImage actor : aiLevelCarousel) {
            actor.next();
        }
        aiLevelLabel.setText("dificultad de la ia (" + (aiLevel == 1 ? "facil)" : (aiLevel == 2 ? "media)" : "dificil)")));
    }

    @Override
    public void hide() {
        dispose();
        super.hide();
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        stage.act();
        stage.draw();
    }

    @Override
    public void dispose() {
        disposeActors();
        stage.dispose();
    }

    protected void disposeActors() {
        if (allBackgroundActors != null) {
            for (DisposableImage actor : allBackgroundActors) {
                actor.dispose();
            }
        }
        if (allForegroundActors != null) {
            for (DisposableImage actor : allForegroundActors) {
                actor.dispose();
            }
        }

    }


    @Override
    public void show() {
        super.show();
        stage.setDebugAll(SCREEN_DEBUG);
        Gdx.gl.glClearColor(1f, 1f, 1f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        createActors();
        addActorsToStage();
    }

}
