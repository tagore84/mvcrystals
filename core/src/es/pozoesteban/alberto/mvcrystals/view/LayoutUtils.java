package es.pozoesteban.alberto.mvcrystals.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import es.pozoesteban.alberto.mvcrystals.view.actors.DisposableImage;

import static es.pozoesteban.alberto.mvcrystals.view.screens.BaseScreen.SKIN;

public class LayoutUtils {

    public static final float WIDTH_SCREEN = Gdx.graphics.getWidth();
    public static final float HEIGHT_SCREEN = Gdx.graphics.getHeight();
    public static final float X_MARGIN = WIDTH_SCREEN * 0.03f;
    public static final float Y_MARGIN = HEIGHT_SCREEN * 0.03f;

    public static void moveLeft(Actor actor) {
        actor.setX(X_MARGIN);
    }
    public static void moveHorizontalCenter(Actor actor) {
        moveX(actor, 0.5f);
    }
    public static void moveX(Actor actor, float percentage) {
        actor.setX(WIDTH_SCREEN*percentage - actor.getWidth()/2f);
    }
    public static void moveRight(Actor actor) {
        actor.setX(WIDTH_SCREEN - (actor.getWidth() + X_MARGIN));
    }
    public static void moveUp(Actor actor) {
        actor.setY(HEIGHT_SCREEN - (actor.getHeight() + Y_MARGIN));
    }
    public static void moveVerticalCenter(Actor actor) {
        moveY(actor, 0.5f);
    }
    public static void moveVerticalCenter(Actor actor, float offset) {
        float percentage = 0.5f;
        percentage += (actor.getHeight()/HEIGHT_SCREEN) * offset;
        moveY(actor, percentage);
    }
    public static void moveY(Actor actor, float percentage) {
        actor.setY(HEIGHT_SCREEN*percentage - actor.getHeight()/2f);
    }
    public static void moveDown(Actor actor) {
        actor.setY(Y_MARGIN);
    }
    public static void moveLeftOther(Actor actor, Actor other) {
        actor.setX(other.getX() - (actor.getWidth() + X_MARGIN));
    }
    public static void moveRightOther(Actor actor, Actor other) {
        actor.setX(other.getX() + other.getWidth() + X_MARGIN);
    }
    public static void moveAboveOther(Actor actor, Actor other) {
        actor.setY(other.getY() + other.getHeight() + Y_MARGIN);
    }
    public static void moveBellowOther(Actor actor, Actor other) {
        actor.setY(other.getY() - (actor.getHeight() + Y_MARGIN));
    }
    public static void moveXDistributeNElements(Actor actor, float pos, int total) {
        moveXDistributeNElements(actor, pos, total, 0, WIDTH_SCREEN);
    }
    public static void moveXDistributeNElements(Actor actor, float pos, int total, float left, float right) {
        left += X_MARGIN;
        if (right < 0) right = WIDTH_SCREEN + right;
        right -= X_MARGIN;
        float margin = ((right - left) - (actor.getWidth()*total)) / (total+1f);
        actor.setX(left + (margin*(pos+1)) + (actor.getWidth()*pos));
    }
    public static void moveYDistributeNElements(Actor actor, float pos, int total, float down, float up) {
        down += Y_MARGIN;
        if (up < 0) up = HEIGHT_SCREEN + up;
        up -= Y_MARGIN;
        float margin = ((up - down) - (actor.getHeight()*total)) / (total+1f);
        actor.setY(down + (margin*(pos+1)) + (actor.getHeight()*pos));
    }
    public static void moveYDistributeNElements(Actor actor, float pos, int total) {
        float down = Y_MARGIN;
        float up = HEIGHT_SCREEN - Y_MARGIN;
        float margin = ((up - down) - (actor.getHeight()*total)) / (total+1f);
        actor.setY(down + (margin*(pos+1)) + (actor.getHeight()*pos));
    }

    public static void moveAlignUpYToOther(Actor actor, Actor other) {
        actor.setY(other.getY() + other.getHeight() - actor.getHeight());
    }
    public static float getScreenRatio() {
        return WIDTH_SCREEN/HEIGHT_SCREEN;
    }
    public static float calcWidthWithPercentageAbsolute(float maxWidthPercentage, float maxWidthAbsolute) {
        float widthByAbosolute = (maxWidthAbsolute-(2*X_MARGIN)) / WIDTH_SCREEN;
        return widthByAbosolute < maxWidthPercentage ? widthByAbosolute : maxWidthPercentage;
    }

    public static float calcHeightWithPercentageAbout(float maxHeightPercentage, float maxHeightAbsolute) {

        float heightByAbosolute = (maxHeightAbsolute-(2*Y_MARGIN) / HEIGHT_SCREEN);
        return heightByAbosolute < maxHeightPercentage ? heightByAbosolute : maxHeightPercentage;
    }

    public static float getWidthByPercentage(float percentage) {
        return (WIDTH_SCREEN-(2f*X_MARGIN))*percentage;
    }
    public static float getHeightByPercentage(float percentage) {
        return (HEIGHT_SCREEN-(2f*Y_MARGIN))*percentage;
    }

    public static void moveAlignCenterXToOther(Actor actor, Actor other) {
        actor.setX(other.getX() + (other.getWidth()/2f) - (actor.getWidth()/2f));
    }

    public static void setSizeByWidthPercentage(Actor actor, float widthPercentage, float wHRatio) {
        float width = WIDTH_SCREEN*widthPercentage;
        float height = width / wHRatio;
        actor.setSize(width, height);
    }
    public static void setSizeByHeightPercentage(Actor actor, float heightPercentage, float wHRatio) {
        float height = HEIGHT_SCREEN *  heightPercentage;
        float width = height * wHRatio;
        actor.setSize(width, height);
    }


    public static float calcMaxHeight(float ocupped, int numElements) {
        float output = HEIGHT_SCREEN - ocupped;
        output -= (Y_MARGIN*(numElements+1));
        output /= (float)numElements;
        return output;
    }

    public static float calcMaxHeight(float down, float up, int numElements) {
        if (up < 0) up = HEIGHT_SCREEN + up;
        float output = up - down - (Y_MARGIN*(numElements+1));
        return output / (float)numElements;
    }

    public static float calcMaxWidth(float left, float right, int numElements) {
        float output = right - left - (X_MARGIN*(numElements+1));
        return output / (float)numElements;
    }

    public static void fullScreen(DisposableImage actor) {
        actor.setSize(WIDTH_SCREEN, HEIGHT_SCREEN);
        actor.setPosition(0f,0f);
    }

    public static float getXCenter() {
        return WIDTH_SCREEN*0.5f;
    }
    public static float getYByPercentage(float percentage) {
        return HEIGHT_SCREEN*percentage;
    }
    public static float getXByPercentage(float percentage) {
        return WIDTH_SCREEN*percentage;
    }


    public static TextButton getTextButton(String text, float widthPercentage, float wHRatio) {
        String style = "default";
        return getTextButton(text, widthPercentage, wHRatio, style);
    }
    public static TextButton getTextButton(String text, float widthPercentage, float wHRatio, String style) {
        TextButton textButton = new TextButton(text, SKIN, style);
        setSizeByWidthPercentage(textButton, widthPercentage, wHRatio);
        float scale = (0.75f * textButton.getWidth()) / textButton.getLabel().getWidth();
        textButton.getLabel().setFontScale(scale);
        return textButton;
    }

}
