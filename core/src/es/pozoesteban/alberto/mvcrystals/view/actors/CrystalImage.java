package es.pozoesteban.alberto.mvcrystals.view.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import es.pozoesteban.alberto.mvcrystals.controller.IllegalMove;
import es.pozoesteban.alberto.mvcrystals.controller.MVCrystalsGameController;
import es.pozoesteban.alberto.mvcrystals.model.GameState;
import es.pozoesteban.alberto.mvcrystals.model.elements.Color;
import es.pozoesteban.alberto.mvcrystals.model.token_position.InBoard;

import static es.pozoesteban.alberto.mvcrystals.Configuration.VIEW_LOGGER;

public class CrystalImage extends TokenImage {

    protected final MVCrystalsGameController controller;

    protected Texture crystalTexture;
    protected Texture crystalSelectedTexture;

    public CrystalImage(final GameState gameState, final MVCrystalsGameController controller, final int id, Color color,
                        BoardImage[] boards, FactoryImage[] factories, CenterPlateImage centerPlate, boolean oneHumanPlayerMode) {
        super(id, gameState, boards, factories, centerPlate, oneHumanPlayerMode);
        this.controller = controller;
        switch (color) {
            case BLUE:
                crystalTexture = new Texture("tokens/crystal_blue.png");
                crystalSelectedTexture = new Texture("tokens/crystal_blue_selected.png");
                break;
            case RED:
                crystalTexture = new Texture("tokens/crystal_red.png");
                crystalSelectedTexture = new Texture("tokens/crystal_red_selected.png");
                break;
            case YELLOW:
                crystalTexture = new Texture("tokens/crystal_yellow.png");
                crystalSelectedTexture = new Texture("tokens/crystal_yellow_selected.png");
                break;
            case WHITE:
                crystalTexture = new Texture("tokens/crystal_white.png");
                crystalSelectedTexture = new Texture("tokens/crystal_white_selected.png");
                break;
            case ORANGE:
                crystalTexture = new Texture("tokens/crystal_orange.png");
                crystalSelectedTexture = new Texture("tokens/crystal_orange_selected.png");
                break;
        }
        addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if (gameState.getCurrentPlayer().isHuman()) controller.selectCrystal(id);
                if (gameState.crystalInSlotPanel(id)) {
                    try {
                        controller.selectRow(gameState.getSlotPanelRow(id));
                    } catch (IllegalMove illegalMove) {}
                }
            }
            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                if (gameState.crystalInSlotPanel(id)) controller.setThinkingRow(gameState.getSlotPanelRow(id));
            }
            @Override
            public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
                if (gameState.crystalInSlotPanel(id)) controller.setThinkingRow(-1);
            }
        });
        addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (gameState.crystalInSlotPanel(id)) controller.setThinkingRow(gameState.getSlotPanelRow(id));
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if (gameState.crystalInSlotPanel(id)) controller.setThinkingRow(-1);
            }
        });
        addListener(new DragListener() {
            @Override
            public void drag(InputEvent event, float x, float y, int pointer) {
                if (gameState.getCurrentPlayer().isHuman()) {
                    moveBy(x - getWidth() / 2, y - getHeight() / 2);
                    InBoard newPosition = checkIfNewPosition();
                    if (newPosition != null) {
                        controller.setThinkingRow(newPosition.getRow());
                    } else {
                        controller.setThinkingRow(-1);
                    }
                }
            }
            @Override
            public void dragStart(InputEvent event, float x, float y, int pointer) {
                if (gameState.getCurrentPlayer().isHuman()) {
                    super.dragStart(event, x, y, pointer);
                    positionBeforeDrag = new float[]{getX(), getY()};
                    draging = true;
                    controller.resetSelectedCrystals();
                    controller.selectCrystal(id);
                }
            }

            @Override
            public void dragStop(InputEvent event, float x, float y, int pointer) {
                if (gameState.getCurrentPlayer().isHuman()) {
                    super.dragStop(event, x, y, pointer);
                    InBoard newPosition = checkIfNewPosition();
                    VIEW_LOGGER.info("Dropping crystal " + id + " to " + (newPosition != null ? newPosition.toString() : "null"));
                    if (newPosition != null) {
                        controller.resetSelectedCrystals();
                        controller.selectCrystal(id);
                        try {
                            controller.selectRow(newPosition.getRow());
                        } catch (IllegalMove illegalMove) {
                            controller.resetSelectedCrystals();
                            VIEW_LOGGER.info("Illegal drag move to " + newPosition.toString());
                        }
                    } else {
                        setPosition(positionBeforeDrag[0], positionBeforeDrag[1]);
                    }
                    draging = false;
                    controller.resetSelectedCrystals();
                }
            }
        });
    }

    private InBoard checkIfNewPosition() {
        return boards[gameState.getCurrentPlayer().getId()].checkIfTokenIsInNewPosition(getX(), getY(), getWidth(), getHeight());
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        boolean selected = gameState.crystalIsSelected(id);
        if (isVisible) {
            if (selected) batch.draw(crystalSelectedTexture, getX(), getY(), getWidth(), getHeight());
            else batch.draw(crystalTexture, getX(), getY(), getWidth(), getHeight());
        }
    }

    @Override
    public void dispose() {
        if (crystalTexture != null) crystalTexture.dispose();
        if (crystalSelectedTexture != null) crystalSelectedTexture.dispose();
    }
}
