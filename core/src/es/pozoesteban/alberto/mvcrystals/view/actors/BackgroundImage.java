package es.pozoesteban.alberto.mvcrystals.view.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import es.pozoesteban.alberto.mvcrystals.view.LayoutUtils;

public class BackgroundImage extends DisposableImage {

    private final Texture texture;

    public BackgroundImage() {
        this.texture = new Texture("background.png");
        LayoutUtils.fullScreen(this);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(texture, getX(), getY(), getWidth(), getHeight());
    }

    @Override
    public void dispose() {
        texture.dispose();
    }
}
