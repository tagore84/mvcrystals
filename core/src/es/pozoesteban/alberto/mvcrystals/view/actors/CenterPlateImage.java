package es.pozoesteban.alberto.mvcrystals.view.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import es.pozoesteban.alberto.mvcrystals.view.LayoutUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.Random;

public class CenterPlateImage extends DisposableImage {

    public static final float W_H_RATIO = 1536f/512f;

    private final int numSlots;

    protected Texture sackTexture;

    protected HashMap<Integer, float[]> positions;

    public CenterPlateImage(int numSlots, boolean oneHumanPlayerMode, Button pointsButton, BoardImage[] boards, FactoryImage[] factories) {
        this.numSlots = numSlots;
        this.sackTexture = new Texture("other_elements/plate_v2.png");
        this.positions = new HashMap();

        int numPlayers = 0;
        for (BoardImage board : boards) {
            if (board != null) numPlayers++;
        }
        setSize(oneHumanPlayerMode, boards, pointsButton, factories, numPlayers);

        if (oneHumanPlayerMode) {
            LayoutUtils.moveXDistributeNElements(
                    this, 0, 1,pointsButton.getX()+pointsButton.getWidth(), boards[0].getX());
            LayoutUtils.moveDown(this);
        } else {
            LayoutUtils.moveLeft(this);
            LayoutUtils.moveUp(this);
        }
        fillPositions();
    }

    private void setSize(boolean oneHumanPlayerMode, BoardImage[] boards, Button pointsButton, FactoryImage[] factories, int numPlayers) {
        float maxWidth, maxHeight;
        if (oneHumanPlayerMode) {
            maxWidth = LayoutUtils.calcMaxWidth(pointsButton.getX() + pointsButton.getWidth(),
                    boards[0].getX(), 1);
            maxHeight = LayoutUtils.calcMaxHeight(0, factories[0].getY(), 1);
            if ((maxWidth / W_H_RATIO) > maxHeight) {
                setSize(maxHeight*W_H_RATIO, maxHeight);
            } else {
                setSize(maxWidth, maxWidth/W_H_RATIO);
            }
        } else {
            float down = 0;
            float right = 0;
            for (BoardImage board : boards) {
                if (board != null) {
                    if (board.getPos() == 1) down = board.getY() + board.getHeight();
                    if (board.getPos() == 2) right = board.getX();
                }
            }
            maxHeight = LayoutUtils.calcMaxHeight(down, -0.0000001f, 1);
            if (numPlayers < 4) {
                setSize(maxHeight*W_H_RATIO, maxHeight);
            } else {
                maxWidth = LayoutUtils.calcMaxWidth(0, right, 1);
                if ((maxWidth / W_H_RATIO) > maxHeight) {
                    setSize(maxHeight*W_H_RATIO, maxHeight);
                } else {
                    setSize(maxWidth, maxWidth/W_H_RATIO);
                }
            }
        }
    }
    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(sackTexture, getX(), getY(), getWidth(), getHeight());
    }

    public void dispose() {
        sackTexture.dispose();
    }

    public float[] getTokenPositionByIndex(int positionIndex) {
        return positions.get(positionIndex);
    }
    public void fillPositions() {
        for (int i = 0; i < 29; i++) {
            //float xSide = numSlots == 28 ? 7.5f : 6.5f;
            //float ySide = numSlots == 16 ? 3.5f : 4.5f;
            float xSide = numSlots == 28 ? 10.5f : 8.5f;
            float ySide = numSlots == 16 ? 2.5f : 3.5f;
            float xPos = (i % (xSide-0.5f));// + 0.25f);
            float yPos = (i / (xSide-0.5f));// + 0.25f);
            float smallMargin = 1f;
            float slotSizeX = (getWidth() - (smallMargin * (xSide + 0.5f))) / xSide;
            float slotSizeY = (getHeight() - (smallMargin * (ySide + 0.5f))) / ySide;
            //float slotSize = Math.min(slotSizeX, slotSizeY);

            Random dice = new Random(new Date().getTime());
            float randomX = dice.nextFloat() * (getWidth()*0.005f) * (dice.nextFloat() > 0.5f ? -1 : 1);
            float randomY = dice.nextFloat() * (getHeight()*0.005f) * (dice.nextFloat() > 0.5f ? -1 : 1);

            float x = ((xPos + 1) * smallMargin) + ((xPos) * slotSizeX) + getX() + (randomX);
            float y = ((yPos + 1) * smallMargin) + ((yPos) * slotSizeY) + getY() + (randomY);

            positions.put(i, new float[]{x, y});
        }
    }


}