package es.pozoesteban.alberto.mvcrystals.view.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import es.pozoesteban.alberto.mvcrystals.view.LayoutUtils;

public class FactoryImage extends DisposableImage {

    public static final float W_H_RATIO = 1f;
    private static final float MAX_HEIGHT_NORMAL_VIEW = LayoutUtils.getHeightByPercentage(0.22f);
    private static final float MIN_HEIGHT_NORMAL_VIEW = LayoutUtils.getHeightByPercentage(0.16f);

    private static final float MAX_HEIGHT_BIG_VIEW = LayoutUtils.getHeightByPercentage(0.25f);

    protected Texture factoryTexture;

    private final int numFactories;
    private final boolean oneHumanPlayerMode;
    private final int index;
    private final int numPlayers;

    public FactoryImage(int index, boolean oneHumanPlayerMode, BoardImage[] boards, int numFactories, Button pointsButton) {
        factoryTexture = new Texture("factories/factory_" + index + ".png");
        this.numFactories = numFactories;
        this.oneHumanPlayerMode = oneHumanPlayerMode;
        this.index = index;
        int total = 0;
        for (BoardImage board : boards) {
            if (board != null) total++;
        }
        this.numPlayers = total;
        setSize(boards, pointsButton);
        setPosition(boards);
    }

    private void setSize(BoardImage[] boards, Button pointsButton) {
        if (oneHumanPlayerMode) {
            float otherBoardHeight = 0;
            for (int i = 1; i < boards.length; i++) {
                if (boards[i] != null) {
                    otherBoardHeight = boards[i].getHeight();
                    break;
                }
            }
            float height1 = LayoutUtils.calcMaxHeight(boards[0].getHeight() + otherBoardHeight, 1);
            float height2 = LayoutUtils.calcMaxHeight(pointsButton.getY() + pointsButton.getHeight(),
                    boards[0].getY() + boards[0].getHeight(), 1);
            float height = height1 < height2 ? height1 : height2;
            if (height > MAX_HEIGHT_BIG_VIEW) height = MAX_HEIGHT_BIG_VIEW;
            setSize(height*W_H_RATIO, height);
        } else {
            int numFactoriesInRow = 0;
            switch (numFactories) {
                case 5:
                    numFactoriesInRow = 3;
                    break;
                case 7:
                    numFactoriesInRow = 4;
                    break;
                case 9:
                    numFactoriesInRow = 5;
                    break;
            }
            float left = 0;
            float right = Float.MAX_VALUE;
            float down = 0;
            float up = Float.MAX_VALUE;
            for (BoardImage board : boards) {
                if (board != null) {
                    if (board.getPos() == 1) {
                        left = board.getX() + board.getWidth();
                        up = board.getY() + board.getHeight();
                    }
                    if (board.getPos() == 3) right = board.getX();
                    if (board.getPos() == 0) down = board.getY() + board.getHeight();
                    if (board.getPos() == 2) up = board.getY();
                }
            }
            float maxHeight = Math.max(MIN_HEIGHT_NORMAL_VIEW, Math.min(MAX_HEIGHT_NORMAL_VIEW, LayoutUtils.calcMaxHeight(down, up, 2)));
            float maxWidth = LayoutUtils.calcMaxWidth(left, right, numFactoriesInRow);
            if (maxHeight < (maxWidth/W_H_RATIO)) {

                setSize(maxHeight*W_H_RATIO, maxHeight);
            } else {
                setSize(maxWidth, maxWidth/W_H_RATIO);
            }
        }
    }
    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(factoryTexture, getX(), getY(), getWidth(), getHeight());
    }

    public void dispose() {
        factoryTexture.dispose();
    }

    private void setPosition(BoardImage[] boards) {
        int factoriesInDownRow, factoriesInUpRow;
        if (oneHumanPlayerMode) {
            switch (numFactories) {
                case 5:
                    factoriesInUpRow = 5;
                    factoriesInDownRow = 0;
                    break;
                case 7:
                    factoriesInUpRow = 5;
                    factoriesInDownRow = 2;
                    break;
                case 9:
                    factoriesInUpRow = 6;
                    factoriesInDownRow = 3;
                    break;
                default:
                    throw new IllegalArgumentException("Invalid num factories: " + numFactories);
            }
            if (index < factoriesInDownRow) {
                // DOWN ROW:
                LayoutUtils.moveXDistributeNElements(this, index+0.2f, factoriesInDownRow, 0, boards[0].getX());
                LayoutUtils.moveAlignUpYToOther(this, boards[0]);
            } else {
                // UP ROW:
                LayoutUtils.moveXDistributeNElements(this, (index-factoriesInDownRow), factoriesInUpRow);
                float otherBoardY = 0;
                for (int i = 1; i < boards.length; i++) {
                    if (boards[i] != null) {
                        otherBoardY = boards[i].getY();
                        break;
                    }
                }
                LayoutUtils.moveYDistributeNElements(this, 0, 1, boards[0].getY()+boards[0].getHeight(), otherBoardY);
                //if (otherBoardY - (getY()+getHeight()) < 50) setSize(getWidth()*0.95f, getHeight()*0.95f);
            }
        } else {
            float left = 0;
            float right = Float.MAX_VALUE;
            float down = 0;
            float up = Float.MAX_VALUE;
            for (BoardImage board : boards) {
                if (board != null) {
                    if (board.getPos() == 1) {
                        left = board.getX() + board.getWidth();
                        up = board.getY() + board.getHeight();
                    }
                    if (board.getPos() == 3) right = board.getX();
                    if (board.getPos() == 0) down = board.getY() + board.getHeight();
                    if (board.getPos() == 2) up = board.getY();
                }
            }
            switch (numFactories) {
                case 5:
                    factoriesInUpRow = 3;
                    factoriesInDownRow = 2;
                    break;
                case 7:
                    factoriesInUpRow = 4;
                    factoriesInDownRow = 3;
                    break;
                case 9:
                    factoriesInUpRow = 5;
                    factoriesInDownRow = 4;
                    break;
                default:
                    throw new IllegalArgumentException("Invalid num factories: " + numFactories);
            }
            if (index < factoriesInDownRow) {
                // DOWN ROW:
                LayoutUtils.moveXDistributeNElements(this, index+0.5f, factoriesInDownRow+1, left, right);
                if (numFactories == 5 ) LayoutUtils.moveVerticalCenter(this, -0.5f);
                else LayoutUtils.moveYDistributeNElements(this, 1, 2, down, up);

            } else {
                // UP ROW:
                LayoutUtils.moveXDistributeNElements(this, (index-factoriesInDownRow), factoriesInUpRow, left, right);
                if (numFactories == 5 ) LayoutUtils.moveVerticalCenter(this, +0.5f);
                else LayoutUtils.moveYDistributeNElements(this, 0, 2, down, up);
            }
        }
    }

    public float[] calcTokenPositionByIndex(int positionIndex, float tokenWidth, float tokenHeight) {
        float x, y;
        if (positionIndex == 0 || positionIndex == 2) {
            x = getX() + (getWidth() / 5f);
        } else {
            x = getX() + getWidth() - (getWidth() / 5f) - tokenWidth;
        }
        if (positionIndex == 0 || positionIndex == 3) {
            y = getY() + (getHeight() / 5f);
        } else {
            y = getY() + getHeight() - (getHeight() / 5f) - tokenHeight;
        }
        return new float[]{x, y};
    }

}