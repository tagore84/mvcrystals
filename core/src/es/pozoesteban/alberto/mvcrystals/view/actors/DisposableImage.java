package es.pozoesteban.alberto.mvcrystals.view.actors;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

public abstract class DisposableImage extends Image {
    public abstract void dispose();
}
