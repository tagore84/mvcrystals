package es.pozoesteban.alberto.mvcrystals.view.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import es.pozoesteban.alberto.mvcrystals.controller.IllegalMove;
import es.pozoesteban.alberto.mvcrystals.controller.MVCrystalsGameController;
import es.pozoesteban.alberto.mvcrystals.model.GameState;
import es.pozoesteban.alberto.mvcrystals.model.elements.Color;

public class CellSelectorImage extends DisposableImage {

    public static final float W_H_RATIO = 1f;


    private final MVCrystalsGameController controller;
    private final int row;
    private final int column;
    private final int playerIndex;
    private final BoardImage[] boards;
    private final GameState gameState;
    protected final Texture[] textureColors;
    protected final Texture textureBad;

    public CellSelectorImage(final GameState gameState, final MVCrystalsGameController controller, final int playerIndex,
                             final int row, int column, BoardImage[] boards, boolean oneHumanPlayerMode) {
        this.controller = controller;
        this.row = row;
        this.column = column;
        this.playerIndex = playerIndex;
        this.boards = boards;
        this.gameState = gameState;
        this.textureColors = new Texture[Color.values().length];
        for (Color color : Color.values()) {
            this.textureColors[color.getValue()] = new Texture("tokens/cell_selector_" + color.toOneChartString() + ".png");
        }
        this.textureBad = new Texture("tokens/cell_selector_floor.png");

        float width = boards[playerIndex].getTokenWidth();
        float height = width / W_H_RATIO;
        setSize(width, height);

        float[] position = boards[playerIndex].calcTokenPositionByRowColumn("SlotsPanel", width, height, row, column);
        setPosition(position[0], position[1]);

        addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if (gameState.getCurrentPlayer().getId() == playerIndex) {
                    try{
                        controller.selectRow(row);
                    } catch (IllegalMove illegalMove) {}
                }
            }
            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                controller.setThinkingRow(row);
            }

            @Override
            public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
                controller.setThinkingRow(-1);
            }
        });
        addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                controller.setThinkingRow(row);
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                controller.setThinkingRow(-1);
            }
        });

    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (gameState.getPlayer(playerIndex).isAI()) return;
        if (row < 5) {
            if (gameState.someCrystalSelected() &&
                    gameState.isRowSelectable(row) &&
                    gameState.getCurrentPlayer().getId() == playerIndex &&
                    (5 - gameState.numCrystalSelected()) < (gameState.getCurrentPlayer().getSlotsPanel().numCrystalsInRow(row)) + ((5 - row) + column) &&
                    column < (5 - gameState.getCurrentPlayer().getSlotsPanel().numCrystalsInRow(row))) {
                batch.draw(textureColors[gameState.getSelectedCrystals().get(0).getColor().getValue()], getX(), getY(), getWidth(), getHeight());
            }
        } else if (controller.getThinkingRow() == -1 &&
                    gameState.someCrystalSelected() &&
                    gameState.isRowSelectable(row) &&
                    gameState.getCurrentPlayer().getId() == playerIndex &&
                    column >= gameState.getCurrentPlayer().getSlotsPanel().numCrystalsInFloor() &&
                    column < (gameState.numCrystalSelected() + gameState.getCurrentPlayer().getSlotsPanel().numCrystalsInFloor())) {
            batch.draw(textureBad, getX(), getY(), getWidth(), getHeight());
        } else if (controller.getThinkingRow() >= 0 &&
                gameState.someCrystalSelected() &&
                gameState.isRowSelectable(row) &&
                gameState.getCurrentPlayer().getId() == playerIndex &&
                column < (gameState.getCurrentPlayer().getSlotsPanel().numCrystalsInFloor() + (gameState.numCrystalSelected() - controller.getEmptySlots(controller.getThinkingRow()))) &&
                column >= gameState.getCurrentPlayer().getSlotsPanel().numCrystalsInFloor()) {
            batch.draw(textureBad, getX(), getY(), getWidth(), getHeight());
        }
    }

    @Override
    public void dispose() {
        for (Texture textureColor : this.textureColors) {
            textureColor.dispose();
        }
        this.textureBad.dispose();
    }

}
