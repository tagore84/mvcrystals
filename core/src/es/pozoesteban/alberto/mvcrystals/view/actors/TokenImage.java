package es.pozoesteban.alberto.mvcrystals.view.actors;

import es.pozoesteban.alberto.mvcrystals.model.GameState;
import es.pozoesteban.alberto.mvcrystals.model.token_position.InBoard;
import es.pozoesteban.alberto.mvcrystals.model.token_position.InCenterPlate;
import es.pozoesteban.alberto.mvcrystals.model.token_position.InFactory;
import es.pozoesteban.alberto.mvcrystals.model.token_position.TokenPosition;

import static es.pozoesteban.alberto.mvcrystals.Configuration.VIEW_LOGGER;
import static es.pozoesteban.alberto.mvcrystals.view.screens.BaseScreen.MOVE_DISTANCE;


public abstract class TokenImage extends DisposableImage {

    public static final float W_H_RATIO = 1f;

    protected final GameState gameState;
    protected boolean isVisible;
    protected final BoardImage[] boards;
    private final FactoryImage[] factories;
    private final CenterPlateImage centerPlateActor;
    protected TokenPosition cacheTokenPosition;
    protected final int id;
    protected final boolean oneHumanPlayerMode;
    protected boolean draging;
    protected boolean moving;
    protected float[] positionBeforeDrag;

    public TokenImage(int id, GameState gameState, BoardImage[] boards, FactoryImage[] factories, CenterPlateImage centerPlateActor, boolean oneHumanPlayerMode) {
        this.id = id;
        this.gameState = gameState;
        this.boards = boards;
        this.factories = factories;
        this.centerPlateActor = centerPlateActor;
        this.oneHumanPlayerMode = oneHumanPlayerMode;
        this.draging = false;
        this.moving = false;
    }

    @Override
    public void act(float delta) {
        if (draging) return;
        TokenPosition newPosition;
        try {
            newPosition = gameState.tokenPosition(id, cacheTokenPosition);
            if (!newPosition.equals(cacheTokenPosition)) {
                moving = true;
                cacheTokenPosition = newPosition;
            }
            if (!moving && newPosition.equals(cacheTokenPosition)) { // Static
                return; // Do nothing
            }
            if (moving && newPosition.equals(cacheTokenPosition)) {
                TokenViewValues values = new TokenViewValues(cacheTokenPosition);
                setSize(values.width, values.height);
                isVisible = values.visible;
                setVisible(values.visible);
                if (values.visible) {
                    // move...
                    if (values.position == null) {
                        TokenViewValues debug = new TokenViewValues(cacheTokenPosition);
                    }
                    float distance = calcDistance(values.position[0], values.position[1], getX(), getY());
                    if (distance <= (MOVE_DISTANCE * MOVE_DISTANCE)) {
                        setPosition(values.position[0], values.position[1]);
                        moving = false;
                    } else {
                        float[] move = calcNewPosition(getX(), getY(), values.position[0], values.position[1]);
                        setPosition(move[0], move[1]);
                    }
                }
            }
        } catch (Exception e) {
            VIEW_LOGGER.throwing(this.getClass().getSimpleName(), "act", e);
            VIEW_LOGGER.warning(e.toString());
            VIEW_LOGGER.warning(gameState.toString());
            gameState.tokenPosition(id, cacheTokenPosition);
        }

    }

    private float[] calcNewPosition(float x0, float y0, float x, float y) {
        float[] move = new float[]{x, y};
        if (x0 < x) move[0] = x0 + MOVE_DISTANCE;
        if (x0 > x) move[0] = x0 - MOVE_DISTANCE;
        if (y0 < y) move[1] = y0 + MOVE_DISTANCE;
        if (y0 > y) move[1] = y0 - MOVE_DISTANCE;
        return move;
    }

    private float calcDistance(float x1, float y1, float x2, float y2) {
        return (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2);
    }

    private class TokenViewValues {
        private float[] position;
        private float width;
        private float height;
        private boolean visible;

        private TokenViewValues(float[] position, float width, float height, boolean visible) {
            this.position = position;
            this.width = width;
            this.height = height;
            this.visible = visible;
        }
        public TokenViewValues(TokenPosition tokenPosition) {
            if (tokenPosition instanceof InBoard) {
                width = boards[((InBoard) tokenPosition).getPlayerIndex()].getTokenWidth();
                height = width / W_H_RATIO;
                position = boards[((InBoard) tokenPosition).getPlayerIndex()].calcTokenPositionByRowColumn(
                        ((InBoard) tokenPosition).getBoardPlace(),
                        getWidth(),
                        getHeight(),
                        ((InBoard) tokenPosition).getRow(),
                        ((InBoard) tokenPosition).getColumn());
                visible = true;
            } else if (tokenPosition instanceof InFactory) {
                width = boards[0].getTokenWidth();
                height = width / W_H_RATIO;
                int factoryIndex = ((InFactory) tokenPosition).getFactoryIndex();
                int positionIndex = ((InFactory) tokenPosition).getPositionIndex();
                position = factories[factoryIndex].calcTokenPositionByIndex(positionIndex, getWidth(), getHeight());
                visible = true;
            } else if (tokenPosition instanceof InCenterPlate) {
                width = boards[0].getTokenWidth();
                height = width / W_H_RATIO;
                int positionIndex = ((InCenterPlate) tokenPosition).getPositionIndex();
                position = centerPlateActor.getTokenPositionByIndex(positionIndex);
                visible = true;
            } else {
                visible = false;
            }
        }
    }
}
