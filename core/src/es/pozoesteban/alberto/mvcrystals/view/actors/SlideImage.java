package es.pozoesteban.alberto.mvcrystals.view.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import es.pozoesteban.alberto.mvcrystals.view.screens.HowToPlayScreen;
import es.pozoesteban.alberto.mvcrystals.view.LayoutUtils;

import static es.pozoesteban.alberto.mvcrystals.Configuration.VIEW_LOGGER;

public class SlideImage extends DisposableImage {

    private final int index;
    private final Texture texture;
    public static final String FILE_NAME = "diapositiva";
    private float dragStartX;

    public SlideImage(int index, final HowToPlayScreen screen) {
        this.index = index;
        if (LayoutUtils.getScreenRatio() == 16f/9f) {
            this.texture = new Texture("how_to_play/ratio_16_9/" + FILE_NAME + (index + 1) + ".png");
        } else if (LayoutUtils.getScreenRatio() == 16f/10f) {
            this.texture = new Texture("how_to_play/ratio_16_10/" + FILE_NAME + (index + 1) + ".png");
        } else {
            VIEW_LOGGER.warning("No slides math the screen ratio... loading 16_9");
            this.texture = new Texture("how_to_play/ratio_16_9/" + FILE_NAME + (index + 1) + ".png");
        }

        addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if (dragStartX == 0) screen.next();
            }
        });
        addListener(new DragListener() {
            @Override
            public void drag(InputEvent event, float x, float y, int pointer) {
                if (dragStartX != 0) {
                    moveBy(x - dragStartX, 0);
                }
            }
            @Override
            public void dragStart(InputEvent event, float x, float y, int pointer) {
                super.dragStart(event, x, y, pointer);
                dragStartX = x;
            }

            @Override
            public void dragStop(InputEvent event, float x, float y, int pointer) {
                super.dragStop(event, x, y, pointer);
                if (getX() < LayoutUtils.getWidthByPercentage(0.05f)) {
                    screen.next();
                } else if (getX() > (-1 * LayoutUtils.getWidthByPercentage(0.05f))){
                    screen.previous();
                }
                setPosition(0, 0);
                dragStartX = 0;
            }
        });

        LayoutUtils.fullScreen(this);
    }

    @Override
    public void draw(Batch batch, float parentAlpha)  {
        if(isVisible()) {
            batch.draw(texture, getX(), getY(), getWidth(), getHeight());
        }
    }
    @Override
    public void dispose() {
        texture.dispose();
    }

    public void setCurrentSlide(int tipIndex) {
        setVisible(tipIndex == index || tipIndex == (index-1));
    }
}
