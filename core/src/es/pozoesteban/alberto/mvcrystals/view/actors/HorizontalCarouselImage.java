package es.pozoesteban.alberto.mvcrystals.view.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

import static es.pozoesteban.alberto.mvcrystals.Configuration.HORIZONTAL_CAROUSEL_ACTOR_MOVE;

public class HorizontalCarouselImage extends DisposableImage {

    private final Texture texture;
    private int position;
    private final int numElements;

    private float originalWidth;
    private float originalHeight;
    private float originalX;
    private float originalY;

    public HorizontalCarouselImage(String textureName, int initialPosition, int numElements, float width, float height, float x, float y) {
        this.texture = new Texture(textureName);
        this.position = initialPosition;
        this.numElements = numElements;
        this.originalWidth = width;
        this.originalHeight = height;
        this.originalX = x;
        this.originalY = y;
        if (position == 0) {
            setSize(width, height);
        } else {
            setSize(0, 0);
        }
        setPosition(x + (2*position*width), position == 0 ? y : y+(height/2f));
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(texture, getX(), getY(), getWidth(), getHeight());
    }

    public void next() {
        position--;
        if (position < (numElements/-2)) {
            position = numElements/2;
            setPosition(originalX + (2f*originalWidth), originalY + (originalHeight/2f));
            setSize(0f,0f);
        }
        move(true);
    }
    public void previous() {
        position++;
        if (position > (numElements/2)) {
            position = numElements/-2;
            setPosition(originalX - (2f*originalWidth), originalY + (originalHeight/2f));
            setSize(0f,0f);
        }
        move(false);
    }

    private void move(boolean left) {
        switch (position) {
            case -1:
                addAction(Actions.moveTo(originalX - originalWidth, originalY + (originalHeight/2f), HORIZONTAL_CAROUSEL_ACTOR_MOVE));
                addAction(Actions.sizeTo(0, 0, HORIZONTAL_CAROUSEL_ACTOR_MOVE));
                break;
            case 0:
                addAction(Actions.moveTo(originalX, originalY, HORIZONTAL_CAROUSEL_ACTOR_MOVE));
                addAction(Actions.sizeTo(originalWidth, originalHeight, HORIZONTAL_CAROUSEL_ACTOR_MOVE));
                break;
            case 1:
                addAction(Actions.moveTo(originalX + (2f*originalWidth), originalY + (originalHeight/2f), HORIZONTAL_CAROUSEL_ACTOR_MOVE));
                addAction(Actions.sizeTo(0, 0, HORIZONTAL_CAROUSEL_ACTOR_MOVE));
                break;
        }
    }

    @Override
    public void dispose() {
        texture.dispose();
    }

    public void setValues(float x, float y, float width, float height) {
        this.originalX = x;
        this.originalY = y;
        this.originalWidth = width;
        this.originalHeight = height;
        if (position == 0) {
            setSize(width, height);
        } else {
            setSize(0, 0);
        }
        setPosition(x + (2*position*width), position == 0 ? y : y+(height/2f));
    }
}
