package es.pozoesteban.alberto.mvcrystals.view.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import es.pozoesteban.alberto.mvcrystals.model.GameState;
import es.pozoesteban.alberto.mvcrystals.model.player.PlayerColor;
import es.pozoesteban.alberto.mvcrystals.model.token_position.InBoard;
import es.pozoesteban.alberto.mvcrystals.view.LayoutUtils;

import java.util.HashMap;
import java.util.Map;

import static es.pozoesteban.alberto.mvcrystals.Configuration.MODEL_LOGGER;

public class BoardImage extends DisposableImage {

    public static final float W_H_RATIO = 3072f/2048f;

    public static final float MAX_STANDARD_WIDTH = 0.25f;

    public static final float MAX_SMALL_WIDTH = 0.14f;

    public static final float MAX_BIG_WIDTH = 0.40f;
    public static final float MAX_BIG_HEIGHT = 0.46f;

    protected Texture playerBoardTexture;
    protected Texture playerBoardTextureCurrent;
    private final GameState gameState;
    private final boolean oneHumanPlayerMode;
    private final PlayerColor color;
    private final int playerIndex;
    private final boolean spun;
    private int pos;
    private int total;

    private final Map<TokenPositionKey, float[]> positionKeyMap;

    public BoardImage(final GameState gameState, int playerIndex, boolean oneHumanPlayerMode, PlayerColor color) {
        this.gameState = gameState;
        this.playerIndex = playerIndex;
        this.oneHumanPlayerMode = oneHumanPlayerMode;
        this.positionKeyMap = new HashMap();
        this.color = color;
        this.total = gameState.getNumPlayers();
        if (oneHumanPlayerMode) pos = -1;
        else {
            switch (total) {
                case 2:
                    if (playerIndex == 0) pos = 1;
                    else pos = 3;
                    break;
                case 3:
                    if (playerIndex == 0) pos = 0;
                    if (playerIndex == 1) pos = 1;
                    if (playerIndex == 2) pos = gameState.getPlayer(1) == null ?  1 : 3;
                    if (playerIndex == 3) pos = 3;
                    break;
                case 4:
                    pos = playerIndex;
                    break;
            }
        }
        this.spun = !oneHumanPlayerMode && (pos == 1 || pos == 3);
        setSize();
        setPosition();

        addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                MODEL_LOGGER.info(gameState.toString());
            }
        });

    }
    private void setTexture(int pos) {
        playerBoardTexture = new Texture("boards/player_board_3072_2048_" + pos + ".png");
        playerBoardTextureCurrent = new Texture("boards/player_board_3072_2048_" + pos + "_" + color.name().toLowerCase() + ".png");
    }
    private void setSize() {
        if (oneHumanPlayerMode) {
            if (playerIndex == 0) {
                // BIG BOARD:
                float screenRatio = LayoutUtils.getScreenRatio();
                float boardMaxRatio = MAX_BIG_WIDTH/MAX_BIG_HEIGHT;
                if (screenRatio < boardMaxRatio) {
                    LayoutUtils.setSizeByWidthPercentage(this, MAX_BIG_WIDTH, W_H_RATIO);
                } else {
                    LayoutUtils.setSizeByHeightPercentage(this, MAX_BIG_HEIGHT, W_H_RATIO);
                }
            } else {
                // SMALL BOARDS:
                LayoutUtils.setSizeByWidthPercentage(this, MAX_SMALL_WIDTH, W_H_RATIO);
            }
        } else {
            // STANDARD BOARDS:
            LayoutUtils.setSizeByWidthPercentage(this, MAX_STANDARD_WIDTH, W_H_RATIO);
            if (spun) {
                // SPUN
                setSize(getHeight(), getWidth());
            }
        }
    }
    private void setPosition() {
        if (oneHumanPlayerMode && playerIndex == 0) {
            LayoutUtils.moveDown(this);
            LayoutUtils.moveRight(this);
            setTexture(0);
        } else if (oneHumanPlayerMode) {
            LayoutUtils.moveUp(this);
            int total = gameState.getNumPlayers() - 1;
            int pos = -1;
            switch (playerIndex) {
                case 1:
                    pos = 0;
                    break;
                case 2:
                    if (gameState.getPlayer(1) == null) pos = 0;
                    else pos = 1;
                    break;
                case 3:
                    pos = total - 1;
                    break;
            }
            LayoutUtils.moveXDistributeNElements(this, pos, total);
            setTexture(0);
        } else {
            // !oneHumanPlayerMode
            switch (pos) {
                case 0:
                    LayoutUtils.moveDown(this);
                    LayoutUtils.moveHorizontalCenter(this);
                    setTexture(0);
                    break;
                case 1:
                    LayoutUtils.moveLeft(this);
                    LayoutUtils.moveVerticalCenter(this);
                    setTexture(1);
                    break;
                case 2:
                    LayoutUtils.moveHorizontalCenter(this);
                    LayoutUtils.moveUp(this);
                    setTexture(2);
                    break;
                case 3:
                    LayoutUtils.moveRight(this);
                    LayoutUtils.moveVerticalCenter(this);
                    setTexture(3);
                    break;
            }
        }
    }


    //<editor-fold desc="Token Position and Size Methods">
    public float getTokenWidth() {
        float ratio = 249f/3072f;
        return ratio * (!spun ? getWidth() : getHeight());
    }
    public float[] calcTokenPositionByRowColumn(String panel, float crystalWidth, float crystalHeight, int row, int column) {
        TokenPositionKey key = new TokenPositionKey(panel, crystalWidth, crystalHeight, row, column);
        if (positionKeyMap.containsKey(key)) return positionKeyMap.get(key);
        if (row < 5) {
            float[] output = calcTokenPositionInPanels(panel, crystalWidth, crystalHeight, row, column);
            positionKeyMap.put(key, output);
            return output;
        }
        if (row == 5) {
            float[] output = calcTokenPositionInFloor(crystalWidth, crystalHeight, column);
            positionKeyMap.put(key, output);
            return output;
        }
        throw new IllegalArgumentException("Invalid row number: " + row);
    }
    private float[] calcTokenPositionInFloor(float crystalWidth, float crystalHeight, int column) {
        float x, y, initX, initY;
        float leftLayout = 0.045f * (!spun ? getWidth() : getHeight());
        float downLayout =  0.073f * (spun ? getWidth() : getHeight());
        float crystalSeparatorX = 0.012f * (!spun ? getWidth() : getHeight());
        if (oneHumanPlayerMode || pos == 0) {
            initX = getX() + leftLayout;
            x = initX + (column * (crystalWidth + crystalSeparatorX));
            y = getY() + downLayout;
            return new float[]{x, y};
        } else {
            switch (pos) {
                case 1:
                    x = getX() + downLayout;
                    initY = getY() + getHeight() - crystalHeight - leftLayout;
                    y = initY - (column * (crystalHeight + crystalSeparatorX));
                    return new float[]{x, y};
                case 2:
                    initX = getX() + getWidth() - crystalWidth - leftLayout;
                    x = initX - (column * (crystalWidth + crystalSeparatorX));
                    y = getY() + getHeight() - crystalHeight - downLayout;
                    return new float[]{x, y};
                case 3:
                    x = getX() + getWidth() - crystalWidth - downLayout;
                    initY = getY() + leftLayout;
                    y = initY + (column * (crystalHeight + crystalSeparatorX));
                    return new float[]{x, y};
            }
        }
        throw new IllegalArgumentException("Illegal token position: playerIndex " + playerIndex + " floor column " + column);
    }
    private float[] calcTokenPositionInPanels(String panel, float crystalWidth, float crystalHeight, int row, int column) {
        float x, y, initX, initY;
        float leftLayout = panel.equalsIgnoreCase("SlotsPanel") ? 0.0435f * (!spun ? getWidth() : getHeight()) : 0.521f * (!spun ? getWidth() : getHeight());
        float downLayout = 0.285f * (spun ? getWidth() : getHeight());
        float crystalSeparatorX = 0.0098f * (!spun ? getHeight() : getWidth());
        float crystalSeparatorY = 0.0102f * (!spun ? getHeight() : getWidth());
        column = panel.equalsIgnoreCase("SlotsPanel") ? 4 - column : column;
        if (oneHumanPlayerMode || pos == 0) {
            initX = getX() + leftLayout;
            x = initX + (column * (crystalWidth + crystalSeparatorX));
            initY = getY() + downLayout;
            y = initY + ((4 - row) * (crystalHeight + crystalSeparatorY));
            return new float[]{x, y};
        } else {
            switch (pos) {
                case 0:
                    initX = getX() + leftLayout;
                    x = initX + (column * (crystalWidth + crystalSeparatorX));
                    initY = getY() + downLayout;
                    y = initY + ((4 - row) * (crystalHeight + crystalSeparatorY));
                    return new float[]{x, y};
                case 1:
                    initX = getX() + downLayout;
                    x = initX + ((4 - row) * (crystalHeight + crystalSeparatorY));
                    initY = getY() + getHeight() - crystalHeight - leftLayout;
                    y = initY - (column * (crystalWidth + crystalSeparatorX));
                    return new float[]{x, y};
                case 2:
                    initX = getX() + getWidth() - crystalWidth - leftLayout;
                    x = initX - (column * (crystalWidth + crystalSeparatorX));
                    initY = getY() + getHeight() - crystalHeight - downLayout;
                    y = initY - ((4 - row) * (crystalHeight + crystalSeparatorY));
                    return new float[]{x, y};
                case 3:
                    initX = getX() + getWidth() - crystalWidth - downLayout;
                    x = initX - ((4 - row) * (crystalHeight + crystalSeparatorY));
                    initY = getY() + leftLayout;
                    y = initY + (column * (crystalWidth + crystalSeparatorX));
                    return new float[]{x, y};
            }
        }
        throw new IllegalArgumentException("Illegal token position: playerIndex " + playerIndex + " row " + row + " column " + column);
    }
    public InBoard checkIfTokenIsInNewPosition(float x, float y, float crystalWidth, float crystalHeight) {
        if (x < getX() || x > (getX() + getWidth())) return null;
        if (y < getY() || y > (getY() + getHeight())) return null;
        int row = calcRowByPosition(x, y, crystalWidth, crystalHeight);
        if (row >= 0) return new InBoard(playerIndex, gameState.getCurrentPlayer().getSlotsPanel().getClass().getSimpleName(), row, 0);
        return null;
    }
    private int calcRowByPosition(float x, float y, float crystalWidth, float crystalHeight) {
        for (int row = 0; row < 6; row++) {
            float[] rightColumn = calcTokenPositionByRowColumn(gameState.getCurrentPlayer().getSlotsPanel().getClass().getSimpleName(), crystalWidth, crystalHeight, row, 0);
            float[] leftColumn = calcTokenPositionByRowColumn(gameState.getCurrentPlayer().getSlotsPanel().getClass().getSimpleName(), crystalWidth, crystalHeight, row, row == 5 ? 6 : row);
            if (row == 5) { // Flipp
                float[] temp = new float[]{rightColumn[0], rightColumn[1]};
                rightColumn[0] = leftColumn[0];
                rightColumn[1] = leftColumn[1];
                leftColumn[0] = temp[0];
                leftColumn[1] = temp[1];
            }
            if (oneHumanPlayerMode || pos == 0) {
                if (x >= (leftColumn[0] - (crystalWidth / 2f)) && x <= (rightColumn[0] + (crystalWidth / 2f)) &&
                    y >= (rightColumn[1] - (crystalHeight / 2f)) && y <= (rightColumn[1] + (crystalHeight / 2f))) {
                    return row;
                }
            } else {
                switch (pos) {
                    case 1:
                        if (x >= (rightColumn[0] - (crystalWidth / 2f)) && x <= (rightColumn[0] + (crystalWidth / 2f)) &&
                            y >= (rightColumn[1] - (crystalHeight / 2f)) && y <= (leftColumn[1] + (crystalHeight / 2f))) {
                            return row;
                        }
                        break;
                    case 2:
                        if (x >= (rightColumn[0] - (crystalWidth / 2f)) && x <= (leftColumn[0] + (crystalWidth / 2f)) &&
                            y >= (rightColumn[1] - (crystalHeight / 2f)) && y <= (rightColumn[1] + (crystalHeight / 2f))) {
                            return row;
                        }
                        break;
                    case 3:
                        if (x >= (rightColumn[0] - (crystalWidth / 2f)) && x <= (rightColumn[0] + (crystalWidth / 2f)) &&
                            y >= (leftColumn[1] - (crystalHeight / 2f)) && y <= (rightColumn[1] + (crystalHeight / 2f))) {
                            return row;
                        }
                        break;
                }
            }
        }
        return -1;
    }
    //</editor-fold>

    //<editor-fold desc="Override Actor Methods">
    @Override
    public void draw(Batch batch, float parentAlpha)  {
        if (gameState.isCurrentPlayer(playerIndex)) {
            batch.draw(playerBoardTextureCurrent, getX(), getY(), getWidth(), getHeight());
        } else {
            batch.draw(playerBoardTexture, getX(), getY(), getWidth(), getHeight());
        }
    }
    @Override
    public void dispose() {
        playerBoardTextureCurrent.dispose();
        playerBoardTexture.dispose();
    }
    //</editor-fold>

    private class TokenPositionKey {
        private final String panel;
        private final float crystalWidth;
        private final float crystalHeight;
        private final int row;
        private final int column;
        public TokenPositionKey(String panel, float crystalWidth, float crystalHeight, int row, int column) {
            this.panel = panel;
            this.crystalWidth = crystalWidth;
            this.crystalHeight = crystalHeight;
            this.row = row;
            this.column = column;
        }
    }

    public int getPos() {
        return this.pos;
    }
}
