package es.pozoesteban.alberto.mvcrystals.view.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import es.pozoesteban.alberto.mvcrystals.model.GameState;

public class FirstPlayerTokenImage extends TokenImage {

    protected final Texture texture;

    public FirstPlayerTokenImage(final GameState gameState, BoardImage[] boards, FactoryImage[] factories,
                                 CenterPlateImage centerPlate, boolean oneHumanPlayerMode) {
        super(0, gameState, boards, factories, centerPlate, oneHumanPlayerMode);
        this.texture = new Texture("tokens/first_token.png");
    }



    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (isVisible) {
            batch.draw(texture, getX(), getY(), getWidth(), getHeight());
        }
    }

    @Override
    public void dispose() {
        texture.dispose();
    }

}