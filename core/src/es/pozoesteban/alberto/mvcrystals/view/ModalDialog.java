package es.pozoesteban.alberto.mvcrystals.view;

import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

import static es.pozoesteban.alberto.mvcrystals.view.screens.BaseScreen.SKIN;

public class ModalDialog extends Dialog {

    public ModalDialog (String title) {
        super(title, SKIN);
        initialize();
    }

    private void initialize() {
        padTop(60); // set padding on top of the dialog title
        getButtonTable().defaults().height(60); // set buttons height
        setModal(true);
        setMovable(false);
        setResizable(false);
    }

    @Override
    public ModalDialog text(String text) {
        //Label label = new Label(text, SKIN, "title");
        Label label = new Label(text, SKIN);
        super.text(label);
        return this;
    }

    /**
     * Adds a text button to the button table.
     * @param listener the input listener that will be attached to the button.
     */
    public ModalDialog button(String buttonText, InputListener listener) {
        TextButton button = new TextButton(buttonText, SKIN, "size_80");
        button.addListener(listener);
        button(button);
        return this;
    }

    @Override
    public float getPrefWidth() {
        // force dialog width
        return LayoutUtils.getWidthByPercentage(0.7f);
    }

    @Override
    public float getPrefHeight() {
        // force dialog height
        return LayoutUtils.getHeightByPercentage(0.7f);
    }
}