package es.pozoesteban.alberto.mvcrystals.controller.players.support;

import es.pozoesteban.alberto.mvcrystals.model.GameState;
import es.pozoesteban.alberto.mvcrystals.model.elements.Crystal;
import es.pozoesteban.alberto.mvcrystals.model.player.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MoveTree implements Comparable<MoveTree>{

    protected final Crystal crystal;
    protected final int row;
    protected final Player player;
    protected final GameState gameState;
    protected final List<MoveTree> children;
    protected MoveTree parent;
    protected final int numCrystals;

    protected float diffValue;
    protected float value;

    public MoveTree(Crystal crystal, int row, Player player, GameState gameState, int numCrystals) {
        this.crystal = crystal;
        this.row = row;
        this.player = player;
        this.gameState = gameState;
        this.children = new ArrayList();
        this.numCrystals = numCrystals;
    }

    public void addChild(MoveTree moveTree) {
        children.add(moveTree);
        moveTree.setParent(this);
    }

    private void setParent(MoveTree parent) {
        this.parent = parent;
    }

    @Override
    public int compareTo(MoveTree o) {
        if (value > o.value) return -1;
        if (value < o.value) return 1;
        return 0;
    }

    public boolean isRoot() {
        return parent == null;
    }
    public boolean isLeaf() {
        return children.isEmpty();
    }

    public GameState getGameState() {
        return gameState;
    }

    public Player getPlayer() {
        return player;
    }

    public void setValue(float points) {
        value = points;
    }
    public void setDiffValue(float points) {
        diffValue = points;
    }

    public List<MoveTree> getChildren() {
        return children;
    }

    public void pruneNonMaximals(int numChildsOutput) {
        if (isLeaf()) return;
        Collections.sort(children);
        List<MoveTree> bestChilds = new ArrayList(children.subList(0, Math.min(numChildsOutput, children.size())));
        children.clear();
        children.addAll(bestChilds);
    }

    public Crystal getCrystal() {
        return crystal;
    }

    public int getRow() {
        return row;
    }

    public MoveTree getLeaf() {
        if (isLeaf()) return this;
        else return children.get(0).getLeaf();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Integer.toHexString(hashCode()));
        sb.append(player).append(" move ").append(crystal).append(" |").append(numCrystals);
        sb.append("|").append(" to row ").append(row);
        sb.append(" [").append(value).append("]").append(" (diff: ").append(diffValue).append(")");
        return sb.toString();
    }

    public int getNumCrystals() {
        return numCrystals;
    }

    public List<MoveTree> getLeafs() {
        List<MoveTree> leafs = new ArrayList();
        if (this.children.isEmpty()) {
            leafs.add(this);
        } else {
            for (MoveTree child : this.children) {
                leafs.addAll(child.getLeafs());
            }
        }
        return leafs;
    }
    public int numNodes() {
        return numNodes(0);
    }

    private int numNodes(int total) {
        int currentTotal = total + 1;
        if (isLeaf()) return currentTotal;
        for(MoveTree child : children) {
            currentTotal = child.numNodes(currentTotal);
        }
        return currentTotal;

    }

    public int getDeep() {
        if (isLeaf()) return 1;
        else return 1 + children.get(0).getDeep();
    }
}
