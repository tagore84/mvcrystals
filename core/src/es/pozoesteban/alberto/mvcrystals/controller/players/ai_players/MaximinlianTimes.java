package es.pozoesteban.alberto.mvcrystals.controller.players.ai_players;

import com.badlogic.gdx.utils.Timer;
import es.pozoesteban.alberto.mvcrystals.controller.IllegalMove;
import es.pozoesteban.alberto.mvcrystals.controller.MVCrystalsGameController;
import es.pozoesteban.alberto.mvcrystals.controller.players.support.AIPlayerController;
import es.pozoesteban.alberto.mvcrystals.controller.players.support.MoveTree;
import es.pozoesteban.alberto.mvcrystals.model.GameState;
import es.pozoesteban.alberto.mvcrystals.model.elements.Color;
import es.pozoesteban.alberto.mvcrystals.model.elements.Crystal;
import es.pozoesteban.alberto.mvcrystals.model.elements.Factory;
import es.pozoesteban.alberto.mvcrystals.model.elements.TokenWarehouse;
import es.pozoesteban.alberto.mvcrystals.model.player.Player;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static es.pozoesteban.alberto.mvcrystals.Configuration.CONTROLLER_LOGGER;

public class MaximinlianTimes extends AIPlayerController {

    // Estudiar qué tal se comporta con esto, parece que no muy bien pero faltan datos...
    // si funciona bien, parametrizar...

    private final float numCrystalsFactor;
    private final float ruinFactor;
    private final float maxTimeInSeconds;
    private final int maxSpace;
    private int currentNumNodes;

    private float totalSecondsThinking;

    private GameState originalGameState;
    private MVCrystalsGameController controller;

    private Date endTime;

    public MaximinlianTimes(Player player, int maxSpace, float maxTimeInSeconds, float numCrystalsFactor, float ruinFactor) {
        super(player);
        this.totalSecondsThinking = 0;
        this.maxSpace = maxSpace;
        this.numCrystalsFactor = numCrystalsFactor;
        this.maxTimeInSeconds = maxTimeInSeconds;
        this.ruinFactor = ruinFactor;
    }

    @Override
    public void onBegingTurn(final MVCrystalsGameController controller, GameState gameState) {
        if (controller.getUi() && !gameState.isClone()) {
            Timer.schedule(new Timer.Task(){
                @Override
                public void run() {
                    onBegingTurnTask(controller);
                }
            }, 1);
        } else {
            onBegingTurnTask(controller);
        }
    }

    public int getTotalSecondsThingking() {
        return (int)totalSecondsThinking;
    }

    private void onBegingTurnTask(MVCrystalsGameController controller) {
        Date startThinking = new Date();
        endTime = new Date((int)(startThinking.getTime() + (maxTimeInSeconds*1000f)));
        this.currentNumNodes = 0;
        this.controller = controller;
        this.originalGameState = controller.getGameState();

        // Para la primera búsqueda de movimientos no se filtra por space...
        List<MoveTree> roots = noFloorMoves(player, new GameState(this.originalGameState));
        MoveTree bestMove = null;
        float bestDiffValue = -10000;
        if (!roots.isEmpty()) {
            for (MoveTree root : roots) {
                recursiveDeepMove(root, roots.size());
            }

            for (MoveTree moveTree : roots) {
                float diffValue = diffValue(moveTree);
                moveTree.setDiffValue(diffValue);
                if (diffValue > bestDiffValue) {
                    bestDiffValue = diffValue;
                    bestMove = moveTree;
                }
            }
        } else {
            bestMove = floorMove();
        }
        //CONTROLLER_LOGGER.info(player.getName() + " made " + (roots.size() * roots.get(0).numNodes()) + " nodes in " + ((end.getTime()-start.getTime())/1000) + " seconds!");
        //CONTROLLER_LOGGER.info("Best move selected: " + bestMove + " (diff: " + bestDiffValue + ") from " + bestMove.numNodes() + " nodes and " + bestMove.getDeep() + " deep!");
        controller.setGameState(originalGameState);
        Date endThingking = new Date();
        try {
            totalSecondsThinking += (endThingking.getTime()-startThinking.getTime())/1000f;
            controller.selectCrystal(bestMove.getCrystal().getId());
            controller.selectRow(bestMove.getRow());
        } catch (IllegalMove illegalMove) {
            CONTROLLER_LOGGER.warning(illegalMove.toString());
            CONTROLLER_LOGGER.throwing(this.getClass().getSimpleName(), "onBegingTurn", illegalMove);
        }

    }

    private float diffValue(MoveTree moveTree) {
        float initPoints = AIUtils.valueGameState(new GameState(originalGameState), player, ruinFactor);
        float bestLeafPoints = -10000;
        MoveTree bestLeaf = null;
        for (MoveTree leaf : moveTree.getLeafs()) {
            float finalPoints = AIUtils.valueGameState(leaf.getGameState(), player, ruinFactor);
            if (finalPoints > bestLeafPoints) {
                bestLeafPoints = finalPoints;
                bestLeaf = leaf;
            }
        }
        float numCrystalsBonus = (float)moveTree.getNumCrystals() * numCrystalsFactor;
        return (bestLeafPoints - initPoints) + numCrystalsBonus;
    }

    private void valueMove(MoveTree move) {
        move.setValue(AIUtils.valueGameState(move.getGameState(), move.getPlayer(), ruinFactor));
    }


    private void recursiveDeepMove(MoveTree moveTree, int numNodes) {
        if (new Date().getTime() < endTime.getTime()) {
            // Recursive case...
            GameState currentGameState = new GameState(moveTree.getGameState());
            Player player = currentGameState.whoIsNextPlayer();
            List<MoveTree> childMoves = noFloorMoves(player, currentGameState);
            for (MoveTree child : childMoves) {
                moveTree.addChild(child);
                valueMove(child);
            }
            int numChilds = moveTree.getPlayer().equals(player) ? 1: maxSpace;
            this.currentNumNodes += numChilds;
            moveTree.pruneNonMaximals(numChilds);
            for (MoveTree child : moveTree.getChildren()) {
                recursiveDeepMove(child, currentNumNodes);
            }
        } else {
            // Return case...
            return;
        }
    }

    private List<MoveTree> noFloorMoves(Player player, GameState gameState) {
        List<MoveTree> noFloorMoves = new ArrayList();
        for (TokenWarehouse tokenWarehouse : gameState.getTokenWarehouses()) {
            for (Color color : Color.values()) {
                List<Crystal> byColor = tokenWarehouse.getCrystals(color);
                if (!byColor.isEmpty()) {
                    for (int row = 0; row < 5; row++) {
                        GameState moveGameState = new GameState(gameState);
                        moveGameState.setCurrentPlayer(moveGameState.getPlayer(player.getId()));
                        controller.setGameState(moveGameState);
                        if (moveGameState.isRowSelectable(row, byColor.get(0).getColor())) {
                            try {
                                controller.selectCrystal(byColor.get(0).getId());
                                controller.selectRow(row);
                                noFloorMoves.add(new MoveTree(byColor.get(0), row, player, moveGameState, byColor.size()));
                            } catch (IllegalMove illegalMove) {
                                CONTROLLER_LOGGER.throwing(this.getClass().getSimpleName(), "noFloorMoves", illegalMove);
                            }
                        }
                    }
                }
            }
        }
        return noFloorMoves;
    }

    private MoveTree floorMove() {
        int minCrystalsToFloor = 20;
        Crystal selected = null;
        for (Factory factory : originalGameState.getFactories()) {
            for (Color color : Color.values()) {
                List<Crystal> colorCrystals = factory.getCrystals(color);
                if (colorCrystals.size() > 0 && colorCrystals.size() < minCrystalsToFloor) {
                    selected = colorCrystals.get(0);
                    minCrystalsToFloor = colorCrystals.size();
                }
            }
        }
        if (selected == null) {
            for (Color color : Color.values()) {
                List<Crystal> colorCrystals = originalGameState.getCenterPlate().getCrystals(color);
                if (colorCrystals.size() > 0 && colorCrystals.size() < minCrystalsToFloor) {
                    selected = colorCrystals.get(0);
                    minCrystalsToFloor = colorCrystals.size();
                }
            }
        }
        //Crystal crystal, int row, Player player, GameState gameState, int numCrystal
        return new MoveTree(selected, 5, null, null, minCrystalsToFloor);
    }

}
