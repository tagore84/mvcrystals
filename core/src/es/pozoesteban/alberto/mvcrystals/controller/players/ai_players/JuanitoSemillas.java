package es.pozoesteban.alberto.mvcrystals.controller.players.ai_players;

import es.pozoesteban.alberto.mvcrystals.model.player.Player;

import java.util.Random;

public class JuanitoSemillas extends RandomAIPlayerController {

    public JuanitoSemillas(Player player) {
        super(player, new Random(player.getName().hashCode()));
    }

    @Override
    public int getTotalSecondsThingking() {
        return 0;
    }

}
