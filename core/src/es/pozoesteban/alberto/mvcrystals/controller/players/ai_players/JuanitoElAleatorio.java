package es.pozoesteban.alberto.mvcrystals.controller.players.ai_players;

import es.pozoesteban.alberto.mvcrystals.model.player.Player;

import java.util.Date;
import java.util.Random;

public class JuanitoElAleatorio extends RandomAIPlayerController {

    public JuanitoElAleatorio(Player player) {
        super(player, new Random(new Date().getTime()));
    }

    @Override
    public int getTotalSecondsThingking() {
        return 0;
    }
}
