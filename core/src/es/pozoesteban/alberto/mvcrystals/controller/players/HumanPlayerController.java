package es.pozoesteban.alberto.mvcrystals.controller.players;

import es.pozoesteban.alberto.mvcrystals.controller.MVCrystalsGameController;
import es.pozoesteban.alberto.mvcrystals.controller.players.support.PlayerController;
import es.pozoesteban.alberto.mvcrystals.model.GameState;
import es.pozoesteban.alberto.mvcrystals.model.player.Player;

public class HumanPlayerController extends PlayerController {

    public HumanPlayerController(Player player) {
        super(player);
    }

    @Override
    public void onBegingTurn(MVCrystalsGameController mvCrystalsGameController, GameState gameState) {
        //CONTROLLER_LOGGER.info("Human player thinking...");
    }

    @Override
    public int getTotalSecondsThingking() {
        return 0;
    }
}
