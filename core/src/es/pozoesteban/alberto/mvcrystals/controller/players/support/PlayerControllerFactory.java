package es.pozoesteban.alberto.mvcrystals.controller.players.support;

import es.pozoesteban.alberto.mvcrystals.controller.players.HumanPlayerController;
import es.pozoesteban.alberto.mvcrystals.controller.players.ai_players.JuanitoElAleatorio;
import es.pozoesteban.alberto.mvcrystals.controller.players.ai_players.JuanitoSemillas;
import es.pozoesteban.alberto.mvcrystals.controller.players.ai_players.LilloExpertillo;
import es.pozoesteban.alberto.mvcrystals.controller.players.ai_players.MaximinlianTimes;
import es.pozoesteban.alberto.mvcrystals.model.GameState;
import es.pozoesteban.alberto.mvcrystals.model.player.PlayerConfig;

public class PlayerControllerFactory {

    public static PlayerController createPlayerController(PlayerConfig playerConfig, GameState gameState) {
        switch (playerConfig.getType()) {
            case NONE:
                return null;
            case HUMAN:
                return new HumanPlayerController(gameState.getPlayer(playerConfig.getId()));
            case JUANITO_SEMILLAS:
                return new JuanitoSemillas(gameState.getPlayer(playerConfig.getId()));
            case JUANITO_EL_ALEATORIO:
                return new JuanitoElAleatorio(gameState.getPlayer(playerConfig.getId()));
            case MAXIMINLIAN:
                return new MaximinlianTimes(gameState.getPlayer(playerConfig.getId()),
                        playerConfig.maximinConfig.maxSpace,
                        playerConfig.maximinConfig.maxTimeInSeconds,
                        playerConfig.maximinConfig.numCrystalsFactor,
                        playerConfig.maximinConfig.ruinFactor);
            case LILLO_EL_EXPERTILLO:
                return new LilloExpertillo(gameState.getPlayer(playerConfig.getId()));
            default:
                throw new IllegalArgumentException(playerConfig.getType() + " not implemented yet!");
        }
    }
}
