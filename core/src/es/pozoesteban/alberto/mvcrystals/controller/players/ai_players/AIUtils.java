package es.pozoesteban.alberto.mvcrystals.controller.players.ai_players;

import es.pozoesteban.alberto.mvcrystals.model.GameState;
import es.pozoesteban.alberto.mvcrystals.model.player.Player;

import java.util.ArrayList;
import java.util.List;

public class AIUtils {
    public static float valueGameState(GameState moveGameState, Player movePlayer, float ruinFactor) {
        float points = 0;
        List<Player> players = new ArrayList();
        for (Player currentPlayer : new GameState(moveGameState).getPlayers()) {
            if (currentPlayer != null) players.add(currentPlayer);
        }
        for (Player player : players) {
            player.finalPhasePoints();
            player.finalGamePoints();
            if (player.equals(movePlayer)) {
                points += player.getPoints();
            } else if (player.isHuman()){
                points -= (2 * ruinFactor) * ((float)player.getPoints() / ((float)players.size() - 1f));
            }
        }
        return points;
    }
}
