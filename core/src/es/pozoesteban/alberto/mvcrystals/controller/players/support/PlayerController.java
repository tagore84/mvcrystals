package es.pozoesteban.alberto.mvcrystals.controller.players.support;

import es.pozoesteban.alberto.mvcrystals.controller.MVCrystalsGameController;
import es.pozoesteban.alberto.mvcrystals.model.GameState;
import es.pozoesteban.alberto.mvcrystals.model.player.Player;

public abstract class PlayerController {

    protected final Player player;

    public PlayerController(Player player) {
        this.player = player;
    }

    public abstract void onBegingTurn(MVCrystalsGameController mvCrystalsGameController, GameState gameState);

    public Player getPlayer() {
        return player;
    }

    public abstract int getTotalSecondsThingking();
}
