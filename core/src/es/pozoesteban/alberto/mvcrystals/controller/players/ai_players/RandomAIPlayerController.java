package es.pozoesteban.alberto.mvcrystals.controller.players.ai_players;

import es.pozoesteban.alberto.mvcrystals.controller.IllegalMove;
import es.pozoesteban.alberto.mvcrystals.controller.MVCrystalsGameController;
import es.pozoesteban.alberto.mvcrystals.controller.players.support.AIPlayerController;
import es.pozoesteban.alberto.mvcrystals.model.GameState;
import es.pozoesteban.alberto.mvcrystals.model.elements.Crystal;
import es.pozoesteban.alberto.mvcrystals.model.elements.TokenWarehouse;
import es.pozoesteban.alberto.mvcrystals.model.player.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static es.pozoesteban.alberto.mvcrystals.Configuration.CONTROLLER_LOGGER;


public abstract class RandomAIPlayerController extends AIPlayerController {

    protected final Random dice;

    public RandomAIPlayerController(Player player, Random dice) {
        super(player);
        this.dice = dice;
    }

    @Override
    public void onBegingTurn(MVCrystalsGameController controller, GameState gameState) {
        List<Crystal> crystals = new ArrayList();
        for (TokenWarehouse tokenWarehouse : gameState.getTokenWarehouses()) {
            for (Crystal crystal : tokenWarehouse.getAllCrystals()) {
                crystals.add(crystal);
            }
        }
        while (true) {
            Crystal crystalSelected = crystals.get(dice.nextInt(crystals.size()));
            controller.selectCrystal(crystalSelected.getId());
            List<Integer> rows = new ArrayList();
            for (int row = 0; row < 5; row++) {
                if (gameState.isRowSelectable(row)) rows.add(row);
            }
            rows.add(5);

            int rowSelected = rows.get(dice.nextInt(rows.size()));
            try {
                controller.selectRow(rowSelected);
                return;
            } catch (IllegalMove illegalMove) {
                CONTROLLER_LOGGER.warning(illegalMove.toString());
                CONTROLLER_LOGGER.throwing(this.getClass().getSimpleName(), "onBegingTurn", illegalMove);
            }
        }
    }

}
