package es.pozoesteban.alberto.mvcrystals.controller.players.ai_players;

import es.pozoesteban.alberto.mvcrystals.controller.IllegalMove;
import es.pozoesteban.alberto.mvcrystals.controller.MVCrystalsGameController;
import es.pozoesteban.alberto.mvcrystals.controller.players.support.AIPlayerController;
import es.pozoesteban.alberto.mvcrystals.model.GameState;
import es.pozoesteban.alberto.mvcrystals.model.elements.*;
import es.pozoesteban.alberto.mvcrystals.model.player.Player;

import java.util.*;

import static es.pozoesteban.alberto.mvcrystals.Configuration.CONTROLLER_LOGGER;

public class LilloExpertillo extends AIPlayerController {


    public LilloExpertillo(Player player) {
        super(player);
    }

    @Override
    public void onBegingTurn(MVCrystalsGameController controller, GameState gameState) {
        Option bestOption = getBestOption(controller, gameState);
        controller.selectCrystal(bestOption.crystalId);
        try {
            controller.selectRow(bestOption.row);
        } catch (IllegalMove illegalMove) {
            CONTROLLER_LOGGER.info("Error en la opción elegida: " + bestOption.toString());
            illegalMove.printStackTrace();
            getBestOption(controller, gameState);
        }

    }

    public Option getBestOption (MVCrystalsGameController controller, GameState gameState) {
        Set<Option> options = getOptions(gameState);
        List<Option> optionList = new ArrayList();
        for (Option option : options) {
            evaluateOption(controller, gameState, option);
            if (option.type != null) {
                optionList.add(option);
            } else {} // Opción injugable.
        }
        Collections.sort(optionList);
        return optionList.get(0);
    }

    public Set<Option> getOptions (GameState gameState) {
        Set<Option> options = new HashSet();
        for (TokenWarehouse tokenWarehouse : gameState.getTokenWarehouses()) {
            for (Color color : Color.values()) {
                List<Crystal> crystals = tokenWarehouse.getCrystals(color);
                if (!crystals.isEmpty()) {
                    for (int row = 0; row < 6; row++) {
                        options.add(new Option(crystals.get(0).getId(), row, color, crystals.size(), (tokenWarehouse instanceof CenterPlate && gameState.getCenterPlate().hasFirstPlayerToken())));
                    }
                }
            }
        }
        return options;
    }
    public void evaluateOption (MVCrystalsGameController controller, GameState gameState, Option option) {
        if (gameState.isRowSelectable(option.row, option.color)) {
            Slot[] slotRow = option.row < 5 ? gameState.getCurrentPlayer().getSlotsPanel().getSlots()[option.row] : gameState.getCurrentPlayer().getSlotsPanel().getFloor();
            int emptySlots = 0;
            int filledSlots = 0;
            for (Slot slot : slotRow) {
                if (slot.isEmpty()) emptySlots++;
                else filledSlots++;
            }
            boolean complete = emptySlots <= option.num;
            boolean twoStepsComplete = complete && filledSlots > 0;
            boolean allToFloor = emptySlots == 0;
            if (option.row < 5) {
                if (allToFloor) option.type = OptionType.ALL_TO_FLOOR;
                else if (twoStepsComplete) option.type = OptionType.TWO_STEPS_COMPLETE;
                else if (complete) option.type = OptionType.ONE_STEP_COMPLETE;
                else option.type = OptionType.INCOMPLETE;
                option.slotNum = Math.min(emptySlots, option.num);
                option.floorNum = Math.max(0, (option.num - emptySlots));
            } else {
                option.type = OptionType.ALL_TO_FLOOR;
                option.slotNum = 0;
                option.floorNum = option.num;
            }
        }
    }

    @Override
    public int getTotalSecondsThingking() {
        return 0;
    }

    private class Option implements Comparable<Option> {
        private final int crystalId;
        private final int row;
        private final Color color;
        private final int num;
        private final boolean firstPlayerToken;
        //-----------
        private OptionType type;
        private int slotNum;
        private int floorNum;

        private Option(int crystalId, int row, Color color, int num, boolean firstPlayerToken) {
            this.color = color;
            this.num = num;
            this.firstPlayerToken = firstPlayerToken;
            this.crystalId = crystalId;
            this.row = row;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Option)) return false;
            Option option = (Option) o;
            return row == option.row &&
                    num == option.num &&
                    firstPlayerToken == option.firstPlayerToken &&
                    slotNum == option.slotNum &&
                    floorNum == option.floorNum &&
                    color == option.color &&
                    type == option.type;
        }

        @Override
        public int hashCode() {
            return Objects.hash(row, color, num, firstPlayerToken, type, slotNum, floorNum);
        }

        @Override
        public int compareTo(Option o) {
            if (type == o.type) {
                switch (type) {
                    case TWO_STEPS_COMPLETE:
                    case ONE_STEP_COMPLETE:
                    case INCOMPLETE:
                        return Integer.valueOf(o.slotNum).compareTo(Integer.valueOf(slotNum));
                    case ALL_TO_FLOOR:
                        return Integer.valueOf(num).compareTo(Integer.valueOf(o.num));
                    default:
                        throw new IllegalArgumentException("Type not found: " + type);
                }
            } else {
                return type.compareTo(o.type);
            }
        }

        @Override
        public String toString() {
            return "Option{" +
                    "crystalId=" + crystalId +
                    ", row=" + row +
                    ", color=" + color +
                    ", num=" + num +
                    ", firstPlayerToken=" + firstPlayerToken +
                    ", type=" + type +
                    ", slotNum=" + slotNum +
                    ", floorNum=" + floorNum +
                    '}';
        }
    }

    private enum OptionType {
        TWO_STEPS_COMPLETE(0), ONE_STEP_COMPLETE(1), INCOMPLETE(2), ALL_TO_FLOOR(3);

        private final int value;

        OptionType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
}
