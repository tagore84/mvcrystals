package es.pozoesteban.alberto.mvcrystals.controller.players.support;

import es.pozoesteban.alberto.mvcrystals.controller.MVCrystalsGameController;
import es.pozoesteban.alberto.mvcrystals.model.GameState;
import es.pozoesteban.alberto.mvcrystals.model.player.Player;

public abstract class AIPlayerController extends PlayerController {

    public AIPlayerController(Player player) {
        super(player);
    }

    @Override
    public abstract void onBegingTurn(MVCrystalsGameController mvCrystalsGameController, GameState gameState);
}
