package es.pozoesteban.alberto.mvcrystals.controller;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Base64Coder;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.Timer;
import es.pozoesteban.alberto.mvcrystals.Configuration;
import es.pozoesteban.alberto.mvcrystals.controller.players.support.PlayerController;
import es.pozoesteban.alberto.mvcrystals.controller.players.support.PlayerControllerFactory;
import es.pozoesteban.alberto.mvcrystals.model.GameState;
import es.pozoesteban.alberto.mvcrystals.model.SaveData;
import es.pozoesteban.alberto.mvcrystals.model.elements.Crystal;
import es.pozoesteban.alberto.mvcrystals.model.elements.Slot;
import es.pozoesteban.alberto.mvcrystals.model.elements.TokenWarehouse;
import es.pozoesteban.alberto.mvcrystals.model.player.*;
import es.pozoesteban.alberto.mvcrystals.model.token_position.InCenterPlate;
import es.pozoesteban.alberto.mvcrystals.model.token_position.InFactory;
import es.pozoesteban.alberto.mvcrystals.model.token_position.TokenPosition;
import es.pozoesteban.alberto.mvcrystals.view.screens.*;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static es.pozoesteban.alberto.mvcrystals.Configuration.*;

public class MVCrystalsGameController extends Game {

	private SaveData saveData;
	private FileHandle fileHandle;

	private float lastReward;

	private WelcomeScreen welcomeScreen;
	private PlayingScreen playingScreen;
	private OnePlayerMenuScreen onePlayerMenuScreen;
	private MultiPlayerBoardMenuScreen multiPlayerBoardMenuScreen;
	private BaseScreen howToPlayScreen;
	private final boolean ui;

	private int thinkingRow;

	public List<PlayerController> playerControllers; // ToDo private! (usado en la ejecucion por consola, para mostrar cosas, eliminar antes de pasar a prod)

	private GameState gameState;

	private HashMap<Player, int[]> deepQNetworkStats;

	private final boolean isTraining;


	public MVCrystalsGameController(boolean ui, boolean isTraining) {
		this.ui = ui;
		this.isTraining = isTraining;

	}
	
	@Override
	public void create () {
		initializeSaveData();
		if (ui) {
			if (saveData.isFirstTime()) {
				saveData.isFirstTime(false);
				goToHowToPlayScreen(true);
			} else {
				welcomeScreen = new WelcomeScreen(this);
				setScreen(welcomeScreen);
			}
		}
	}

	public String getPointsText() {
		List<Player> players = new ArrayList();
		for (Player player : gameState.getPlayers()) {
			if (player != null) players.add(player);
		}
		Collections.sort(players);
		StringBuilder sb = new StringBuilder();
		for (Player player : players) {
			sb.append("   ").append(player.getName()).append(" ").append(player.getPoints()).append(" puntos   ");
			sb.append(System.getProperty("line.separator"));
		}
		return sb.toString();
	}

	//<editor-fold desc="Match management">
	public void startGame(PlayerConfig[] playerConfigs) {
		CONTROLLER_LOGGER.info("Starting game...");
		this.playerControllers = new ArrayList();
		this.thinkingRow = -1;
		this.deepQNetworkStats = new HashMap();
		gameState = new GameState(playerConfigs);

		boolean oneHumanPlayer = true;
		for (int i = 0; i < playerConfigs.length; i++) {
			if (playerConfigs[i] != null) {
				playerControllers.add(PlayerControllerFactory.createPlayerController(playerConfigs[i], gameState));
				if (i == 0 && playerConfigs[i].getType() != PlayerType.HUMAN) oneHumanPlayer = false;
				if (i != 0 && playerConfigs[i].getType() == PlayerType.HUMAN) oneHumanPlayer = false;
			} else {
				if (i == 0) oneHumanPlayer = false;
			}
		}
		if (ui) {
			goToPlayingScreen(oneHumanPlayer);
			saveData(playerConfigs);
		}
		onStartTurn();
	}

	public void endGame() {
		float maxPoints = -10000;
		float maxTieBreakPoints = 0;
		Player winner = null;
		for (Player player : gameState.getPlayers()) {
			if (player != null) {
				player.finalGamePoints();
				if (player.getPoints() > maxPoints ||
						(player.getPoints() == maxPoints && player.getTieBreakPoints() > maxTieBreakPoints)) {
					maxPoints = player.getPoints();
					maxTieBreakPoints = player.getTieBreakPoints();
					winner = player;
				}
			}
		}
		gameState.setWinner(winner);
		gameState.isFinish(true);
		if (!gameState.isClone()) showPoints();
		CONTROLLER_LOGGER.info("Game over!");
	}

	private void showPoints() {
		if (ui) {
			((PlayingScreen)screen).showPoints();
		}
		StringBuilder sb = new StringBuilder(System.getProperty("line.separator"));
		for (Player player : gameState.getPlayers()) {
			if (player != null) {
				sb.append(player.toString()).append(" got: ").append(player.getPoints() + " points!").append(System.getProperty("line.separator"));
				sb.append(player.getFinalPanel().toLargeString()).append(System.getProperty("line.separator"));
				if(deepQNetworkStats.containsKey(player)) {
					sb.append("valid: ").append(deepQNetworkStats.get(player)[0]).append(" invalid: ").append(deepQNetworkStats.get(player)[1]);
					sb.append(" ").append((float)deepQNetworkStats.get(player)[0] / (float)(deepQNetworkStats.get(player)[0] + deepQNetworkStats.get(player)[1]));
					sb.append(System.getProperty("line.separator"));
				}
			}
		}
		CONTROLLER_LOGGER.info(sb.toString());
	}
	//</editor-fold>

	//<editor-fold desc="Turn management">
	public void onStartTurn() {
		//CONTROLLER_LOGGER.info(gameState.toString());
		PlayerController currentPlayer = findPlayerController(gameState.getCurrentPlayer());
		currentPlayer.onBegingTurn(this, gameState);
	}

	private void noMoreCrystalsTask() {
		for (Player player : gameState.getPlayers()) {
			if (player != null) {
				player.finalPhasePoints();
				if (player.hasFinished()) {
					endGame();
					return;
				}
			}
		}
		gameState.nextPlayer(true);
		gameState.putCrystalsInFactories();
		gameState.getFirstPlayerToken().isTaken(false);
		onStartTurn();

	}
	private void moreCrystalsTask() {
		gameState.nextPlayer(false);
		onStartTurn();
	}

	public void onCurrentPlayerEndTurn() {
		if (gameState.noMoreCrystalsInTokenWarehouse()) {
			boolean wait = ui && !gameState.isClone();
			if (wait) {
				Timer.schedule(new Timer.Task(){
					@Override
					public void run() {
						noMoreCrystalsTask();
					}
				}, gameState.thereAreSomeHuman() ? END_PHASE_UI_DELAY_WITH_HUMAN : END_PHASE_UI_DELAY_WITHOUT_HUMAN);
			} else {
				noMoreCrystalsTask();
			}
		} else {
			moreCrystalsTask();
		}
	}
	//</editor-fold>

	//<editor-fold desc="Screens Navigation">
	public void goToHowToPlayScreen(boolean firstTime) {
		CONTROLLER_LOGGER.info("Loading how to play screen...");
		if (howToPlayScreen == null) {
			howToPlayScreen = new HowToPlayScreen(this, firstTime);
		}
		setScreen(howToPlayScreen);
	}
	public void goToPlayingScreen(boolean oneHumanPlayer) {
		CONTROLLER_LOGGER.info("Loading playing screen...");
		playingScreen = new PlayingScreen(this, gameState, oneHumanPlayer);
		setScreen(playingScreen);
	}
	public void goToWelcomeScreen() {
		CONTROLLER_LOGGER.info("Loading main menu screen...");
		if (welcomeScreen == null) {
			welcomeScreen = new WelcomeScreen(this);
		}
		setScreen(welcomeScreen);
	}
	public void goToOnePlayerMenuScreen() {
		CONTROLLER_LOGGER.info("Loading one player menu screen...");
		if (onePlayerMenuScreen == null) {
			onePlayerMenuScreen = new OnePlayerMenuScreen(this);
		}
		setScreen(onePlayerMenuScreen);
	}
	public void goToBoardMultiplayerScreen() {
		CONTROLLER_LOGGER.info("Loading board multiplayer menu screen...");
		if (multiPlayerBoardMenuScreen == null) {
			multiPlayerBoardMenuScreen = new MultiPlayerBoardMenuScreen(this);
		}
		setScreen(multiPlayerBoardMenuScreen);
	}

	//</editor-fold>

	//<editor-fold desc="Actions">
    public void selectCrystal(int crystalId) {
		List<Crystal> crystals = new ArrayList();
		List<Crystal> partners = new ArrayList();

		TokenPosition position = gameState.tokenPosition(crystalId);
		if (position instanceof InFactory) {
			crystals.addAll(gameState.getFactory(((InFactory) position).getFactoryIndex()).getCrystals(crystalId));
			partners.addAll(gameState.getFactory(((InFactory) position).getFactoryIndex()).getOthers(crystalId));
		}
		if (position instanceof InCenterPlate) {
			crystals.addAll(gameState.getCenterPlate().getCrystals(crystalId));
		}

		if (crystals.size() > 0) {
			boolean selected = !crystals.get(0).isSelected();

			if (selected) {
				gameState.setSelectedCrystals(crystals);
				gameState.setSelectedCrystalPartners(partners);
			} else {
				gameState.resetSelectedCrystals();
				gameState.resetSelectedCrystalPartners();
			}
		}

    }
	/**
	 * Realiza la insercción de cristales en la fila seleccionada, en caso de
	 * que sea un movimiento válido.
	 * Como solo mostramos los selectores de fila que son "válidos" desde la
	 * interfaz la row seleccionada siempre es correcta.
	 * Sin embargo, otra fuente de entrada (no dependiente de la interfaz) como una IA,
	 * podría pedir una row no válida, así que hay que comprobarlo.
	 * @param row Fila seleccionada para colocar los cristales
	 */
	public void selectRow(final int row) throws IllegalMove {
		boolean wait = ui && gameState.getCurrentPlayer().isAI() && !gameState.isClone();
		if (wait) {
			// Probamos antes de esperar para lanzar la excepción lo primero en caso de ser un movimiento inválido...
			GameState original = getGameState();
			GameState cloneToTry = new GameState(original);
			setGameState(cloneToTry);
			try {
				selectRowTask(row);
			} catch (IllegalMove illegalMove) {
				// Movimiento ilegal, recuperamos el gameState y salimos con la excepción...
				setGameState(original);
				throw illegalMove;
			}
			// Si no hemos lanzado ya una excepción...
			setGameState(original);
			ScheduledThreadPoolExecutor executor = (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(1);
			executor.schedule(
					new Callable<Boolean>() {
						@Override
						public Boolean call() throws IllegalMove {
							selectRowTask(row);
							return true;
						}
					}
					, gameState.thereAreSomeHuman() ? AI_THINKING_UI_DELAY_WITH_HUMAN : AI_THINKING_UI_DELAY_WITHOUT_HUMAN, TimeUnit.SECONDS);
		} else {
			selectRowTask(row);
		}
	}
	private void selectRowTask(final int row) throws IllegalMove {
		if (!gameState.someCrystalSelected()) {
			CONTROLLER_LOGGER.warning("Trying to select a row but no crystals previusly selected!");
			lastReward = -10;
			throw new IllegalMove(gameState, -1, row);
		}
		if (!gameState.isRowSelectable(row)) {
			CONTROLLER_LOGGER.warning("Unselectable row selected! (" + row + ")");
			lastReward = -10;
			throw new IllegalMove(gameState, gameState.getSelectedCrystals().get(0).getId(), row);
		}
		float previousPoints = valueGameState();


		if (gameState.takingFromCenterPlate()) {
			gameState.takeFirstPlayerToken();
		}
		StringBuilder sb = new StringBuilder(gameState.getCurrentPlayer().toString());
		sb.append(" puttin ");
		for (Crystal crystal : gameState.getSelectedCrystals()) {
			sb.append(crystal.toString()).append(" ");
		}
		sb.append("into row ").append(row);
		//if (!gameState.isClone()) CONTROLLER_LOGGER.info(sb.toString());
		gameState.putSelectedCrystalsIntoPlayerBoard(row);
		gameState.resetSelectedCrystals();
		gameState.putSelectedCrystalsPartnersIntoCenterPlate();
		gameState.resetSelectedCrystalPartners();
		if (!gameState.isClone()) {
			onCurrentPlayerEndTurn();
		}
		lastReward = valueGameState() - previousPoints;
	}
	//</editor-fold>

	//<editor-fold desc="SaveData management">
	public SaveData getSaveData() {
		return saveData;
	}
	private void initializeSaveData() {
		fileHandle = Gdx.files.local("save/save_data.cry");
		if (!fileHandle.exists()) {
			saveData = new SaveData();
			saveData();
		} else {
			try {
				loadData();
			} catch (Exception ex) {
				CONTROLLER_LOGGER.warning("Error loading data... deleting");
				fileHandle.delete();
				saveData = new SaveData();
				saveData();
			}
		}
	}
	private void saveData() {
		if (saveData != null) {
			Json json = new Json();
			fileHandle.writeString(Base64Coder.encodeString(json.prettyPrint(saveData)),
					false);
		}
	}
	private void saveData(PlayerConfig[] playerConfigs) {
		for (int i = 0; i < playerConfigs.length; i++) {
			if (playerConfigs[i] != null) {
				saveData.setLastPlayer(playerConfigs[i].getConcretePlayer(), i);
				saveData.setLastPlayerColor(playerConfigs[i].getColor(), i);
				saveData.setLastPlayerName(playerConfigs[i].getName(), i);
			} else {
				saveData.setLastPlayer(ConcretePlayer.NONE, i);
				saveData.setLastPlayerColor(PlayerColor.values()[i], i);
				saveData.setLastPlayerName("", i);
			}
		}
		saveData();
	}

	private void loadData() {
		Json json = new Json();
		saveData = json.fromJson(SaveData.class,
				Base64Coder.decodeString(fileHandle.readString()));
	}
	//</editor-fold>
	private PlayerController findPlayerController(Player currentPlayer) {
		for (PlayerController playerController : playerControllers) {
			if (playerController.getPlayer().equals(currentPlayer)) return playerController;
		}
		throw new IllegalArgumentException("Player controller not found " + currentPlayer.toString());
	}

	public int getThinkingRow() {
		return thinkingRow;
	}
	public void setThinkingRow(int thinkingRow) {
		this.thinkingRow = thinkingRow;
	}

	//<editor-fold desc="CLONE EMULATION PROPUSES">
	public void setGameState(GameState gameState) {
		this.gameState = gameState;
	}
	public void setPlayerControllers(List<PlayerController> playerControllers) {
		this.playerControllers = playerControllers;
	}

	public boolean getUi() {
		return ui;
	}

	public GameState getGameState() {
		return gameState;
	}

	public int getEmptySlots(int thinkingRow) {
		int emptySlots = 0;
		if (thinkingRow < 5) {
			for (Slot slot : gameState.getCurrentPlayer().getSlotsPanel().getSlots()[thinkingRow]) {
				if (slot.isEmpty()) emptySlots++;
			}
		} else {
			for (Slot slot : gameState.getCurrentPlayer().getSlotsPanel().getFloor()) {
				if (slot.isEmpty()) emptySlots++;
			}
		}
		return emptySlots;
	}

	//</editor-fold>

	//<editor-fold desc="ReinformentLearningMethods Testing...">
	public void createGame(String argsString) {
		if (!isTraining) throw new IllegalArgumentException("Is not training!");
		CONTROLLER_LOGGER.info("Creating game: " + argsString);
		List<Integer> playerIndex = new ArrayList();
		int seedIndex = -1;
		String[] args = argsString.split(" ");
		for (int i = 0; i < args.length; i++) {
			switch (args[i]) {
				case "-player":
					playerIndex.add(i);
					break;
				case "-seed":
					seedIndex = i;
					break;
			}
		}
		if (seedIndex >= 0) {
			try {
				Configuration.SEED = Integer.parseInt(args[seedIndex + 1]);
			} catch (NumberFormatException ex) {
				System.out.println("Invalid seed " + args[seedIndex + 1]);
				throw ex;
			} catch (ArrayIndexOutOfBoundsException ex) {
				System.out.println("Invalid seed!");
			}
		} else {
			Configuration.SEED = (int) new Date().getTime();
		}
		PlayerConfig[] playerConfigs = new PlayerConfig[playerIndex.size()];
		for (int i = 0; i < playerIndex.size(); i++) {
			playerConfigs[i] = loadPlayerConfig(args, playerIndex.get(i));
		}
		this.startGame(playerConfigs);
	}
	private static PlayerConfig loadPlayerConfig(String[] args, int index) {
		PlayerType type = PlayerType.valueOf(args[index+1]);
		int id = Integer.parseInt(args[index+2]);
		String name = args[index+3];
		switch (type) {
			case HUMAN:
				return PlayerConfig.createHumanPlayerConfig(id, name, PlayerColor.values()[id]);
			case JUANITO_SEMILLAS:
				return PlayerConfig.createJuanitoSemillasPlayerConfig(id, name, PlayerColor.values()[id]);
			case JUANITO_EL_ALEATORIO:
				return PlayerConfig.createJuanitoElAleatorioPlayerConfig(id, name, PlayerColor.values()[id]);
			case MAXIMINLIAN:
				int maxSpace_ = Integer.parseInt(args[index+3]);
				int maxTimeInSeconds = Integer.parseInt(args[index+4]);
				float numCrystalsFactor_ = Float.parseFloat(args[index+5]);
				float ruinFactor = Float.parseFloat(args[index+6]);
				return PlayerConfig.createMaximinlian(id, name, maxSpace_, maxTimeInSeconds, numCrystalsFactor_, ruinFactor, PlayerColor.values()[id]);
			case LILLO_EL_EXPERTILLO:
				return PlayerConfig.createLillo(id, name, PlayerColor.values()[id]);
			default:
				throw new IllegalArgumentException("Unknown player type args[index+1]");
		}
	}
	public float move(int action) throws IllegalMove {
		if (!isTraining) throw new IllegalArgumentException("Is not training!");
		Player current = gameState.getCurrentPlayer();
		int factoryId = action / (5*6);
		int rest = action % (5*6);
		int colorId = rest / 6;
		int row = rest % 6;
		float reward = move(factoryId, colorId, row);

		int[] values = deepQNetworkStats.containsKey(current) ? deepQNetworkStats.get(current) : new int[]{0,0};
		if (reward != -10f) {
			values[1]++;
		} else {
			values[0]++;
		}
		deepQNetworkStats.put(current, values);
		return reward;
	}
	private float move(int factoryId, int colorId, int row) throws IllegalMove {
		if (!isTraining) throw new IllegalArgumentException("Is not training!");
		Player currentPlayer = gameState.getCurrentPlayer();
		TokenWarehouse warehouse = factoryId == gameState.getNumFactories() ? gameState.getCenterPlate() : gameState.getFactory(factoryId);
		for (Crystal crystal : warehouse.getAllCrystals()) {
			if (crystal.getColor().getValue() == colorId) {
				selectCrystal(crystal.getId());
				break;
			}
		}
		if (!gameState.isRowSelectable(row)) {
			return -10f;
		}
		selectRow(row);
		if (gameState.isFinish()) {
			if (gameState.getWinner().equals(currentPlayer)) {
				CONTROLLER_LOGGER.info(currentPlayer.toString() + " wins!");
				return 100f;
			} else {
				CONTROLLER_LOGGER.info(currentPlayer.toString() + " lose!");
				return -100f;
			}
		}
		//if (gameState.getPhase() > currentPhase) {
		//	float reward = AIUtils.valueGameState(gameState, currentPlayer, 0.4f);
		//	return reward;
		//}
		return 0f;

	}
	private float valueGameState() {
		if (!isTraining) return 0;
		GameState cloneGameState = new GameState(getGameState());
		cloneGameState.getCurrentPlayer().finalPhasePoints();
		cloneGameState.getCurrentPlayer().finalGamePoints();
		return cloneGameState.getCurrentPlayer().getPoints();
	}
	public float getLastReward() {
		if (!isTraining) throw new IllegalArgumentException("Is not training!");
		return lastReward;
	}

	public void resetSelectedCrystals() {
		gameState.resetSelectedCrystals();
	}
    //</editor-fold>
}
