package es.pozoesteban.alberto.mvcrystals.controller;

import es.pozoesteban.alberto.mvcrystals.model.GameState;

public class IllegalMove extends Exception {

    private GameState gameState;
    private final int crystalId;
    private final int row;

    public IllegalMove(GameState gameState, int crystalId, int row)  {
        this.gameState = gameState;
        this.crystalId = crystalId;
        this.row = row;
    }
}
