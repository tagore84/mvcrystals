package es.pozoesteban.alberto.mvcrystals;

import java.util.Date;
import java.util.Random;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.SimpleFormatter;

public abstract class Configuration {

    public static java.util.logging.Logger CONTROLLER_LOGGER;
    public static java.util.logging.Logger MODEL_LOGGER;
    public static java.util.logging.Logger VIEW_LOGGER;
    static {
        final ConsoleHandler handler = new ConsoleHandler();
        handler.setLevel(Level.FINEST);
        handler.setFormatter(new SimpleFormatter() {
            private static final String format = "[%1$tT] [%2$-10s] [%3$-4s] [%4$-10s] [%5$-3s] %6$s %n";

            @Override
            public synchronized String format(LogRecord lr) {
                return String.format(format,
                        new Date(lr.getMillis()),
                        lr.getLoggerName(),
                        lr.getLevel().getLocalizedName(),
                        lr.getSourceClassName().substring(lr.getSourceClassName().lastIndexOf(".")+1),
                        lr.getSourceMethodName(),
                        lr.getMessage()
                );
            }
        });

        CONTROLLER_LOGGER = java.util.logging.Logger.getLogger("Controller");
        CONTROLLER_LOGGER.setLevel(Level.INFO);
        CONTROLLER_LOGGER.addHandler(handler);
        CONTROLLER_LOGGER.setUseParentHandlers(false);



        MODEL_LOGGER = java.util.logging.Logger.getLogger("Model");
        MODEL_LOGGER.setLevel(Level.INFO);
        MODEL_LOGGER.addHandler(handler);
        MODEL_LOGGER.setUseParentHandlers(false);

        VIEW_LOGGER = java.util.logging.Logger.getLogger("View");
        VIEW_LOGGER.setLevel(Level.INFO);
        VIEW_LOGGER.addHandler(handler);
        VIEW_LOGGER.setUseParentHandlers(false);

    }

    public static final boolean SCREEN_DEBUG = false;


    public static final float HORIZONTAL_CAROUSEL_ACTOR_MOVE = 0.3f; // Seconds

    public static int SEED = (int) new Date().getTime();
    public static Random DICE = new Random(SEED);
    public static final int END_PHASE_UI_DELAY_WITHOUT_HUMAN  = 0; // ToDo Seconds
    public static final int END_PHASE_UI_DELAY_WITH_HUMAN  = 2; // ToDo Seconds
    public static final int AI_THINKING_UI_DELAY_WITHOUT_HUMAN  = 0; // Seconds
    public static final int AI_THINKING_UI_DELAY_WITH_HUMAN  = 1; // Seconds

    public static final boolean CENTER_PLATE_RANDOM_POSITIONS = false;

    public static final String SKIN_NAME = "skins/crystals_v3/crystals_v3.json";
}
